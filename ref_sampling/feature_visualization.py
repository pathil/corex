import torch
from torch.utils.data.dataloader import Dataset

from typing import List

from zennit.composites import EpsilonPlusFlat, EpsilonPlus
from zennit.canonizers import SequentialMergeBatchNorm
from crp.attribution import CondAttribution
from crp.concepts import ChannelConcept
from crp.helper import get_layer_names
from crp.visualization import FeatureVisualization
from crp.image import vis_opaque_img

# from common.loader import LOADER
from common.loader import ComposerInfo

class FeatureVisualizer:
    def __init__(self, model : torch.nn.Module, dataset: Dataset, composer_info: ComposerInfo, device : str = "cuda:0" if torch.cuda.is_available() else "cpu") -> None:
        self.model = model
        self.comp_info = composer_info
        self.device = device
        self.dataset = dataset

    def analyse(self, results_dir: str, batch_size: int = 16):        
        self.model.to(self.device)
        self.model.eval()

        preprocessing_fn = self.comp_info.normalizer

        canonizers = [SequentialMergeBatchNorm()]
        composite = EpsilonPlusFlat(canonizers)
        cc = ChannelConcept()

        layer_names = get_layer_names(self.model, [torch.nn.Conv2d])
        layer_map = {layer: cc for layer in layer_names}

        attribution = CondAttribution(self.model)

        cache = None #ImageCache()

        fv = FeatureVisualization(attribution, self.dataset, layer_map, preprocess_fn=preprocessing_fn,
                                    path=results_dir, cache=cache)

        fv.run(composite, 0, len(self.dataset), batch_size=batch_size)


    def get_max_ref(self, concepts: List[int], layer_name: str, results_dir: str, n_refimgs=10):
        mode = "relevance"
        
        self.model.to(self.device)
        self.model.eval()

        preprocessing_fn = self.comp_info.normalizer

        canonizers = [SequentialMergeBatchNorm()]
        composite = EpsilonPlusFlat(canonizers)
        cc = ChannelConcept()

        layer_names = get_layer_names(self.model, [torch.nn.Conv2d])
        layer_map = {layer: cc for layer in layer_names}

        attribution = CondAttribution(self.model)

        cache = None #ImageCache()

        fv = FeatureVisualization(attribution, self.dataset, layer_map, preprocess_fn=preprocessing_fn,
                                    path=results_dir, cache=cache)

        ref_imgs = fv.get_max_reference(concepts, layer_name, mode, (0, n_refimgs), composite=composite, rf=True, plot_fn=vis_opaque_img)

        return ref_imgs
