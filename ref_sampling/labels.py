from tinydb import TinyDB
from tinydb import Query

class DbAccess:
	def __init__(self, data_path):
		self.data_path = data_path
		# db_path = os.path.join(data_path, "concept_labels.json")
		self.db = TinyDB(data_path)

	def save_desc(self, c_id : int, layer : str, desc : str) -> bool :
		todo = Query()	
		item = self.db.get((todo.c_id == c_id) 
				& (todo.layer == layer))	

		if (item != None):		
			ids = self.db.update({"desc":  desc}, todo.c_id == c_id and todo.layer == layer)

			print(f"Updated concept {item['c_id']}")		
		else:
			newItem = {"c_id": c_id, "layer": layer, "desc": desc}
			self.db.insert(newItem)
			print(f"Added description '{desc}' for concept {c_id}")

	def load_desc(self, c_id : int, layer : str) -> str:
		todo = Query()	
		item = self.db.get((todo.c_id == c_id) 
				& (todo.layer == layer))	
		if (item != None):		
			return item['desc']
		else:
			return ""