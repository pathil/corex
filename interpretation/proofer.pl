%%  Proofer
:- op(750, xfy, =>).

prove_clause(G, Body) :-
    clause(G, B),
    defaulty_better(B, Body).

defaulty_better(true, true).
defaulty_better((A,B), (BA,BB)) :-
    defaulty_better(A, BA),
    defaulty_better(B, BB).
defaulty_better(G, g(G)) :-
    G \= true,
    G \= (_,_).

prove(true, true).
prove((A,B), (TA,TB)) :-
        prove(A, TA),
        prove(B, TB).

prove(g(G), TBody => G) :-
        prove_clause(G, Body),
        prove(Body, TBody).
