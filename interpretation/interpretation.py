from typing import List, Dict
import common.paths as paths
from common.paths import FileNavigator, DirNavigator

from common.structs import Rule, Fact
from pipeline.formalisation import Aleph, MODEH
from multiprocessing import Process, Queue
from pyswip import Prolog, Functor
import os
import shutil
from tqdm import tqdm
from common.helper import parse_fact
from common.structs import Relation
from pathlib import Path

KB = "kb.pl"

class Interpretation:
    def __init__(self, sample: str, sample_bg: dict, set_relations: Dict[str,Relation], rules : List[Rule], out_dir) -> None:
        self.sample = sample
        self.sample_bg = sample_bg
        self.set_relations = set_relations
        self.rules: List[Rule] = rules
        self.out_dir = out_dir
        self.kb_file = Path(out_dir) / f"kb.pl"
        self._setup_explainings()

    def _rules(self) -> str:
        # for i in range(len(self.rules)):
        #     self.rules[i].name += f"_{i}"
        rules = self.rules
        # for r_i in range(len(rules)):
        #     rules[r_i].head.name = self.rules[r_i].name + f"_{r_i}"
        rule_str = "\n".join(rule.to_prolog_str(format=True) for rule in rules)
        return rule_str

    def _discontiguous(self) -> str:
        disc_str = ":- style_check(-singleton).\n"
        for rel in self.set_relations['unary']:
            disc_str += f":- discontiguous({rel.name}_bg/2).\n"

        for rel in self.set_relations['binary']:
            disc_str += f":- discontiguous({rel.name}_bg/3).\n"

        disc_str += ":- discontiguous(pos/2).\n"
        disc_str += ":- discontiguous(neg/2).\n"
        disc_str += ":- discontiguous(pos_bg/2).\n"
        disc_str += ":- discontiguous(neg_bg/2).\n"
        disc_str += ":- discontiguous(trace/2).\n"
        disc_str += ":- discontiguous(prove/2).\n"
        
        return disc_str

    def _sample_bg(self, sample : str):
        sample_str = ""
        for layer in self.sample_bg:
            # negs = []
            for rel in self.sample_bg[layer]['unary']:
                # if rel.name == "has_a":
                #     continue

                # if rel[0] == "is_neg":
                #     negs.append(rel[1])

                fact : Fact = Fact(rel[0] + "_bg", sample, f"{layer}_c{rel[1]}")
                sample_str += fact.to_prolog_str() + "\n"

            for rel in self.sample_bg[layer]['binary']:
                # fact : Fact = Fact(rel[0], sample, f"{layer}_c{rel[1]}", f"{layer}_c{rel[2]}")
                # sample_str += fact.to_prolog_str(statement=True) + "\n"
                # c_1 = (Fact("is_neg", sample, f"{layer}_c{rel[1]}") if rel[1] in negs else Fact("is_pos", sample, f"{layer}_c{rel[1]}")).to_prolog_str(False)
                # c_2 = (Fact("is_neg", sample, f"{layer}_c{rel[2]}") if rel[2] in negs else Fact("is_pos", sample, f"{layer}_c{rel[2]}")).to_prolog_str(False)
                # fact : Fact = Fact(rel[0], sample, c_1, c_2)
                fact : Fact = Fact(rel[0] + "_bg", sample, f"{layer}_c{rel[1]}", f"{layer}_c{rel[2]}")
                sample_str += fact.to_prolog_str() + "\n"

        return sample_str

    def _setup_explainings(self):
        sample = self.sample
        out_dir = self.out_dir

        prog = self._discontiguous()
        prog += "\n\n"
        prog += self._rules()
        # prog += rule.to_prolog_str(format=True)
        prog += "\n\n"
        prog += Aleph.domain_facts(self.set_relations)
        prog += "\n\n"
        prog += self._sample_bg(sample)
        prog += "\n%Proofer\n"

        prog += FileNavigator.proofer().read()
        # with open(paths.FILE_PROVER, 'r') as file:
        #     prog += file.read()

        prog += "\n\n"
        prog += FileNavigator.tracer().read()

        # with open(paths.FILE_TRACER, 'r') as file:
        #     prog += file.read()

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        with open(self.kb_file, 'w') as file:
            file.write(prog)

    def is_absent(self, fact: Fact):
        '''Check if a concept has negative relevance'''
        queries = []
        for var in fact.variables[1:]:
            queries.append(f"is_neg({fact.variables[0]},{var}).")
        results = self._prolog(self.kb_file, queries)
        return results

    def is_existent(self, fact: Fact) -> bool:
        '''Check if tha sample has the concept'''
        queries = []
        for var in fact.args[1:]:
            queries.append(f"has_a({fact.args[0]},{var}).")
        results = self._prolog(self.kb_file, queries)
        
        if type(results[0]) == str and results[0] == 'false.':
            return False
        elif type(results[0]) == list and type(results[0][0]) == str \
            and results[0][0] == 'true.':
            return True

        return None

    def explain(self):
        queries = []
        for rule in self.rules:
            queries.append(f"{rule.head.name}({self.sample}).")
        results = self._prolog(self.kb_file, queries)
        
        valid_rules = []
        for r_i in range(len(results)):
            if type(results[r_i]) == list and results[r_i][0] == 'true.':
                valid_rules.append(self.rules[r_i])
        return valid_rules

    def prove(self, rule_idx: int):
        rule = self.rules[rule_idx].head.name
        query_str = f"prove(g({rule}({self.sample})),M)."
        
        result = self._prolog(self.kb_file, [query_str])
        if type(result) == list:
            return result[0]
        return result

    def trace(self, rule_idx: int):
        rule = self.rules[rule_idx].head.name
        query_str = f"trace({rule}({self.sample}),M)."
        result = self._prolog(self.kb_file, [query_str])
        return result[0]

    def _prolog(self, filename, queries: List[str]):
        try:
            prolog = Prolog()
            response = list(prolog.query(f"working_directory(CWD, CWD)."))
            cwd = response[0].get('CWD')
            
            ilp_dir = os.path.dirname(os.path.abspath(filename)).replace('\\','/')
            
            if cwd != ilp_dir.lower():
                response = list(prolog.query(f"working_directory(_, '{ilp_dir}')."))

            consult_name = os.path.basename(filename)
            prolog.consult(consult_name,catcherrors=(True))
            
            result = []
            for q in queries:
                quer_res = []
                out = list(prolog.query(q, normalize=False))
                if len(out) == 0:
                    result.append("false.")
                else:
                    for i in range(len(out)):
                        if len(out[i]) == 0:
                            quer_res.append("true.")
                            continue
                        functor: Functor = out[i][0]
                        val = functor.value['M'] #string bei proof
                        if type(val) == list:
                            quer_res.append([v.value for v in val])
                        elif type(val) == str:
                            quer_res.append(val)

                    result.append(quer_res)
            
            response = list(prolog.query(f"working_directory(_, '{cwd}')."))
            # query.close()
            # prolog.query("halt.")
            # del prolog
        except Exception as ex:
            result = f"Error occured while querying {filename}:\n{ex}"
        finally:
            # q.put(result)
            return result
