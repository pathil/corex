%%  Tracer
trace(Goal, [Goal|Trace]) :-
    trace_b(Goal,Trace).
 
 % trace_b(true).  %unnötig?
 trace_b(false). %unnötig?
 trace_b(true,_) :- true.
 
 trace_b((A,_), [A|Body]) :-
    trace_b(A,Body).
 
 trace_b((_,B), [B|Body]) :-
    trace_b(B,Body).
 
 trace_b(Goal, Y) :-
    Goal \= true,
    Goal \= false,  %unnötig?
    Goal \= (_,_),
    clause(Goal,Body),
    trace_b(Body,Trace),
    (Body \= true ->
        Y = [Body|Trace]
        % ;Y = [true]   % uncomment this line to trace all paths (i.e. true and false paths)
    ).   
 
 trace_b(Goal, Trace) :-
    Goal \= true,
    Goal \= (_,_),
    \+ clause(Goal,_),
    Trace = [false].
 