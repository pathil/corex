import sys
import os
current = os.path.dirname(os.path.realpath(__file__))
root = os.path.dirname(current)
sys.path.append(root)

import math
from tkinter import *
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)
import matplotlib.pyplot as plt
import numpy as np
from typing import Tuple

from common.paths import ProjectNavigator, FileNavigator, DirNavigator
from common.loader import LOADER, COMPOSER
from common.datasets import DATASET
from ref_sampling.labels import DbAccess

import zennit.image as zimage
import argparse

from common.config import Config

def rgb_hack(rgb):
	return "#%02x%02x%02x" % rgb  

class View:
	'''
	In the past there was a need for a UI, that allows inidiviual control of each item in the grid. E.g. to select/unselect a single img.
	However, the need for this functionality got lost. So think about using a single matplotlib-figure instead of multiple ones to 
	boost rendering time.
	'''

# region Properties
	#	desc	
	@property
	def desc(self):		
		return View.get_text(self.desc_text_box)
	
	@desc.setter
	def desc(self, value):
		self._desc = value
		self.desc_text_box.delete('1.0', 'end')
		self.desc_text_box.insert(END, str(value))

	#	c_id	
	@property
	def c_id(self):		
		return int(View.get_text(self.c_id_text_box))
	
	@c_id.setter
	def c_id(self, value):
		self._c_id = value
		self.c_id_text_box.delete('1.0', 'end')
		self.c_id_text_box.insert(END, str(value))

	#	layer
	@property
	def layer(self):		
		return View.get_text(self.layer_text_box)
	
	@layer.setter
	def layer(self, value):
		self._layer = value
		self.layer_text_box.delete('1.0', 'end')
		self.layer_text_box.insert(END, str(value))

	#	page
	@property
	def page(self):		
		return self._page
	
	@page.setter
	def page(self, value):
		self._page = value
		self.page_label.config(text = f"Page {value}/{self.pages_total}")

# endregion

	def __init__(self, layout : Tuple[int,int]) -> None:
		self.layout = layout
		self._c_id = ""
		self._layer = ""
		self._desc = ""
		self._page = 1
		self.pages_total = 1
		self.lookupList = []
		self.init_window()

	def __call__(self):
		# run the gui
		self.window.mainloop()

	def init_window(self):
		window = Tk()
		window.title('Label Imagenet-Filters')
		window.geometry(f"{self.layout[1] * 150}x{self.layout[0] * 150 + 150}")	#	= [grid rows] * 150 + (25 * [6 additional rows])

		window.config(bg=rgb_hack((220, 220, 220)))

		head_label = Label(
			window,
			height=1,
			text='',
			bg=rgb_hack((128, 128, 128)),
			anchor="w"
		)
		head_label.grid(row=self.layout[0] + 1, column=0, columnspan=2, sticky='nsew')	
		self.head_label = head_label

		desc_label = Label(
			window,
			height=1,	
			text=f'label:',
			bg=rgb_hack((192, 192, 192)),
			anchor="w"
		)
		desc_label.grid(row=self.layout[0] + 2, column=0, columnspan=2, sticky='nsew')		

		desc_text_box = Text(
			window,
			height=1,
			width=20,
			wrap='none',	
		)
		desc_text_box.grid(row=self.layout[0] + 2, column=0, columnspan=2, sticky='w', padx=(60,0), pady=(5,0))
		self.desc_text_box = desc_text_box

		Button(	
			window,
			text='Confirm',
			command=self._confirm_command,
			height=1
		).grid(row=desc_text_box.grid_info().get('row'), column=desc_text_box.grid_info().get('column') + 1, sticky='e')

		c_id_label = Label(
			window,
			height=1,	
			text=f'concept:',
			anchor="w",
			bg=rgb_hack((160, 160, 160))

		)
		c_id_label.grid(row=self.layout[0] + 3, column=0, columnspan=2, sticky='nsew')

		c_id_text_box = Text(
			window,
			height=1,
			width=10,
			wrap='none',
		)
		c_id_text_box.grid(row=c_id_label.grid_info().get('row'), column=0, columnspan=2, sticky='w', padx=(60,10), pady=c_id_label.grid_info().get('pady'))
		self.c_id_text_box = c_id_text_box

		layer_label = Label(
			window,
			height=1,	
			text=f'layer:',
			anchor="w",
			bg=rgb_hack((160, 160, 160))
		)
		layer_label.grid(row=self.layout[0] + 4, column=0, columnspan=2, sticky='nsew')

		layer_text_box = Text(
			window,
			height=1,
			width=20,
			wrap='none'	
		)
		layer_text_box.grid(row=layer_label.grid_info().get('row'), column=0, columnspan=2, sticky='w', padx=(60,10), pady=layer_label.grid_info().get('pady'))
		self.layer_text_box = layer_text_box

		Button(	
			window,
			text='Reload',
			command=self._reload_command,	
			# width=50,			
		).grid(row=layer_text_box.grid_info().get('row'), column=layer_text_box.grid_info().get('column') + 1, sticky='e')

		page_label = Label(
			window,
			height=1,	
			text=f'Page 1/?',
			bg=rgb_hack((128, 128, 128)),
			anchor='w'
		)
		page_label.grid(row=self.layout[0] + 5, column=0, columnspan=2, sticky='nsew')
		self.page_label = page_label

		Button(	
			window,
			text='<-',
			command=self._page_prev_command
		).grid(row=page_label.grid_info().get('row'), column=page_label.grid_info().get('column'), columnspan=2, sticky='w', padx=(60,0))

		Button(	
			window,
			text='->',
			command=self._page_next_command
		).grid(row=page_label.grid_info().get('row'), column=page_label.grid_info().get('column'), columnspan=2, sticky='w', padx=(90,0))

		error_label = Label(
			window,
			text= '',
			font= ('Helvetica 12 underline'),
			# foreground='black',
			bg=rgb_hack((220, 220, 220)),	
			anchor="w",
		)
		error_label.grid(row=self.layout[0] + 6, column=0, columnspan=self.layout[1], sticky='nsew')
		self.error_label = error_label

		window.bind('<Return>', self._return_key)
		desc_text_box.focus_set()
		self.window = window

	def _plot(self, matrix, concept_refields):
		#	destroy the forms to get rid of pyplot figures
		for child in self.window.winfo_children():
			if child.widgetName == "canvas":
				child.destroy()

		# Adds a subplot at the 1st position
		for i in range(len(matrix)):
			for j in range(len(matrix[i])):

				if matrix[i][j] >= len(concept_refields):
					continue

				tensor = concept_refields[matrix[i][j]]

				# create figure
				fig = plt.figure(figsize=(1.5, 1.5))			

				fig.add_subplot()
				
				img = zimage.imgify(tensor, vmin=None, vmax=None, symmetric=False)
				# img = zimage.imgify(tensor.detach().cpu(), vmin=None, vmax=None, symmetric=False)
				ax = plt.gca()

				ax.imshow(img)
				ax.set_xticks([])
				ax.set_yticks([])

				canvas = FigureCanvasTkAgg(fig,
									master = self.window)
				canvas.draw()

				# placing the canvas on the Tkinter window
				get_widz = canvas.get_tk_widget()
				get_widz.grid(row=i, column=j)	

				plt.close('all')	#	pyplot might open figure in window

	def _set_grid(self, page : int = 1):
		if self.error_label != None:
			self.error_label.config(text='')

		arr = np.full(self.layout, fill_value=-1, dtype=np.int8)

		start = arr.size * (page - 1)
		arr_i = 0
		for c, img in enumerate(self.concept_re_field[start:], start=start):
			if arr_i >= arr.size:
				break	
			arr.itemset(arr_i, c)
			arr_i += 1

		self._plot(arr, self.concept_re_field)

	def set_data(self, c_id : int, layer : str, concept_re_field, desc : str = ""):
		self.c_id = c_id
		self.layer = layer
		self.desc = desc
		self.concept_re_field = concept_re_field
		self.pages_total = math.ceil(len(concept_re_field) / (layout[0] * layout[1]))
		self.page = 1
		self.head_label.config(text = f'Name the recognized concept for filter {c_id}:' )		
		self._set_grid()

	def get_text(text_box : Text):
		text = text_box.get('1.0', 'end-1c')
		while text.endswith('\n'):
			text = text[:len(text) - 1]
		return text

	def _return_key(self, event):
		print("return_key pressed")
		self.confirm()

	def _reload_command(self):
		self.reload(self.c_id, self.layer)

	def _confirm_command(self):
		self.confirm()

	def _page_next_command(self):
		if self.page < self.pages_total:
			self.page += 1
			self._set_grid(self.page)

	def _page_prev_command(self):
		if self.page > 1:
			self.page -= 1
			self._set_grid(self.page)

	def reload(self, c_id : int, layer : str):
		pass

	def confirm(self):
		pass

	def show_error(self, error):
		self.errorLabel.config(text = f"[Error]: {error}")
		self.errorLabel.config(foreground='red')

	def show_info(self, info):
		self.errorLabel.config(text = f"[Info]: {info}")
		self.errorLabel.config(foreground='black')

class Control:
	def __init__(self, pn: ProjectNavigator, layout, c_id : int, layer : str = "") -> None:
		self.pn = pn
		self.config = pn.config		
		self.db_access = DbAccess(pn.file.labels_db().path)
		
		view = View(layout)
		view.confirm = self.confirm
		view.reload = self.load_data
		self.view = view

		if layer == "":
			layer = self.config.CRP.layers

		self.load_data(c_id, layer)

	def load_data(self, c_id : int, layer : str):
		print(f"{c_id=}")
		desc = self.db_access.load_desc(c_id, layer)
		print(f"{desc=}")

		# concept_id = text_box_c_id.get('1.0', 'end-1c')
		re_field = self.get_visualizations(c_id, layer)
		self.view.set_data(c_id, layer, re_field[c_id], desc)

	def confirm(self):
		new_desc = self.view.desc
		c_id = self.view.c_id
		layer = self.view.layer
		if len(new_desc) > 0:
			self.db_access.save_desc(c_id, layer, new_desc)
		else:
			self.view.show_error(f"Empty value not allowed")

	'''
		We gather the Reference Sampling results
		As implemented in https://github.com/rachtibat/zennit-crp/blob/master/tutorials/feature_visualization.ipynb
		
	'''
	def get_visualizations(self, c_id : int, layer : str):
		import torch
		from ref_sampling.feature_visualization import FeatureVisualizer
		from crp.image import plot_grid

		num = 40	#	number of samples to load
		device = "cuda:0" if torch.cuda.is_available() else "cpu"
		model = LOADER[self.config.General.model](device)
		comp_info = COMPOSER[self.config.General.composer]
		dataset = DATASET[self.config.General.dataset_loader](self.config.General.dataset)

		fv = FeatureVisualizer(model, dataset, comp_info)
		ref_c = fv.get_max_ref([c_id], layer, self.config.FeatureVis.results, num)
		return ref_c
		
	def start(self):
		self.view()

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Label imagenet receptive fields from concepts of a VGG16.')
	parser.add_argument('-p', '--project', type=str,
                    choices=list(x.replace(".ini","") for x in os.listdir(DirNavigator.configs())),
                    required=True,
                    help='The identifier of the model to conduct an analysis for.')
	parser.add_argument('-c', '--cid', type=int,
					default=0,
					help='The initial concept the window loads with.')
	# parser.add_argument('-l', '--layer', type=str,
	# 				default='features.40',
	# 				help='The initial layer the concept is member of.')

	args : argparse.Namespace = parser.parse_args()

	config = Config.load_config(FileNavigator.config(args.project))
	pn = ProjectNavigator(config)

	layout = (4,5)
	c_id = args.cid
	# layer = args.layer
	layer = config.CRP.layers[0]


	controller = Control(pn, layout, c_id, layer)
	controller.start()


