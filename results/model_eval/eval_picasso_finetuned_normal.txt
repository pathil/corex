Evaluation results
train-phase:
TPs: 8886
TNs: 8996
FPs: 5
FNs: 115
Precision: 0.999437633562029
Recall: 0.9872236418175758
F1: 0.9932930918846412
test-phase:
TPs: 984
TNs: 999
FPs: 0
FNs: 15
Precision: 1.0
Recall: 0.984984984984985
F1: 0.9924357034795764

Mode normal