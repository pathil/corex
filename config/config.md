Define a .INI-File for a project in this folder. Equip it with the following properties:

####[General]
    -   project: Name of the project. take the name of the .INI-file
    -   results: output-directory for results of the pipeline and the statistics.
    -   samples: the directory, that serves as the input of the pipeline and is a subset of the original dataset. Samples have to be arranged in folders pos/ and neg/ that have to represent the ground truth.
    -   dataset: directory of the original dataset. It is used for finetuning and evaluation.
    -   file_ending: Define the file ending of the image-samples here. Cases must match.
    -   model: key of model-loader in the common.loader.LOADER dictionary.
    -   composer: key of preprocessing arguments from the common.loader.COMPOSER dictionary.
    -   model_pos: the target output of the model for whose samples a theory is supposed to constructed for.
    -   model_neg: the target output of the model for the contrastive samples for model_pos.

####[CRP]
    -   layer: Dict-key of the investigated layer (only one layer supported at the moment)
    -   filter_quantil: The top quantil to select concepts from the set of all concepts within a layer.
    
####[Localization]
    -   rel_mask_threshold: All normalized values of the relevance map below this value are set to 0.
    -   alpha_val: The alpha-value for the concave-hull. It determines the number of size of produced polygons. A value of 0.4 is recommended for our experiments.

####[Relations]
    -   intersect_tolerance: Tolerance for center-relation in CompassAlign and for amid-relation in Surrounding
    -   sets: List of Relation-sets, from which relations shall be computed.

####[Aleph]
    -   random_seed: Sets Alephs random seed.
    -   pos_only: Set true, if you learn from only a positive set.