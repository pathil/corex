import os
import argparse

from typing import List, Tuple

from common.paths import FileNavigator, DirNavigator
from common.config import Config, str_to_type

import time

from pipeline.pipeline import Pipeline
from common.logging import Logger

class Controller:
    def __init__(self, config: Config, steps: Tuple[int, int], device: str, ignore_existing: bool = False, concepts_masked: List[int] = []):
        self.config: Config = config
        self.steps = steps
        self.device = device
        self.results = config.General.results + config.General.project
        self.ignore_existing = ignore_existing
        self.concepts_masked = concepts_masked
        
        self.log = Logger(config.General.results)
        # steps = (3,3)
                
        self.pipeline = Pipeline(config, device, self.log, ignore_existing, concepts_masked)
        self.pipeline.assemble(steps)

    @staticmethod
    def all_with_filending(path: str, file_ending: str):
        collection = list()
        for root, dir, files in os.walk(path):
            for file in files:
                if(file.endswith(file_ending)):
                    collection.append(path / file)
        return collection

    def info(self, config, args):
        info = self.generation_info(args)
        self.log.default(info)

    def run(self):
        self.pipeline.produce()

    def generation_info(self, args):
        info = info_seperator(inner=False)
        info += align_info(f"CONFIGURATION")
        info += align_info(info_seperator())
        info += align_info(f"Device: '{args.device}'")
        if self.steps[0] == self.steps[1]: 
            info += align_info(f"Executing step: {self.steps[0]}")
        else: 
            info += align_info(f"Executing steps: {self.steps[0]} to {self.steps[1]}")
        info += align_info(f"Ignore Existing: {self.ignore_existing}")
        info += align_info(f"Mode: {'masked' if self.concepts_masked else 'normal'}")
        if self.concepts_masked:
            info += align_info(f"Masked concepts: {self.concepts_masked}")
        info += align_info(info_seperator())
        info += align_info(f"Project: {args.project}")
        info += align_info(f"config_file = '{FileNavigator.config(args.project).path.name}'", level=1)
        info += align_info(f"data-dir = '{self.config.General.samples}'", level=1)
        info += align_info(f"results-dir = '{self.config.General.results}'", level=1)
        info += align_info(f"model-loader = {self.config.General.model}", level=1)
        info += align_info(f"composer = {self.config.General.composer}", level=1)
        info += align_info(f"model positive target class = {self.config.General.model_pos}", level=1)
        info += align_info(f"model positive target class = {self.config.General.model_neg}", level=1)
        info += align_info(info_seperator())
        info += align_info(f"CRP:")    
        info += align_info(f"filter_quantile = {self.config.CRP.filter_quantil}", level=1)
        info += align_info(f"layer = {self.config.CRP.layers}", level=1)
        info += align_info(info_seperator())
        info += align_info(f"Localization:")
        info += align_info(f"rel_mask_threshold = {self.config.Localization.rel_mask_threshold}", level=1)
        info += align_info(f"alpha_val = {self.config.Localization.alpha_val}", level=1)
        info += align_info(info_seperator())
        info += align_info(f"Relations:")
        info += align_info(f"intersect_tolerance = {self.config.Relations.intersect_tolerance}", level=1)
        info += align_info(f"sets = {self.config.Relations.sets}", level=1)
        info += align_info(f"exclude = {self.config.Aleph.exclude}", level=1)
        info += align_info(info_seperator())
        info += align_info(f"ILP-Framework: Aleph")
        info += align_info(f"random_seed = '{self.config.Aleph.random_seed}'", level=1)
        info += align_info(f"pos_only = {self.config.Aleph.pos_only}", level=1)
        info += info_seperator(inner=False)
        return info

def parse_step_range(arg: str, default=Tuple):
    if len(arg) > 1:
        first = arg[0] if arg[0] != '' else default[0]
        second = arg[1] if arg[1] != '' else default[1]
        steps = (int(first), int(second))
    elif len(arg) == 1:
        steps = (int(arg[0]), int(arg[0]))    
    return (max(steps[0], default[0]), min(steps[1], default[1]))

def align_info(line:str, padding: int=80, level=0):
    line = ' '*4*level + line
    return f"| {line.ljust(padding, ' ')} |\n"

def info_seperator(padding: int=80, inner:bool= True):
    if inner:
        return "-" * padding
    return " " + "-" * (padding+2) + " \n"


def main(args : argparse.Namespace):
    project : str = args.project
    steps = parse_step_range(args.steps, (0,5))
    device = args.device
    masked_filter: List[int] = []
    if args.mask != "":
        masked_filter = str_to_type(args.mask, List[int])
    ignore_existing: bool = args.ignore_existing
    
    config = Config.load_config(FileNavigator.config(project))

    controller = Controller(config, steps, device, ignore_existing, masked_filter)
    controller.info(config, args)
    controller.run()
    # controller.postprocess()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect concept predictions for selected samples.')
    parser.add_argument('-p', '--project', type=str,
                    choices=list(x.replace(".ini","") for x in os.listdir(DirNavigator.configs())),
                    required=True,
                    help='The identifier of the experiment to generate hypothesis for.')
    parser.add_argument('-s', '--steps', type=lambda x: x.split('-'),
                    default='0-5',
                    required=False,
                    help='Specify, which steps to execute. Steps are consecutive so you are not allowed to change the order, but you can skip steps coming before or after. If not specified all 5 steps are executed. \'t\'')

    parser.add_argument('-d', '--device', 
                    default="cuda:0",
                    help='Sets the device model and samples are loaded to.')

    parser.add_argument('-i', '--ignore-existing',
                    action='store_true',
                    default=False,
                    help='If set, then generation ignores already computed files.')

    parser.add_argument('-m', '--mask', type=str,
                    default='',
                    help='Filters, which parameters are being set to zero. Define comma-separated like "30,0,411,128". Only works when using single layer. May not work if started after step 2. Overwrites bg_aleph.pl')

    args : argparse.Namespace = parser.parse_args()
    
    start = time.time()
    main(args)
    end = time.time()
    
    print('Execution Time: {}'.format(end-start))

# multiple layer for hplp