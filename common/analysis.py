import numpy as np
from matplotlib import pyplot as plt

from typing import List, Tuple
from shapely.geometry import Polygon
from PIL import Image

from shapely import geometry


COLORS = ["red", "blue", "green", "gold", "purple", "steelblue", "orange", "magenta",
        "darkred", "cyan", "lime", "olive", "gray" ,"mediumpurple", "peru", "violet"]


def plot_shapes(polys : List[Polygon], scale:int=3, img:Image=None, xlim:int=256, ylim:int=256, single_color:bool=True, colors: List[str]=[], fill:bool=True, no_axis:bool=True, labels:List[str]=[], info=False, out_path=""):
    plt.figure(figsize=(scale,scale))    
    plt.subplots_adjust(left=0,right=1,bottom=0,top=1)
    
    if len(polys) > 1:
        for i in range(len(polys)):
            if len(colors) == 0:
                c = COLORS[0] if single_color else COLORS[i % len(COLORS)]
            else:
                c = colors[0] if single_color else colors[i]
                
            x,y = polys[i].exterior.xy
            plt.plot(x,y, c=c)
            if fill:
                plt.fill(x,y, c=c)
            if len(labels) == len(polys):
                plt.annotate(f"{labels[i]}",xy=(polys[i].centroid.x,polys[i].centroid.y))
        if info:
            print(f"Count: {len(polys)}")
            print("\n".join(f"shape_{i}: area = {polys[i].area}" for i in range(len(polys))))
    else:
        poly = polys[0]
        if (type(poly) == geometry.linestring.LineString or
         type(poly) == geometry.point.Point):
            x,y = poly.xy
        elif type(poly) == Polygon:
            x,y = poly.exterior.xy
        else:
            raise f"Cannot plot {type(poly)}"
        plt.plot(x,y, c="red")
        if fill:
            plt.fill(x,y, c="red")
        if len(labels) == len(polys):
            plt.annotate(f"{labels[0]}",xy=(poly.centroid.x,poly.centroid.y))
        if info:
            print(f"shape_{0}: area = {poly.area}")

    if no_axis:
        ax = plt.gca()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)


    plt.ylim(ylim, 0)
    plt.xlim(0, xlim)

    if img != None:
        ax = plt.gca()
        img = ax.imshow(np.asarray(img), alpha=0.8)
    
    if out_path == "":
        plt.show()
    else:
        import matplotlib
        matplotlib.use('Agg')
        plt.savefig(str(out_path), dpi=300)

def plot_heatmap(heatmap : np.ndarray, out_path: str):
    import matplotlib.pyplot as plt
    import numpy as np
    from matplotlib.colors import ListedColormap
    from matplotlib.colors import TwoSlopeNorm

    my_cmap = plt.cm.seismic(np.arange(plt.cm.seismic.N))
    my_cmap = ListedColormap(my_cmap)    

    vmin = np.min(heatmap)
    vmax = np.max(heatmap)
    
    if vmin < 0 and 0 < vmax :
        norm = TwoSlopeNorm(vmin=vmin, vcenter=0, vmax=vmax)
    elif vmin == 0 and 0 < vmax:
        norm = TwoSlopeNorm(vmin=-1, vcenter=0, vmax=vmax)
    elif vmin < 0 and 0 == vmax:
        norm = TwoSlopeNorm(vmin=vmin, vcenter=0, vmax=1)
    elif vmin < 0 and vmax < 0:
        norm = TwoSlopeNorm(vmin=vmin, vcenter=0, vmax=1)
    elif 0 < vmin and 0 < vmax:
        norm = TwoSlopeNorm(vmin=-1, vcenter=0, vmax=vmax)        
    elif vmin == 0 and 0 == vmax:
        print(f"norm == None")
        norm = None

    plt.figure(figsize=(3,3))
    plt.subplots_adjust(left=0,right=1,bottom=0,top=1)
    plt.axis('off')
    plt.imshow(heatmap.tolist(), cmap=my_cmap, interpolation='nearest', norm=norm)

    if out_path == "":
        plt.show()
    else:
        import matplotlib
        matplotlib.use('Agg')
        plt.savefig(str(out_path), dpi=300)
