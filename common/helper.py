import common.paths as paths


###############
#   ILP
###############

import regex
from common.structs import Predicate, Rule, Fact
from typing import Tuple
import pyparsing as pp

LITERAL = "\w+"
PREDICATE_ATOMIC = f"{LITERAL}\({LITERAL}(,\s*{LITERAL})*\)" # predicate with atomic arguments
PRED_ARGS = f"\(({LITERAL}|{PREDICATE_ATOMIC})(,\s*({LITERAL}|{PREDICATE_ATOMIC}))*\)"
PREDICATE = f"{LITERAL}{PRED_ARGS}"

FACT = PREDICATE + '\.'
SINGLE_FACT = '^' + FACT + '$'

BODY = f"{PREDICATE}(\s*,\s*{PREDICATE})*"
RULE = f"({PREDICATE}|{LITERAL})\s*:-\s*{BODY}\."
# RULE = f"{PREDICATE}\s*:-\s*{BODY}\."

SINGLE_RULE = '^' + RULE + '$'


def parse_predicate(pred: str) -> (Predicate or None):
    if regex.match(PREDICATE, pred, flags=regex.DOTALL):
        args = list()
        args_part = regex.search(PRED_ARGS, pred, flags=regex.DOTALL)[0]
        
        matches = regex.finditer(f"({PREDICATE}|{LITERAL})", args_part, flags=regex.DOTALL)
        for match in matches:
            if match == "":
                continue
            if regex.fullmatch(LITERAL, match[0]):
                args.append(match[0])
            else:
                pred_parsed = parse_predicate(match[0])
                args.append(pred_parsed)
        name = pred.replace(args_part, "").replace(".", "")
        return Predicate(name, *args)

def parse_fact(fact: str) -> (Fact or None):
    if regex.match(FACT, fact, flags=regex.DOTALL):
        predicate: Predicate = parse_predicate(fact)
        return Fact(predicate.name, *predicate.args)
    return None

def parse_rule(rule: str) -> (Rule or None): 
    if regex.match(RULE, rule, flags=regex.DOTALL):
        head_str = regex.search(f"{PREDICATE}\s*:-", rule, flags=regex.DOTALL)[0]
        head_pred_str = regex.search(f"{PREDICATE}", head_str, flags=regex.DOTALL)[0]
        head: Predicate = parse_predicate(head_pred_str)

        body = list()
        body_str = regex.search(f":-\s*{BODY}", rule, flags=regex.DOTALL)[0]
        matches = regex.finditer(f"{PREDICATE}", body_str, flags=regex.DOTALL)
        for match in matches:
            pred = parse_predicate(match[0])
            body.append(pred)

        return Rule(head, *body)
    return None


def treat_pyswip_proof_format(proof: str):
    '''
    pyswip messes up order of prolog operators, which makes it hard to parse.
    This is a workaround. Might return correct results only with non-recursive
    literals. Returns a nested list struct.
    '''
    result = []

    opening = pp.Or(["=>(", ",("])

    closing = pp.Forward()
    closing << pp.NoMatch()

    closingStack = []

    def pushClosing(t):
        closingStack.append(closing.expr)
        closing << pp.Literal(")")

    def popClosing():
        closing << closingStack.pop()

    opening.setParseAction(pushClosing)
    closing.setParseAction(popClosing)
    content = "true" | pp.Regex(PREDICATE) | ","
    
    matchedNesting = pp.nestedExpr( opening, closing, content )
    result = matchedNesting.parseString(proof).asList()

    return result

import os
from pathlib import Path
def sample_img_path(samples_dir: str, sample: str) -> str:
    if os.path.exists(samples_dir):
        for root, dir, files in os.walk(samples_dir):
            for file in files:
                if Path(file).stem == sample:
                    return os.path.join(root,file)
    # if os.path.exists(paths.POS(data)):
    #     for file in os.scandir(paths.POS(data)):
    #         if Path(file).stem == sample:
    #             return file.path
    # if os.path.exists(paths.NEG(data)):
    #     for file in os.scandir(paths.NEG(data)):
    #         if Path(file).stem == sample:
    #             return file.path
    raise Exception(f"Original image of sample {sample} couldn't be found in {samples_dir}")

def layer_concept_splitter(placeholder: str):
        '''
            parse a placeholder like 'features_40_c30' and return both the layer-part and concept-part as string
        '''
        placeholder = placeholder.replace('[','').replace(']','')
        splitted = placeholder.split("_")
        layer = '.'.join(splitted[:len(splitted)-1])
        concept = splitted[len(splitted) - 1].replace('c','')
        return layer, int(concept)


import time
from datetime import datetime, timezone
def time_offset_format(from_timestamp: float, format='%H:%M:%S') -> str:
    return datetime.fromtimestamp(time.time()-from_timestamp, tz=timezone.utc).strftime(format)
