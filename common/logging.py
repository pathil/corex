import os
from datetime import datetime
import inspect

class Logger:
    def __init__(self, dir: str = ".") -> None:
        os.makedirs(dir,exist_ok=True)

        now = datetime.now()
        self.path = os.path.join(dir, f"log_{str(now.date())}_{now.time().strftime('%H-%M-%S')}.txt")

    def __caller_name(self, level=3):
        stack = inspect.stack()
        return stack[level][0].f_locals["self"].__class__.__name__

    def __write(self, line: str):
        with open(file=self.path, mode='a') as file:
            file.writelines(line + "\n")

    def __entry(self, prefix: str, line: str, print_out: bool=True):
        entry = f"[{prefix}]\t| [{self.__caller_name()}]: {line}"
        self.__write(entry)

        if print_out:
            print(entry)

    def default(self, line: str, print_out: bool=True):
        self.__write(line)
        if print_out:
            print(line)

    def info(self, line: str, print_out: bool=True):
        self.__entry("INFO", line, print_out)

    def warning(self, line: str, print_out: bool=True):
        self.__entry("WARNING", line, print_out)

    def error(self, line: str, print_out: bool=True):
        self.__entry("ERROR", line, print_out)