import os
from typing import Callable, Dict, List

import torch
import torch.nn as nn
from torchvision.models import VGG, vgg16, VGG16_Weights, VGG16_BN_Weights, resnet50
from torchvision.models.vgg import vgg16_bn
import torchvision.transforms as T

from finetuned_models.med_models import ResNet50  # https://github.com/MedMNIST/MedMNIST

import common.paths as paths

#   vgg16_bn original
def torch_vgg16_bn(device : str) -> VGG:
    model = vgg16_bn(weights=VGG16_BN_Weights.DEFAULT).to(device)
    return model

#   vgg16 original
def torch_vgg16(device : str) -> VGG:
    model = vgg16(weights=VGG16_Weights.DEFAULT).to(device)
    return model

#   teapot-vase finetuned vgg16_bn out 2
def torch_vgg16_bn_teapot_ft(device: str) -> torch.nn.Module:
    n_class = 2
    finetuned_path = os.path.join(paths.MODELS, "vgg16_bn_teapot_ft.pt")
    vgg_finetuned : Dict[str, torch.Tensor] = torch.load(finetuned_path, map_location=device)

    model = vgg16_bn(pretrained=True).to(device)
    model.classifier[-1] = torch.nn.Linear(4096, n_class, bias=True)
    if vgg_finetuned is not None:
        model.load_state_dict(vgg_finetuned)
    return model

#   teapot-vase finetuned vgg16 out 2
def torch_vgg16_adience(device: str) -> torch.nn.Module:
    n_class = 2
    finetuned_path = os.path.join(paths.MODELS, "adience_gender_vgg16.pt")
    vgg_finetuned : Dict[str, torch.Tensor] = torch.load(finetuned_path, map_location=device)

    model = vgg16(pretrained=True)
    model.classifier[-1] = torch.nn.Linear(4096, n_class, bias=True)
    if vgg_finetuned is not None:
        model.load_state_dict(vgg_finetuned)
    return model

#   pathMNIST finetuned resnet50 out 2 (8; not 8)
def pathmnist_resnet50(device: str) -> ResNet50:
    finetuned_path = os.path.join(paths.MODELS, "pathmnist_2_classes_resnet.pth")
    resnet_finetuned : Dict[str, torch.Tensor] = torch.load(finetuned_path, map_location=device)

    resnet: ResNet50 = ResNet50(in_channels=3, num_classes=2)
    
    if resnet_finetuned is not None:
        resnet.load_state_dict(resnet_finetuned)
    
    return resnet

#   vgg16 picasso finetuned (1 class threshold)
#   adapted from Rabold, et. al, slightly modified
#   https://github.com/mc-lovin-mlem/concept-embeddings-and-ilp/tree/ki2020
def picasso_vgg16(device : str) -> VGG:
    """Modify a VGG16 model to have num_classes output classes.
    A VGG16 instance is created (initialized according to pretrained) and modified as follows:
    The last before final linear dense layers is exchanged for one with hidden_dim units in- and
    output, and the number of output classes is set to num_classes.

    :param pretrained: whether to initialize the model with the pretrained VGG16 weights where
        applicable; overridden by state_dict
    :param state_dict: state dict with which to initialize parameters
    :param num_classes: number of output classes of the modified model (no sigmoid applied)
    :param hidden_dim: in- and output dimension of the second dense layer
    :return: the modified VGG instance; all non-modified layers are initialized with the
        pretrained weights if pretrained is True
    """
    finetuned_path = os.path.join(paths.MODELS, "picasso_vgg16.pt")
    vgg_finetuned : Dict[str, torch.Tensor] = torch.load(finetuned_path, map_location=device)

    pretrained: bool = True
    num_classes: int = 1
    hidden_dim: int = 4096
    
    # Add fine-tuning/transfer learning modules
    vgg = vgg16(pretrained=pretrained)
    vgg.classifier[0] = torch.nn.Linear(7 * 7 * 512, hidden_dim)
    vgg.classifier[3] = torch.nn.Linear(hidden_dim, hidden_dim)
    vgg.classifier[6] = torch.nn.Linear(hidden_dim, num_classes)

    if vgg_finetuned is not None:
        vgg.load_state_dict(vgg_finetuned)

    return vgg

class ComposerInfo:
    def __init__(self) -> None:
        self.crop_h = None
        self.crop_w = None
        self.transformer = None
        self.normalizer  =  None

    def get_composer(self, with_normalizer = True):
        if with_normalizer:
            return T.Compose(self.transformer + [self.normalizer])
        return T.Compose(self.transformer)

class ImageNetComposerInfo(ComposerInfo):
    def __init__(self) -> None:
            self.crop_h = 224
            self.crop_w = 224
            self.transformer = [T.Resize(224), T.CenterCrop((224, 224)), T.ToTensor()]
            self.normalizer  =  T.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

class PathMNIStComposerInfo(ComposerInfo):
    def __init__(self) -> None:
        self.crop_h = 28
        self.crop_w = 28
        self.transformer = [T.ToTensor()]
        self.normalizer = T.Normalize(mean=[.5], std=[.5])

COMPOSER : Dict[str, ComposerInfo] = dict(
    imagenet = ImageNetComposerInfo(),
    pathmnist = PathMNIStComposerInfo(),
)

LOADER : Dict[str,Callable[..., torch.nn.Module]] = dict(
    vgg16 = torch_vgg16,
    vgg16_bn = torch_vgg16_bn,
    picasso_finetuned = picasso_vgg16,
    pathmnist_finetuned = pathmnist_resnet50,
    vgg16_adience = torch_vgg16_adience,
    vgg16_bn_teapot_ft = torch_vgg16_bn_teapot_ft,
)


def mask(model: torch.nn.Module, layer_name: str, concepts: List[int], device: str):
    '''
    Only one layer supported. Supports only VGG16 and resnet.
    '''
    layer_split = layer_name.split('.')
    if len(layer_split) < 2 or len(layer_split) > 3:
        raise Exception(f"Not supported model with layer_name {layer_name}")

    if len(layer_split) == 2:   #   VGG16
        #   e.g. 'features.28'
        conv = model._modules[layer_split[0]][int(layer_split[1])]
    elif len(layer_split) == 3:   #   resnet
        #   e.g. 'layer4.2.conv3'
        conv = model._modules[layer_split[0]][int(layer_split[1])]._modules[layer_split[2]]

    weight = conv.weight
    shape_w = weight.shape
    mask_w = torch.ones(shape_w, device=device)

    if len(layer_split) == 2:   #   VGG16 (no bias in the resnet-layer)
        bias = conv.bias
        shape_b = bias.shape
        mask_b = torch.ones(shape_b, device=device)

    if len(layer_split) == 2:   #   VGG16
        for c in concepts:
            mask_w[c] = mask_w[c] * 0
            mask_b[c] = mask_b[c] * 0

    elif len(layer_split) == 3:   #   resnet
        for c in concepts:
            mask_w[c] = mask_w[c] * 0

    model.to(device)
   
    if len(layer_split) == 2:   #   VGG16
        model._modules[layer_split[0]][int(layer_split[1])].weight = torch.nn.Parameter(weight * mask_w)
        model._modules[layer_split[0]][int(layer_split[1])].bias = torch.nn.Parameter(bias * mask_b)
    elif len(layer_split) == 3:   #   resnet
        model._modules[layer_split[0]][int(layer_split[1])]._modules[layer_split[2]].weight = torch.nn.Parameter(weight * mask_w)

    return model
