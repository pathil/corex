import os

from pathlib import Path
import pickle
import numpy as np

from common.config import Config

class FileAccess():
    def __init__(self, path: Path) -> None:
        self.path: Path = path

    def __fspath__(self):
        return str(self)

    def __str__(self) -> str:
        return str(self.path)

    def write(self, data: object) -> None:
        os.makedirs(self.path.parent, exist_ok=True)
        pass
    
    def read(self) -> object:
        pass

class BinaryFileAccess(FileAccess):
    def __init__(self, path: Path) -> None:
        super().__init__(path)

    def write(self, data: object) -> None:
        super().write(data)
        with open(file=self.path, mode='wb') as file:
            pickle.dump(data, file, pickle.HIGHEST_PROTOCOL)
    
    def read(self) -> object:
        with open(file=self.path, mode='rb') as file:
            data: object = pickle.load(file)
        return data

class TextFileAccess(FileAccess):
    def __init__(self, path: Path) -> None:
        super().__init__(path)

    def write(self, data: str) -> None:
        super().write(data)
        with open(file=self.path, mode='w') as file:
            file.write(data)
    
    def read(self) -> str:
        with open(file=self.path, mode='r') as file:
            data: str = file.read()
        return data

class TensorFileAccess(FileAccess):
    def __init__(self, path: Path) -> None:
        super().__init__(path)

    def write(self, data: object) -> None:
        super().write(data)
        from torch import save as torch_save

        torch_save(data, self.path)

    def read(self) -> object:
        from torch import load as torch_load

        return torch_load(self.path)

class NumpyFileAccess(FileAccess):
    def __init__(self, path: Path) -> None:
        super().__init__(path)

    def write(self, data: object) -> None:
        super().write(data)
        np.save(self.path, data)

    def read(self) -> object:
        return np.load(self.path)

class DirNavigator:
    def __init__(self, config: Config) -> None:
        self.config = config

    @classmethod
    def root(cls) -> Path:
        return Path(__file__).absolute().parent.parent

    @classmethod
    def configs(cls) -> Path:
        return cls.root() / "config"

    @classmethod
    def templates(cls) -> Path:
        return cls.root() / "aleph_setup"

    @classmethod
    def interpretation(cls) -> Path:
        return cls.root() / "interpretation"

    def results(self) -> Path:
        return Path(self.config.General.results)

    def concepts(self) -> Path:
        return self.results() / "concepts"

    def sample(self, sample: str) -> Path:
        return self.concepts() / sample

    def layer(self, sample: str, layer: str) -> Path:
        return self.sample(sample) / layer  

    def concept(self, sample: str, layer: str, concept) -> Path:
        return self.layer(sample, layer) / str(concept)

    def ilp(self) -> Path:
        return self.results() / "ilp"
    
    def fv_results(self) -> Path:
        return Path(self.config.FeatureVis.results)

    def explanations(self) -> Path:
        return self.ilp() / "explanations"
    
    def explanation(self, sample: str) -> Path:
        return self.explanations() / sample

    def finetune_data(self) -> Path:
        return Path(self.config.General.dataset)

    @classmethod
    def finetuned_models(cls) -> Path:
        return cls.root() / "finetuned_models"

    def statistics(self) -> Path:
        return self.results() / "experiment_resuls"

class FileNavigator:
    def __init__(self, config: Config, dir_nav: DirNavigator) -> None:
        self.config = config
        self.dir: DirNavigator = dir_nav

    @classmethod
    def config(cls, project: str) -> FileAccess:
        return TextFileAccess(DirNavigator.configs() / (project + ".ini"))

    @classmethod
    def aleph_struct(cls) -> FileAccess:
        return TextFileAccess(DirNavigator.templates() / "aleph_struct.txt")

    # def hplp_struct(self) -> FileAccess:
    #     return TextFileAccess(self.results_dir / "hplp_struct.txt")

    @classmethod
    def tracer(cls) -> FileAccess:
        return TextFileAccess(DirNavigator.interpretation() / "tracer.pl")

    @classmethod
    def proofer(cls) -> FileAccess:
        return TextFileAccess(DirNavigator.interpretation() / "proofer.pl")

    def ground_truth(self) -> FileAccess:
        return BinaryFileAccess(self.dir.results() / "ground_truth.dict")
    
    def model_truth(self) -> FileAccess:
        return BinaryFileAccess(self.dir.results() / "model_truth.dict")

    def rlv_order(self, sample: str, layer: str, masked: bool=False) -> FileAccess:
        return NumpyFileAccess(self.dir.layer(sample, layer) \
                / ("rlv_order_masked.npy" if masked else "rlv_order.npy"))

    def layer_rlvs(self, sample: str, layer: str, masked: bool=False) -> FileAccess:
        return NumpyFileAccess(self.dir.layer(sample, layer) \
                / ("layer_rlvs_masked.npy" if masked else "layer_rlvs.npy"))

    def rlv_map(self, sample: str, layer: str, concept: str) -> FileAccess:
        return TensorFileAccess(self.dir.concept(sample, layer, concept) / "rlv_map.pt")

    def shapes(self, sample: str, layer: str, concept: str) -> FileAccess:
        return BinaryFileAccess(self.dir.concept(sample, layer, concept) / "shapes.list")

    def sample_bg(self, sample: str, masked: bool=False) -> FileAccess:
        return BinaryFileAccess(self.dir.sample(sample) \
                / ("sample_bg_masked.dict" if masked else "sample_bg.dict"))

    def bk_aleph(self, constr: bool=False) -> FileAccess:
        return TextFileAccess(self.dir.ilp() \
                / ("bk_aleph_constr.pl" if constr else "bk_aleph.pl"))

    def aleph_record(self) -> FileAccess:
        return TextFileAccess(self.dir.ilp() / "recordfile.txt")

    def rules(self, constr: bool=False) -> FileAccess:
        return BinaryFileAccess(self.dir.ilp() \
                / ("rules_constr.list" if constr else "rules.list"))

    def labels_db(self) -> FileAccess:
        return TextFileAccess(self.dir.fv_results() / "labels.json")

    def constraints(self) -> FileAccess:
        return BinaryFileAccess(self.dir.ilp() / "constraints.list")

class ProjectNavigator:
    def __init__(self, config: Config) -> None:
        self.config = config
        self.dir = DirNavigator(config)
        self.file: FileNavigator = FileNavigator(config, self.dir)

def config_file(project: str) -> Path:
    return FileNavigator.config(project)

def configs() -> Path:
    return DirNavigator.configs()