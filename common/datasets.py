import os

from torch.utils.data.dataloader import DataLoader, Dataset
import torchvision.transforms as T
from torchvision.datasets import ImageNet

import pandas as pd
from PIL import Image
from pathlib import Path

import common.paths as paths


DATASET = dict(
    adience = lambda path: AdienceGender(path),
    pathmnist = lambda path: PathMNIST(path),
    imagenet = lambda path: ImageNet(path, transform=T.Compose([T.Resize(224), T.CenterCrop((224,224)), T.ToTensor()]), split="val"),
    teapot = lambda path: ImageNet(path, transform=T.Compose([T.Resize(224), T.CenterCrop((224,224)), T.ToTensor()]), split="val"),
    picasso = lambda path: Picasso(path),
)

class Picasso(Dataset):

    def __init__(self, root_dir, transform=None, split="train") -> None:

        super().__init__()

        self.root_dir = root_dir
        self.transform = transform
        self.split = split


        if self.transforms == None:
            self.transforms = [T.Resize(224), T.CenterCrop((224,224)), T.ToTensor()]
            self.transforms.append(T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)))
            self.transforms = T.Compose(self.transforms)

        csv_path = os.path.join(root_dir, "picasso.csv")

        if os.path.exists(csv_path):
            self.csv = pd.read_csv(csv_path)
        else:
            d = dict(sample=[], split=[], class_t=[], path=[])
            
            for root, dir, file in os.walk(root_dir):

                parent, class_t = os.path.split(root)
                _, data_split = os.path.split(parent)

                for file in file:
                    if (file.endswith(".png")):
                        d["sample"].append(file)
                        d["split"].append(data_split)
                        d["class_t"].append(class_t)
                        d["path"].append(os.path.join(split, class_t, file))
                        

                        # print(file + f" - {data_split}, {class_t}")
            
            df = pd.DataFrame(d)
            df.to_csv(csv_path)
        
        
        self.csv = pd.read_csv(csv_path)
        self.csv = self.csv[self.csv["split"].isin([split])]


    def __getitem__(self, index):
        row = self.csv.iloc[index]

        t_path = os.path.join(paths.DATASETS("picasso"), row["split"], row["class_t"], row["sample"])
        image = Image.open(t_path)

        if self.transform:
            image = self.transform(image)
                    
        label = 1.0 if row["class_t"] == "pos" else 0.0
        return image, label


    def __len__(self):

        return len(self.csv)



class PathMNIST(Dataset):

    def __init__(self, path, transform, split="train", normalize=True, one_per_user=False) -> None:
        super().__init__()
        
        self.path = path
        self.split = split

        if split == "train":
            self.csv = pd.read_csv(f"{path}/pathmnist_train.csv", delimiter=",")
        elif split == "test":
            self.csv = pd.read_csv(f"{path}/pathmnist_test.csv", delimiter=",")        


        self.transforms = [T.ToTensor()]

        if normalize:
            self.transforms.append(T.Normalize(mean=[.5], std=[.5]))

        self.transforms = T.Compose(self.transforms)
        self.output_map = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 1, 9: 0}


    def __getitem__(self, index):
        row = self.csv.iloc[index]
        
        t_path = os.path.join(paths.DATASETS("pathmnist"), self.split, row[1])

        image = Image.open(t_path)
        image = self.transforms(image)

        label = row[2]
        label = self.output_map[int(label)]

        return image, label

    def __len__(self):
        return len(self.csv)


class AdienceGender(Dataset):
# adapted based on a first version, that was implemented by Reduan Achtibat
# https://github.com/rachtibat

    def __init__(self, root_dir, transform=None, split="train", normalize=True, one_per_user=False) -> None:
        super().__init__()
        csvs = []
        if split == "train":
            for i in range(4):
                csvs.append(pd.read_csv(os.path.join(root_dir,  f"fold_frontal_{i}_data.txt"), delimiter="\t"))
        elif split == "test":
            csvs.append(pd.read_csv(os.path.join(root_dir, f"fold_frontal_{4}_data.txt"), delimiter="\t"))


        self.csv = pd.concat(csvs)
        self.root_dir = root_dir
        self.transforms = transform
        if self.transforms == None:
            if split=="train":
                self.transforms = [T.RandomAffine(10, (0.1, 0.1), (0.8, 1.2), 5), T.Resize(224), T.CenterCrop((224,224)), T.ToTensor()]
            else:
                self.transforms = [T.Resize(224), T.CenterCrop((224,224)), T.ToTensor()]

            if normalize:
                self.transforms.append(T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)))

            self.transforms = T.Compose(self.transforms)

        self.output_map = {"m": 0, "f": 1}
        # drop all NaN values
        self.csv = self.csv[self.csv["gender"].isin(["m", "f"])]

        if one_per_user:
            self.csv = self.csv.drop_duplicates(subset='face_id', keep="first")


    def __getitem__(self, index):

        row = self.csv.iloc[index]
        t_path = Path(self.root_dir) / "aligned" / row["user_id"] / str("landmark_aligned_face." + str(row["face_id"]) + "." + row["original_image"])

        image = Image.open(t_path)
        image = self.transforms(image)

        label = row["gender"]
        label = self.output_map[label]

        return image, label

    def __len__(self):

        return len(self.csv)



class TeapotVase(Dataset):

    def __init__(self, root_dir, transform, split="train") -> None:

        super().__init__()

        self.root_dir = root_dir
        self.transform = transform
        self.split = split

        csv_path = os.path.join(root_dir, "teapot_vase.csv")

        if os.path.exists(csv_path):
            self.csv = pd.read_csv(csv_path)
        else:
            d = dict(sample=[], split=[], class_t=[], path=[])
            
            for root, dir, file in os.walk(root_dir):

                parent, class_t = os.path.split(root)
                _, data_split = os.path.split(parent)

                for file in file:
                    if (file.endswith(".JPEG")):
                        d["sample"].append(file)
                        d["split"].append(data_split)
                        d["class_t"].append(class_t)
                        d["path"].append(os.path.join(split, class_t, file))
                        
            df = pd.DataFrame(d)
            df.to_csv(csv_path)
        
        
        self.csv = pd.read_csv(csv_path)
        self.csv = self.csv[self.csv["split"].isin([split])]

    def __getitem__(self, index):
        row = self.csv.iloc[index]

        t_path = os.path.join(self.root_dir, row["split"], row["class_t"], row["sample"])
        image = Image.open(t_path)

        if self.transform:
            image = self.transform(image.convert('RGB')) #  grayscales possible

        label = 1 if row["class_t"] == "pos" else 0
        return image, label


    def __len__(self):

        return len(self.csv)

