import abc

from typing import List, Dict, Tuple, Callable
from shapely.geometry.base import BaseGeometry
#   region Relations

#
#   Relation Structs
#
class Relation:
    def __init__(self, name: str, func: Callable, verbal: str, counterpart: str = None) -> None:
        self.name: str = name
        self.verbal: str = verbal
        self.func: Callable[..., bool] = func
        self.counterpart: str = counterpart

class RelationSet(abc.ABC):
    relations: Dict[str,Relation] = dict()

    def __init__(self) -> None:
        pass

    def _create_rels() -> dict():
        raise NotImplementedError("Must be overriden")

    def relation(self, *args: List[BaseGeometry]):
        pass

    def _set_relations(self, value):
        raise AttributeError("attribute is not writable!")


class UnarySet(RelationSet):
    def __init__(self) -> None:
        super().__init__()

    def relation(self, a) -> List[str]:
        pass    

class BinarySet(RelationSet):    
    def __init__(self) -> None:
        super().__init__()

    def relation(self, a: List[BaseGeometry], b: List[BaseGeometry]) -> List[str]:
        pass

def _purge_name_for_prolog(name: str) -> str:
    '''
        Some chars are not allowed in variable names in Prolog, like dots.
        This function replaces them with '_'.
    '''
    replaced = name.replace('.', '_')
    return replaced


#   endregion Relations

#   region Prolog

#
#   Prolog Structs
#

class Predicate:
    def __init__(self, name: str, *args: object):
        self.name = name
        self.args = args

    def to_prolog_str(self) -> str:
        args_str_list = []
        if len(self.args) == 0:
            return self.name
        for arg in self.args:
            if isinstance(arg, Predicate):
                args_str_list.append(arg.to_prolog_str())
            else:
                args_str_list.append(_purge_name_for_prolog(str(arg)))

        args_str = ", ".join(arg for arg in args_str_list)
        return f"{self.name}({args_str})"

class Fact(Predicate):
    def __init__(self, name: str, *args: object):
        super().__init__(name, *args)

    def to_prolog_str(self) -> str:
        pred_str = super().to_prolog_str()
        return pred_str + "."

class Rule:
    def __init__(self, head: Predicate, *body: Predicate):
        self.name = head.name
        self.head = head
        self.body = body

    def to_prolog_str(self, format: bool = False) -> str:
        line_break = "\n  " if format else ""
        head_str = self.head.to_prolog_str()
        body_str = f", {line_break}".join(pred.to_prolog_str() for pred in self.body)
        return f"{head_str}:-{line_break} {body_str}."

#   endregion Prolog

#   region Pipeline

#
#   Pipeline Structs: For communication within Pipeline
#

class PipelineInfo:
    def __init__(self) -> None:
        pass

class SampleInfo(PipelineInfo):
    def __init__(self, sample_name: str, class_mt: int, num_layers: int, path: str="") -> None:
        super().__init__()
        self.name = sample_name
        self.class_mt = class_mt
        self.path = path
        self.num_layers = num_layers

class LayerInfo(PipelineInfo):
    def __init__(self, layer_name: str, sample_info: SampleInfo, num_concepts: int) -> None:
        super().__init__()
        self.name = layer_name
        self.sample_info = sample_info
        self.num_concepts = num_concepts

class ConceptInfo(PipelineInfo):
    def __init__(self, id: int, layer_info: LayerInfo, rlv: float=.0) -> None:
        super().__init__()
        self.id = id
        self.layer_info = layer_info
        self.rlv = rlv

#   endregion Pipeline
