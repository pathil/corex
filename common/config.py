import configparser
from typing import List
from pathlib import Path

"""
Configuration-Object for all user-parameters needed
throughout the pipeline

:param outputFile:  Name of Backgroung-Knowledge-File to be generated
:param posClass:    [obligatory] Class-label of positive samples
:param posSize:     [obligatory] amount of positive samples
:param negClass:    Class-label of positive samples (ignored when useNeg=False)
:param negSize:     amount of positive samples (ignored when useNeg=False)
:param useNeg:      consider negative samples
:param isLocation8: describe Positions of Filters relative to each other using 4(False) or 8(True) Directions
:param foldsCount:  amount of folds in HPLP (ignored when not using ilpType.HPLP)
:param ilpType:     Type of ILP-Framework. Determines which bg-File is being generated and which type of Theory.
:return: -
"""
DEFAULT = Path(__file__).absolute().parent.parent / "default.ini"

class Config:
    def __init__(self, config=None):
        if isinstance(config, configparser.ConfigParser):
            for section in config._sections:
                self.__dict__[section] = Config.__dict__[section](config)                
        else:
            self.General = self.General()
            self.CRP = self.CRP()
            self.Localization = self.Localization()
            self.Relations = self.Relations()
            self.Aleph = self.Aleph()
        
    def set_option_from_config(sectionObj, config):
        if isinstance(config, configparser.ConfigParser):
            sect = config._sections[type(sectionObj).__name__]
            for option in sect:
                sett = config._sections[type(sectionObj).__name__][option]
                setattr(sectionObj, option, sett)                
            return True
        return False

    class General:
        def __init__(self, config=None):            
            if Config.set_option_from_config(self, config) == False:
                self.project = "teapot"
                self.results = "results/experiments/teapot"
                self.samples = "data/samples/teapot/"
                self.dataset = "data/datasets/teapot/"
                self.dataset_loader = "imagenet"
                self.file_ending = "JPEG"
                self.model_pos = 849
                self.model_neg = 883
                self.model = "vgg16_bn"
                self.composer = "imagenet"
            
        @property
        def model_pos(self):
            return self._model_pos

        @model_pos.setter
        def model_pos(self, value):
            self._model_pos = str_to_type(value, int)

        @property
        def model_neg(self):
            return self._model_neg

        @model_neg.setter
        def model_neg(self, value):
            self._model_neg = str_to_type(value, int)

    class CRP:
        def __init__(self, config=None):
            if Config.set_option_from_config(self, config) == False:
                self.layers = "features.40"
                self.filter_quantil = 0.1
            self.model_neg = self.model_neg if hasattr(self, 'model_neg') else None

        @property
        def layers(self):
            return self._layers

        @layers.setter
        def layers(self, value):
            self._layers = str_to_type(value, List[str])

        @property
        def filter_quantil(self):
            return self._filter_quantil

        @filter_quantil.setter
        def filter_quantil(self, value):
            self._filter_quantil = str_to_type(value, float)

    class Localization:
        def __init__(self, config=None):
            if Config.set_option_from_config(self, config) == False:
                self.rel_mask_threshold = .2
                self.alpha_val = .4

        @property
        def rel_mask_threshold(self):
            return self._rel_mask_threshold

        @rel_mask_threshold.setter
        def rel_mask_threshold(self, value):
            self._rel_mask_threshold = str_to_type(value, float)

        @property
        def alpha_val(self):            
            return self._alpha_val

        @alpha_val.setter
        def alpha_val(self, value):
            self._alpha_val = str_to_type(value, float)

    class Relations:
        def __init__(self, config=None):
            if Config.set_option_from_config(self, config) == False:
                self.intersect_tolerance = 20.0
                self.sets = ['compass_align']

        @property
        def intersect_tolerance(self):
            return self._intersect_tolerance

        @intersect_tolerance.setter
        def intersect_tolerance(self, value):
            self._intersect_tolerance = str_to_type(value, float)

        @property
        def sets(self):
            return self._sets

        @sets.setter
        def sets(self, value):
            self._sets = str_to_type(value, List[str])

    class Aleph:
        def __init__(self, config=None):
            if Config.set_option_from_config(self, config) == False:
                self.random_seed = "123abc"
                self.pos_only = False
                self.exclude = "disjoint"

        @property
        def pos_only(self):
            return self._pos_only

        @pos_only.setter
        def pos_only(self, value):
            self._pos_only = str_to_type(value, bool)

        @property
        def exclude(self):
            return self._exclude

        @exclude.setter
        def exclude(self, value):
            self._exclude = str_to_type(value, List[str])


        # @property
        # def verbose(self):
        #     return self._verbose

        # @verbose.setter
        # def verbose(self, value):
        #     self._verbose = str_to_type(value, bool)

    class FeatureVis:
        def __init__(self, config=None):
            if Config.set_option_from_config(self, config) == False:
                self.dataset = 'data/datasets/imagenet/'
                self.results = "results/feature_visualization/imagenet/"

    @staticmethod
    # def load_config(filename : str = os.path.join(os.path.dirname(__file__),"../", CONFIG, FILE_CONFIG)):        
    def load_config(filename : str = DEFAULT):
        config = Config.__read_config(filename)
        if len(config.sections()) == 0:
            Config.write_default_config()
            config = Config.__read_config(DEFAULT)
        return Config(config)

    @staticmethod
    def __read_config(filename):
        config = configparser.ConfigParser()
        config.read(filename)
        return config

    @staticmethod
    def write_default_config():
        config_file = configparser.ConfigParser()

        config = Config()

        for section in config.__dict__:
            config_file.add_section(section)
            for option in config.__dict__[section].__dict__:
                no_leading_under = option
                if option[0] == '_':
                    no_leading_under = option[1:]
                
                config_file.set(
                    section,
                    no_leading_under,
                    str(config.__dict__[section].__dict__[option]))

        with open(Path("../default.ini"), 'w') as configfileObj:
            config_file.write(configfileObj)
            configfileObj.flush()
            configfileObj.close()

def str_to_type(val, type):
    if type == bool:
        return val == 'True'
    elif type == List[int] and isinstance(val, str):
        lst : List[int] = []
        for item in val.split(','):
            lst.append(int(item))
        return lst
    elif type == List[str] and isinstance(val, str):
        lst : List[str] = []
        for item in val.split(','):
            lst.append(item.strip())
        return lst        
    elif isinstance(val, str):
        return type(val)
    return val