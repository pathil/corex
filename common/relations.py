import math
from shapely.geometry import LineString, Point, Polygon
from shapely.geometry.base import BaseGeometry
from typing import Dict, List, Tuple
from common.structs import UnarySet, BinarySet, Relation

#region Sets

class Existence(UnarySet):
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def _create_rels():
        relations = dict()

        rel = "has_a"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A]", \
            func= lambda a: True, \
            counterpart = "")
        
        return relations

    def relation(self, a) -> List[Relation]:
        for rel in Existence.relations:
            if Existence.relations[rel].func(a[1]):
                return [rel]

class Negativity(UnarySet):
    def __init__(self, layer_rlvs) -> None:
        self.layer_rlvs = list(layer_rlvs)
        super().__init__()

    def _create_rels():
        relations = dict()

        rel = "neg"
        relations[rel] = Relation( \
            name=rel, \
            verbal= "[A], that shouldn't be there,", \
            func= lambda a, layer_rlvs: layer_rlvs[a] < 0, \
            counterpart= "pos")

        rel = "pos"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[A]", \
            func= lambda a, layer_rlvs: layer_rlvs[a] > 0, \
            counterpart= "neg")

        return relations

    def relation(self, a) -> List[Relation]:
        for rel in Negativity.relations:
            if Negativity.relations[rel].func(a[0], self.layer_rlvs):
                return [rel]
        return []

class SimpleAlignment(BinarySet):
    def __init__(self) -> None:
        super().__init__()

    def _create_rels():
        relations = dict()
        rel = "left_of"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is left of [A]", \
            func= lambda a, b: a.x > b.x, \
            counterpart= "right_of")

        rel = "right_of"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is right of [A]", \
            func= lambda a, b: a.x < b.x, \
            counterpart= "left_of")
        
        rel = "above_of"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is above of [A]", \
            func= lambda a, b: a.y > b.y, \
            counterpart= "below_of")
        
        rel = "below_of"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is below of [A]", \
            func= lambda a, b: a.y < b.y, \
            counterpart= "above_of")

        return relations

    def relation(self, a: List[BaseGeometry], b: List[BaseGeometry]) -> List[Relation]:
        rels = []
        for rel in SimpleAlignment.relations:
            if SimpleAlignment.relations[rel].func(a[0].centroid, b[0].centroid):
                rels.append(rel)
        return rels

class CompassAlign(BinarySet):
    ROT_STEP = 45/2
    #   East == 0°
    ANGLES: Dict[str,int] = dict(
        ESE    = 0   + ROT_STEP,
        SSE    = 45  + ROT_STEP,
        SSW    = 90  + ROT_STEP,
        WSW    = 135 + ROT_STEP,
        WNW    = 180 + ROT_STEP,
        NNW    = 225 + ROT_STEP,
        NNE    = 270 + ROT_STEP,
        ENE    = 315 + ROT_STEP,
    )

    ORIENTATION: Dict[str,int] = dict(
        SE    = ('ESE', 'SSE'),
        S     = ('SSE', 'SSW'),
        SW    = ('SSW', 'WSW'),
        W     = ('WSW', 'WNW'),
        NW    = ('WNW', 'NNW'),
        N     = ('NNW', 'NNE'),
        NE    = ('NNE', 'ENE'),
        E     = ('ENE', 'ESE'),
    )

    def __init__(self, tolerance) -> None:
        super().__init__()
        self.tolerance = tolerance

    def _create_rels():
        relations = dict()

        rel = "middle_right"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is middle right to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('E',a,b), \
            counterpart= "middle_left")

        rel = "bottom_right"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is bottom right to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('SE',a,b), \
            counterpart= "top_left")

        rel = "bottom_middle"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is middle bottom to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('S',a,b), \
            counterpart= "top_middle")

        rel = "bottom_left"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is bottom left to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('SW',a,b), \
            counterpart= "top_right")

        rel = "middle_left"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is middle left to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('W',a,b), \
            counterpart= "middle_right")

        rel = "top_left"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is top left to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('NW',a,b), \
            counterpart= "bottom_right")

        rel = "top_middle"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is middle top to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('N',a,b), \
            counterpart= "bottom_middle")

        rel = "top_right"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is top right to [A]", \
            func= lambda a,b,t: CompassAlign.oriented('NE',a,b), \
            counterpart= "bottom_left")

        rel = "center"
        relations[rel] = Relation( \
            name= rel, \
            verbal= "[B] is centered on [A]", \
            func= lambda a,b,t: a[0].centroid.buffer(t).intersects(b[0].centroid), \
            counterpart= "")
        
        return relations

    @staticmethod
    def oriented(orientation: str, a: List[BaseGeometry], b: List[BaseGeometry]):
        p_a = a[0].centroid
        p_b = b[0].centroid

        angle_1, angle_2 = CompassAlign.ORIENTATION[orientation]
        l_1 = CompassAlign.get_line(p_a.x, p_a.y, CompassAlign.ANGLES[angle_1], 5000)   #   5000: make sure it exceeds image border
        l_2 = CompassAlign.get_line(p_a.x, p_a.y, CompassAlign.ANGLES[angle_2], 5000)

        _, end_1 = l_1.boundary.geoms
        _, end_2 = l_2.boundary.geoms
        poly = Polygon((p_a, end_1, end_2))

        # if poly.contains(p_b):    this when only one?
        if poly.intersects(b[0]):
            return True
        return False

    @staticmethod
    def get_line(start_x: float, start_y: float, angle_degree: float, length: float) -> LineString:
        start = Point(start_x, start_y)
        angle_rad = math.radians(angle_degree)
        end = Point(start.x + length * math.cos(angle_rad),
                    start.y + length * math.sin(angle_rad))
        return LineString([start, end])

    def relation(self, a: List[BaseGeometry], b: List[BaseGeometry]) -> List[Relation]:
        rels = []
        for rel in CompassAlign.relations:
            if CompassAlign.relations[rel].func(a, b, self.tolerance):
                rels.append(rel)
        return rels

class NineIM(BinarySet):
    def __init__(self) -> None:
        super().__init__()

    def _create_rels():
        relations = dict()

        rel = "disjoint"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] is disjoint of [B]", \
            func= lambda a, b: a.disjoint(b), \
            counterpart = "disjoint")
        
        rel = "equals"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] equals [B]", \
            func= lambda a, b: a.equals(b), \
            counterpart = "equals")

        rel = "touches"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] touches [B]", \
            func= lambda a, b: a.touches(b), \
            counterpart = "touches")

        rel = "overlaps"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] overlaps [B]", \
            func= lambda a, b: a.overlaps(b), \
            counterpart = "overlaps")

        rel = "covers"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] covers [B]", \
            func= lambda a, b: a.covers(b), \
            counterpart = "covered_by")

        rel = "contains"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] contains [B]", \
            func= lambda a, b: a.contains(b), \
            counterpart = "within")

        rel = "covered_by"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] is covered by [B]", \
            func= lambda a, b: a.covered_by(b), \
            counterpart = "covers")

        rel = "within"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] is within [B]", \
            func= lambda a, b: a.within(b), \
            counterpart = "contains")

        return relations

    def relation(self, a: List[BaseGeometry], b: List[BaseGeometry]) -> List[Relation]:
        for rel in NineIM.relations:
            if NineIM.relations[rel].func(a[0], b[0]):
                return [rel]
        return ["topology_error"] # but should always be disjoint if nothing found
    
class Distance(BinarySet):
    def __init__(self, img_shape: Tuple[int,int]) -> None:
        super().__init__()
        self.max_range = max(img_shape[0], img_shape[1]) * 0.20     #  everything closer than 1/4 of the longer side of an img is considered close
        
    def _create_rels():
        relations = dict()
        rel = "close_to"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[A] is close to [B]", \
            func= lambda a, b, range: a.distance(b) < range, \
            counterpart = "close_to")
        
        return relations

    def relation(self, a: List[BaseGeometry], b: List[BaseGeometry]) -> List[Relation]:
        for rel in Distance.relations:
            if Distance.relations[rel].func(a[0], b[0], self.max_range):
                return [rel]
        return []

class Amid(BinarySet):
    def __init__(self, tolerance) -> None:
        super().__init__()
        self.tolerance = tolerance

    def _create_rels():
        relations = dict()
        rel = "amid_x"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[B] is horizontally surrounded by [A]", \
            func= (lambda a_1, a_2, b, t: ((a_1.x + t/2) < b.x and (a_2.x - t/2) > b.x) \
                                        or ((a_1.x - t/2) > b.x and (a_2.x + t/2) < b.x)) , \
            counterpart = "")

        rel = "amid_y"
        relations[rel] =  Relation( \
            name= rel, \
            verbal= "[B] is vertically surrounded by [A]", \
            func= (lambda a_1, a_2, b, t: ((a_1.y + t/2) < b.y and (a_2.y - t/2) > b.y) \
                                        or ((a_1.y - t/2) > b.y and (a_2.y + t/2) < b.y)), \
            counterpart = "")

        return relations

    def relation(self, a: List[BaseGeometry], b: List[BaseGeometry]) -> List[Relation]:
        rels = []
        if len(a) >= 2:
            for rel in Amid.relations:
                if Amid.relations[rel].func(a[0].centroid, a[1].centroid, b[0].centroid, self.tolerance):
                    rels.append(rel)
        return rels

#endregion Sets

def set_relations(sets: List[str]) -> Dict[str,Relation]:
    sets = [x.lower() for x in sets]
    preds = dict()

    preds['unary'] = []
    preds['unary'] = preds['unary'] + list(Existence.relations.values())
    # if 'negativity' in sets:
        # preds['unary'] = preds['unary'] + list(Negativity.relations.keys())
    # preds['unary'] = preds['unary'] + list(Negativity.relations.keys())
    preds['binary'] = []
    if 'simple_align' in sets:
        preds['binary'] = preds['binary'] + list(SimpleAlignment.relations.values())
    if 'compass_align' in sets:
        preds['binary'] = preds['binary'] + list(CompassAlign.relations.values())
    if 'nine_im' in sets:
        preds['binary'] = preds['binary'] + list(NineIM.relations.values())
    if 'distance' in sets:
        preds['binary'] = preds['binary'] + list(Distance.relations.values())
    if 'amid' in sets:
        preds['binary'] = preds['binary'] + list(Amid.relations.values())

    return preds

def resolve_in_nl(relation):
    for set in [Existence, Negativity, SimpleAlignment, CompassAlign, NineIM, Distance, Amid]:
        nl: Relation = set.relations.get(relation)
        if nl != None:
            return nl.verbal
    return None

for set in [Existence, Negativity, SimpleAlignment, CompassAlign, NineIM, Distance, Amid]:
    set.relations = set._create_rels()
