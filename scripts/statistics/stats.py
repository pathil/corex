import os
import sys
sys.path.append(os.getcwd())

import pickle
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from typing import List, Dict
import shutil

from common.paths import ProjectNavigator, DirNavigator, FileNavigator
from common.relations import set_relations, resolve_in_nl
from common.helper import sample_img_path, layer_concept_splitter
from common.config import Config
from common.structs import Rule, Fact, Predicate
from interpretation.interpretation import Interpretation

def gather_bg():
    bg_dict = dict()
    
    for root, samples_dirs, files in os.walk(pn.dir.concepts()):
        for sample in tqdm(samples_dirs, desc=f"collect samples background", leave=False):
            bg_dict[sample] = pn.file.sample_bg(sample).read()
        break
    return bg_dict

def write_positives_rules(config: Config, bg_dict: dict):
    predicates = set_relations(config.Relations.sets)

    rules: List[Rule] = pn.file.rules().read()

    samples_dict = pn.file.model_truth().read()

    out = pn.dir.ilp()

    sample_rules = dict()
    for sample in tqdm(samples_dict["pos"], "write positive rules"):
        ip = Interpretation(sample, bg_dict[sample], predicates, rules, out)
        claused_rules: List[Rule] = ip.explain()
        sample_rules[sample] = [r.head.name for r in claused_rules]

    #   write rules_per_sample
    with open(pn.dir.statistics() / "rules_per_sample.txt", 'w') as file:
        lines = [f"{s}: {sample_rules[s]}\n" for s in sample_rules]
        file.writelines(lines)

    with open(pn.dir.statistics() / "rules_per_sample.dict", 'wb') as file:
        pickle.dump(sample_rules, file, pickle.HIGHEST_PROTOCOL)


    ##     samples_per_rule
    samples_per_rule = dict()
    for rule in rules:
        samples_per_rule[rule.head.name] = []
    samples_per_rule["not_covered"] = []

    for sample in sample_rules:
        if len(sample_rules[sample]) == 0:
            samples_per_rule["not_covered"].append(sample)
        for rule_name in sample_rules[sample]:
            if rule_name in samples_per_rule:
                samples_per_rule[rule_name].append(sample)
            else:
                raise Exception(f"Sample {sample} has a rule {rule_name}, that shouldnt exist.")

    with open(pn.dir.statistics() / "samples_per_rule.txt", 'w') as file:
        lines = [f"{r} ({len(samples_per_rule[r])} covered): {samples_per_rule[r]}\n" for r in samples_per_rule]
        file.writelines(lines)

    with open(pn.dir.statistics() / "samples_per_rule.dict", 'wb') as file:
        pickle.dump(samples_per_rule, file, pickle.HIGHEST_PROTOCOL)

    ##  rules_coverage
    with open(pn.dir.statistics() / "rules.txt", 'w') as file:
        lines = [f"Rule [{r.head.name.removeprefix('is_class_')}] :     ({len(samples_per_rule[r.head.name])} covered):\n{r.to_prolog_str(True)}\n" for r in rules]
        file.writelines(lines)


def write_negative_traces(config: Config, bg_dict: dict):
    predicates = set_relations(config.Relations.sets)

    samples_dict = dict()    
    samples_dict = pn.file.model_truth().read()

    out = pn.dir.ilp()

    with open(rules = pn.file.rules(), mode='rb') as file:
        rules_iter: List[Rule] =  pickle.load(file)

    rules_traces = dict()
    for r_idx in tqdm(range(len(rules_iter)), "write negative traces"):
        for sample in tqdm(samples_dict["neg"], leave=False):
            with open(rules = pn.file.rules(), mode='rb') as file:
                rules: List[Rule] =  pickle.load(file)

            ip = Interpretation(sample, bg_dict[sample], predicates, rules, out)
            
            if not ip.rules[r_idx].head.name in rules_traces.keys():
                rules_traces[ip.rules[r_idx].head.name] = []
            
            trace = ip.trace(r_idx)
            if len(trace) != 0:
                misses = [f[-2] for f in trace]
                rules_traces[rules[r_idx].head.name].append((sample, misses))
            else: 
                rules_traces[rules[r_idx].head.name].append((sample, []))

    lines = ""
    for rule in rules_traces:
        section = rule + "\n"
        traces = rules_traces[rule]
        section += "\n".join([f"{traces[i][0]}: {traces[i][1]}" for i in range(len(traces))])
        lines += section + "\n\n"
    #   write
    with open(pn.dir.statistics() / "trace_per_rule.txt", 'w') as file:
        file.writelines(lines)
    with open(pn.dir.statistics() / "trace_per_rule.dict", mode='wb') as file:
        pickle.dump(rules_traces, file, pickle.HIGHEST_PROTOCOL)

def distinct_concepts(rules: List[Rule]):
    # retrieve concepts from rules
    concepts = []
    for rule in rules:
        for predicate in rule.body:
            for arg in predicate.args:
                if arg == "A":
                    continue
                if type(arg) == Predicate:
                    for pos_neg_arg in arg.args:
                        if pos_neg_arg == "A":
                            continue
                        _, c = layer_concept_splitter(pos_neg_arg)
                        if not c in concepts:
                            concepts.append(c)
                elif type(arg) == str:
                    _, c = layer_concept_splitter(arg)
                    if not c in concepts:
                        concepts.append(c)
                else:
                    raise Exception(f"Unexpected arg-type: {type(arg)=}")
    return concepts

def rankings(proj_path: str, layer: str):
    # für ein concept, wo im ranking
    rules: List[Rule] = pn.file.rules().read()
    
    with open(pn.dir.statistics() / "samples_per_rule.dict", mode="rb") as file:
        samples_per_rule: dict = pickle.load(file)

    ##  select Top 3 rules
    top_3: List[Rule] = []
    for i in range(min(3, len(samples_per_rule) - 1)): # -1: minus [not_covered]
        best = ("", 0)
        for rule_ident in samples_per_rule:
            if rule_ident == "not_covered":
                continue
            if len(samples_per_rule[rule_ident]) > best[1]:
                best = (rule_ident, len(samples_per_rule[rule_ident]))
        
        top_3.append([rule for rule in rules if rule.head.name == best[0]][0])
        samples_per_rule.pop(best[0])
        best = ("", 0)
    
    for case in ["all", "top_3"]:
        if case == "all":
            concepts = distinct_concepts(rules)
        elif case == "top_3":
            concepts = distinct_concepts(top_3)

        print(f"{concepts=}")
        samples_dict = pn.file.model_truth().read()

        ranks = dict()
        for c in concepts:
            ranks[c] = np.zeros(2048, dtype=int)

        # alle layer rels von allen samplen holen
        for sample in tqdm(samples_dict["pos"], "write rankings"):
            layer_rels = pn.file.layer_rlvs(sample, layer).read()
            abs_sorted = np.argsort(np.abs(layer_rels))[::-1]

            for c in concepts:
                rank = np.where(abs_sorted==c)
                ranks[c][rank] += 1

        if case == "all":
            #   write
            lines = ""
            for c in ranks:
                lines += "Concept: " + str(c) + "\n"
                for r in np.argsort(ranks[c])[::-1]:
                    count = ranks[c][r]
                    if count == 0:
                        break
                    lines += f"{r}: {count}\n"

            with open(pn.dir.statistics() / "concept_rankings.txt", 'w') as file:
                file.writelines(lines)

            with open(pn.dir.statistics() / "concept_rankings.dict", mode='wb') as file:
                pickle.dump(ranks, file, pickle.HIGHEST_PROTOCOL)

        output_path = pn.dir.statistics() / f"plt_rankings_{case}_{'_'.join(str(c) for c in concepts[:6])}.png"
        save_plot_rankings(concepts, ranks, output_path)

def save_plot_rankings(concepts: List[int], ranks: dict, out_path: str):
    x = 52
    datas = []
    for c in concepts:
        datas.append(ranks[c][:x])

    width = 1 / len(datas)

    labels = range(0,x)
    fig = plt.figure() 
    fig.set_figwidth(12)
    plt.xticks(range(len(datas[0])), labels)
    plt.xlabel('Rank')
    plt.ylabel('Amounts')
    plt.title(f'Rankings concepts {", ".join(str(c) for c in concepts)}')

    offset = lambda x: (width /2) + (width * x)
    for i in range(len(datas)):
        plt.bar(np.arange(len(datas[i])) + offset(i), datas[i], width=width)

    plt.xlim(left=0, right=x)
    plt.savefig(str(out_path), dpi=300)

def cluster(config: Config):
    with open(pn.dir.statistics() / "rules_per_sample.dict", 'rb') as file:
        rps = pickle.load(file)

    classes = dict()
    for sample in tqdm(rps, "write cluster: "):
        rules = ", ".join(rps[sample])
        if not rules in classes:
            classes[rules] = [sample]
        else:
            classes[rules].append(sample)

    classes = {key: val for key, val in sorted(classes.items(), key = lambda ele: len(ele[1]), reverse=True)}
    
    lines = ""
    for c in classes:
        if c == "":
            lines += f"[Not covered] (Total: {len(classes[c])})\n"
        else:
            lines += f"{c} (Total: {len(classes[c])})\n"
        lines += "\n".join(s for s in classes[c])
        lines += "\n"

    with open(pn.dir.statistics() / "subclasses.txt", 'w') as file:
        file.writelines(lines)
    with open(pn.dir.statistics() / "subclasses.dict", mode='wb') as file:
        pickle.dump(classes, file, pickle.HIGHEST_PROTOCOL)

    output_path = pn.dir.statistics() / f'plt_cluster.png'
    save_plot_clusters(classes, output_path, f'Powerset for \'{config.General.project}\'')

def save_plot_clusters(subclasses: dict, out_path: str, title: str):
    data = list()

    for item in list(subclasses.items()):
        data.append(len(item[1]))

    x = min(len(data), 50)
    data = data[:x]

    labels = list()
    for key in subclasses.keys():
        if key == "":
            labels.append("{}")
        else:
            labels.append('{' + key.replace('is_class_', 'r').replace(', ', ',') + '}')

    fig = plt.figure() 
    fig.set_figwidth(10)
    plt.xticks(range(x), labels[:x], rotation=80)
    plt.xlabel('Powerset')
    plt.ylabel('Count samples')
    plt.title(title)

    plt.bar(np.arange(len(data)), data)
    fig.tight_layout()
    plt.savefig(str(out_path), dpi=300)

def out_rules_polys(config: Config):
    from shapely.geometry import Polygon
    from PIL import Image
    from common.loader import COMPOSER
    from common.analysis import plot_shapes
    import torchvision.transforms as transforms

    proj_path = config.General.results + config.General.project
    
    rules: List[Rule] = pn.file.rules().read()

    with open(pn.dir.statistics() / "samples_per_rule.dict", 'rb') as file:
        spr = pickle.load(file)

    for i in tqdm(range(len(rules)), desc="If existing, save visualization of Rule", leave=True):
        rule = rules[i]
        p = pn.dir.statistics() / "rules_shapes" / rule.head.name
        os.makedirs(p, exist_ok=True)
        
        for sample in tqdm(spr[rule.head.name], desc="for covered sample", leave=False):
            polys: List[Polygon] = []
            labels: List[str] = []
            colors: List[str] = []
            img_path = sample_img_path(config.General.samples, sample)
            img = Image.open(img_path)

            for pred in rule.body:
                for pos_neg_pred in pred.args[1:]:

                    if type(pos_neg_pred) == Predicate:
                        for pos_neg_arg in pos_neg_pred.args:
                            if pos_neg_arg == "A":
                                continue
                            for var in pos_neg_pred.args[1:]:
                                layer, concept = layer_concept_splitter(var)
                                
                                shapes = pn.file.shapes(sample, layer, concept).read()

                                polys.append(shapes[0]) # only first
                                label = var     #   resolve like responder?
                                labels.append(label)

                                l_rels = pn.file.layer_rlvs(sample, layer).read()
                                colors.append("red") if l_rels[concept] > 0 else colors.append("blue")

                    elif type(pos_neg_pred) == str:
                        layer, concept = layer_concept_splitter(pos_neg_pred)
                        shapes = pn.file.shapes(sample, layer, concept).read()

                        polys.append(shapes[0]) # only first
                        label = pos_neg_pred     #   resolve like responder?
                        labels.append(label)

                        l_rels = pn.file.layer_rlvs(sample, layer).read()
                        colors.append("red") if l_rels[concept] > 0 else colors.append("blue")
                    else:
                        raise Exception(f"Unexpected arg-type: {type(pos_neg_pred)=}")

            composer = COMPOSER[config.General.composer]
            t_img = composer.get_composer(False)(img)
            img : Image = transforms.ToPILImage()(t_img)
            height = composer.crop_h
            width = composer.crop_w
            out_path = p / f"{sample}_r{i + 1}.png"
            plot_shapes(polys=polys, img=img, ylim=height, xlim=width, labels=labels, colors=colors, single_color=False, fill=False, out_path=out_path)


def out_concepts_rlv_map(layer: str):
    from torch import load as torch_load
    from common.analysis import plot_heatmap
    
    rules: List[Rule] =  pn.file.rules().read()

    model_truth = pn.file.model_truth().read()
    
    concepts = distinct_concepts(rules)

    for concept in tqdm(concepts, desc="heatmaps for theory concepts"):
        os.makedirs(pn.dir.statistics() / "concepts_rlv_map" / str(concept), exist_ok=True)
        
        for sample in tqdm(model_truth["pos"], desc="(model) positive samples", leave=False):        
            rlv_map_path = pn.file.rlv_map(sample, layer, concept)
            if not os.path.exists(rlv_map_path):
                continue

            hm = rlv_map_path.read().cpu().numpy()
            
            out = pn.dir.statistics() / "concepts_rlv_map" / str(concept) / f"{sample}_{layer}_c{concept}.png"
            plot_heatmap(hm, out)


def faithfulness(config: Config, bg_dict: dict):
    ground_truth = pn.file.ground_truth().read()
    model_truth = pn.file.model_truth().read()

    d = len(ground_truth["pos"]) + len(ground_truth["neg"])
    
    info = f"samples total: D = {d}\n"
    gt_len_pos = str(len(ground_truth["pos"]))
    gt_len_neg = str(len(ground_truth["neg"]))
    info += f"ground truth: pos = {gt_len_pos}, neg = {gt_len_neg}\n"
    info += f"pos: " + str([x for x in ground_truth["pos"]]) + "\n"
    info += f"neg: " + str([x for x in ground_truth["neg"]]) + "\n"

    mt_len_pos = len(model_truth["pos"])
    mt_len_neg = len(model_truth["neg"])
    info += f"model truth: pos = {mt_len_pos}, neg = {mt_len_neg}\n"
    info += f"pos: " + str([x for x in model_truth["pos"]]) + "\n"
    info += f"neg: " + str([x for x in model_truth["neg"]]) + "\n"
    
    predicates = set_relations(config.Relations.sets)

    explainer_truth = dict(pos=[], neg=[])
    failings = dict(pos=[], neg=[])

    for x in ["pos", "neg"]:
        for sample in tqdm(model_truth[x], f"check explainer truth for (model) {x}"):
            rules: List[Rule] =  pn.file.rules().read()

            ip = Interpretation(sample, bg_dict[sample], predicates, rules, pn.dir.ilp())
            rules = ip.explain()
            if x =="pos" and len(rules) > 0:
                explainer_truth["pos"].append(sample)
            elif x == "pos" and len(rules) == 0:
                failings["neg"].append(sample)
            elif x == "neg" and len(rules) == 0:
                explainer_truth["neg"].append(sample)
            elif x == "neg" and len(rules) > 0:
                failings["pos"].append(sample)
            else:
                raise Exception("...")

    et_len_pos = len(explainer_truth["pos"])
    et_len_neg = len(explainer_truth["neg"])
    info += f"explainer truth: TP = {et_len_pos}, TN = {et_len_neg} (w.r.t. model truth)\n"
    info += f"TP: " + str([x for x in explainer_truth["pos"]]) + "\n"
    info += f"TN: " + str([x for x in explainer_truth["neg"]]) + "\n"

    failings_len_pos = len(failings["pos"])
    failings_len_neg = len(failings["neg"])
    info += f"explainer truth: FP = {failings_len_pos}, FN = {failings_len_neg}\n"
    info += f"FP: " + str([x for x in failings["pos"]]) + "\n"
    info += f"FN: " + str([x for x in failings["neg"]]) + "\n"

    faith_score = (et_len_pos + et_len_neg) / d

    info += f"faithfulness = {faith_score}\n"
    info += f"\n"

    with open(pn.dir.statistics() / "faithfulness.txt", 'w') as file:
        lines = info
        file.writelines(lines)

def all_concepts_in_bg(config: Config, bg_dict: dict = None) -> list:
    proj_path = config.General.results +  config.General.project

    if bg_dict == None:
        bg_dict = gather_bg(proj_path)
    concepts = []
    for sample in tqdm(bg_dict):
        for layer in bg_dict[sample]:
            for rel in bg_dict[sample][layer]["unary"]:
                if not rel[1] in concepts:
                    concepts.append(rel[1])
    lines = ""
    lines += f"{len(concepts)} concepts in BG\n\n"
    
    for c in [f"{str(x)}\n" for x in concepts]:
        lines += c
    with open(pn.dir.statistics() / "concepts_bg.txt", 'w') as file:
        file.writelines(lines)
    with open(pn.dir.statistics() / "concepts_bg.list", 'wb') as file:
        pickle.dump(concepts, file, pickle.HIGHEST_PROTOCOL)
    return concepts

def ref_sampling(config: Config):
    import torch
    
    from crp.attribution import CondAttribution
    from crp.concepts import ChannelConcept
    from crp.helper import get_layer_names
    from crp.visualization import FeatureVisualization
    from zennit.canonizers import SequentialMergeBatchNorm
    from zennit.composites import EpsilonPlusFlat
    from crp.image import imgify, vis_opaque_img
    
    from common.loader import LOADER, COMPOSER
    from common.datasets import DATASET
    from matplotlib import pyplot as plt
    
    rules: List[Rule] =  pn.file.rules().read()

    dist_concepts = distinct_concepts(rules)

    sample_id = 0

    layer_name = config.CRP.layers
    mode = "relevance"
    n_refimgs = 14
    n_concepts = 15

    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    model : torch.nn.Module = LOADER[config.General.model](device).to(device)
    model.eval()

    dataset = DATASET[config.General.dataset_loader](config.FeatureVis.dataset)

    comp_info = COMPOSER[config.General.composer]
    preprocessing_fn = comp_info.normalizer

    canonizers = [SequentialMergeBatchNorm()]
    composite = EpsilonPlusFlat(canonizers)
    cc = ChannelConcept()

    layer_names = get_layer_names(model, [torch.nn.Conv2d])
    layer_map = {layer: cc for layer in layer_names}

    attribution = CondAttribution(model)

    cache = None #ImageCache()

    fv = FeatureVisualization(attribution, dataset, layer_map, preprocess_fn=preprocessing_fn,
                                path=config.FeatureVis.results, cache=cache)

    target = config.General.model_pos
    data_p, target = fv.get_data_sample(sample_id, preprocessing=True)

    attr = attribution(data_p.requires_grad_(),
                    [{"y": target}],
                    composite,
                    record_layer=[layer_name])

    channel_rels = cc.attribute(attr.relevances[layer_name], abs_norm=True)

    topk = torch.topk(channel_rels[0], n_concepts)
    topk_ind = topk.indices.detach().cpu().numpy()
    topk_rel = topk.values.detach().cpu().numpy()

    print(topk)
    topk_ind = dist_concepts
    n_concepts = len(topk_ind)

    # conditional heatmaps
    conditions = [{"y": target, layer_name: c} for c in topk_ind]
    cond_heatmap, _, _, _ = attribution(data_p.requires_grad_(), conditions, composite)


    ref_imgs = fv.get_max_reference(topk_ind, layer_name, mode, (0, n_refimgs), composite=composite, rf=True, plot_fn=vis_opaque_img)

    fig, axs = plt.subplots(n_concepts, 2 + n_refimgs, figsize=(2 * n_refimgs, 1.6 * n_concepts)) 

    for r, row_axs in enumerate(axs):

        for c, ax in enumerate(row_axs):
            if c == 0:
                if r == 0:
                    ax.set_title("input")
                    img = imgify(fv.get_data_sample(sample_id, preprocessing=False)[0][0])
                    ax.imshow(img)
                if r == 1:
                    ax.set_title("heatmap")
                    img = imgify(attr.heatmap, cmap="bwr", symmetric=True)
                    ax.imshow(img)

            if c == 1:
                if r == 0:
                    ax.set_title("conditional heatmap")
                ax.imshow(imgify(cond_heatmap[r], symmetric=True, cmap="bwr", padding=True))
                ax.set_ylabel(f"concept {topk_ind[r]}\n relevance: {(topk_rel[r]*100):2.1f}%")

            elif c >= 2:
                if r == 0 and c==2:
                    ax.set_title("concept")

                img = imgify(ref_imgs[topk_ind[r]][c-2], padding=True)
                ax.imshow(img)
                ax.yaxis.set_label_position("right")

            ax.set_xticks([])
            ax.set_yticks([])

    plt.tight_layout()
    plt.show()
    p = pn.dir.statistics()
    print(p)
    os.makedirs(p, exist_ok=True)
    p = p / f"sample_{sample_id}_wrt_{target}_crp_{mode}.png"
    print(p)
    plt.savefig(str(p), dpi=300)

def main(args):
    config = Config.load_config(FileNavigator.config(args.project))

    global pn
    pn = ProjectNavigator(config)

    proj_path = pn.dir.results()

    pn.dir.statistics().mkdir(exist_ok=True)

    bg_dict = gather_bg()

    if os.path.exists(pn.dir.statistics() / "concepts_bg.list") == False:
        all_concepts_in_bg(config, bg_dict)

    record_file = pn.file.aleph_record()

    shutil.copyfile(record_file, pn.dir.statistics() / os.path.basename(record_file))
    config_file = FileNavigator.config(config.General.project)
    shutil.copyfile(config_file, pn.dir.statistics() / os.path.basename(config_file))

    # #   each rule that is true to each positive samples
    if os.path.exists(pn.dir.statistics() / "rules_per_sample.dict") == False:
        write_positives_rules(config, bg_dict)

    #   traces on all negative samples for each rule induced with positives
    # write_negative_traces(config, bg_dict)

    #   count on which ranking for all samples a concept is listed by absolute relevance
    if os.path.exists(pn.dir.statistics() / "concept_rankings.dict") == False:
        rankings(proj_path, config.CRP.layers[0])

    #   show distribution of valid rules for all samples (find subclasses)
    if os.path.exists(pn.dir.statistics() / "subclasses.dict") == False:
        cluster(config)

    #   get filter relevance Map and Rule Polygons for covered examples
    if os.path.exists(pn.dir.statistics() / "rules_shapes") == False:
        out_rules_polys(config)

    if os.path.exists(pn.dir.statistics() / "concepts_rlv_map") == False:
        out_concepts_rlv_map(config.CRP.layers[0])

    if os.path.exists(pn.dir.statistics() / "faithfulness.txt") == False:
        faithfulness(config, bg_dict)

    # ref_sampling(config)

import argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect concept predictions for selected samples.')
    parser.add_argument('-p', '--project', type=str,
                    choices=list(x.replace(".ini","") for x in os.listdir(DirNavigator.configs())),
                    required=True,
                    help='The identifier of the model to conduct an analysis for.')

    args : argparse.Namespace = parser.parse_args()
    
    main(args)
