import os
import sys
sys.path.append(os.getcwd())

import argparse
from typing import List

import torch
from torch.utils.data.dataloader import DataLoader
from tqdm import tqdm
import pickle

from common.config import Config
import common.paths as paths
from common.datasets import Picasso, PathMNIST, AdienceGender, TeapotVase
from common.loader import COMPOSER, LOADER
from common.structs import Rule
from stats import distinct_concepts, all_concepts_in_bg
from pipeline.preprocessing import mask


softmax = torch.nn.Softmax(dim=-1)

def prediction(outputs, model_name):

    if model_name == "vgg16_adience":
        preds = softmax(outputs)
        preds = torch.argmax(preds, dim=-1)
    elif model_name == "pathmnist_finetuned":
        # _, preds = torch.max(outputs, 1)
        preds = softmax(outputs)
        preds = torch.argmax(preds, dim=-1)
    elif model_name == "picasso_finetuned":
        preds = (torch.sigmoid(outputs) > 0.5).squeeze(1).float()
    elif model_name == "pathmnist_finetuned":
        # _, preds = torch.max(outputs, 1)
        preds = softmax(outputs)
        preds = torch.argmax(preds, dim=-1)

    else:
        _, preds = torch.max(outputs, 1)

    return preds

def eval_model(model: torch.nn.Module, dataloaders, device, pred_func):

    scores_out = f"Evaluation results\n"
    scores_out += f""

    model.to(device)
 
    for phase in ['train', 'test']: 
        print("Current phase:", phase)
        model.eval()

        tp = 0
        tn = 0
        fp = 0
        fn = 0

        for inputs, labels in tqdm(dataloaders[phase], desc=f"phase {phase}"): 
            inputs = inputs.to(device)
            labels = labels.to(device)

            with torch.set_grad_enabled(False): 
                outputs = model(inputs)
                preds = pred_func(outputs)
            
            preds_list = preds.tolist()
            labels_list = labels.data.tolist()
            
            # schau, welche outputs tn, fp, etc sind
            for p, l in zip(preds_list, labels_list):
                if p == 0 and l == 0:
                    tn += 1
                elif p == 1 and l == 0:
                    fp += 1
                elif p == 0 and l == 1:
                    fn += 1
                elif p == 1 and l == 1:
                    tp += 1

        # berechne precision, recall und f1
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        f1 = 2 * ((precision * recall)/(precision + recall))

        # Printe alle Werte. Diese Werte bräuchte ich dann für die Tabellen im Paper
        scores_out += f"{phase}-phase:\n"
        scores_out += f"TPs: {tp}\n"
        scores_out += f"TNs: {tn}\n"
        scores_out += f"FPs: {fp}\n"
        scores_out += f"FNs: {fn}\n"
        scores_out += f"Precision: {precision}\n"
        scores_out += f"Recall: {recall}\n"
        scores_out += f"F1: {f1}\n"
        
    return scores_out


def main(args: argparse.Namespace):
    config = Config.load_config(paths.FILE_CONFIG(args.project))

    transform =  COMPOSER[config.General.composer].get_composer(True)

    data_dir = config.General.dataset

    n_filter = 0
    if config.General.model == "vgg16_adience":
        train_set = AdienceGender(data_dir, transform, "train")
        test_set = AdienceGender(data_dir, transform, "test")
        n_filter = 512
    elif config.General.model == "pathmnist_finetuned":
        train_set = PathMNIST(data_dir, transform, "train")
        test_set = PathMNIST(data_dir, transform, "test")
        n_filter = 2048
    elif config.General.model == "picasso_finetuned":
        train_set = Picasso(data_dir, transform, "train")
        test_set = Picasso(data_dir, transform, "test")
        n_filter = 512
    elif config.General.model == "vgg16_bn_teapot_ft":
        train_set = TeapotVase(paths.DATASETS("teapot_subset"), transform, "train")
        test_set = TeapotVase(paths.DATASETS("teapot_subset"), transform, "val")
        n_filter = 512

    train_loader = DataLoader(train_set, shuffle=False, batch_size=16, num_workers=8)
    test_loader = DataLoader(test_set, shuffle=False, batch_size=16, num_workers=8)

    model = LOADER[config.General.model](args.device)

    if args.mode != "normal":

        filter_all = list(range(0,n_filter))
        concepts_in_bg = all_concepts_in_bg(config)

        with open(paths.FILE_RULES(config.General.results + config.General.project), mode='rb') as file:
            rules: List[Rule] =  pickle.load(file)
        rule_concepts = distinct_concepts(rules)

        if args.mode == "mask_other":
            filters_to_mask = set(filter_all).difference(set(concepts_in_bg))

            model = mask(model, config.CRP.layers, filters_to_mask, args.device)

        elif args.mode == "mask_rules":
            filters_to_mask = set(filter_all).difference(set(concepts_in_bg))
            filters_to_mask = list(filters_to_mask) + rule_concepts

            model = mask(model, config.CRP.layers, filters_to_mask, args.device)

        else:
            raise Exception("Define a valid mode!")

    scores_out = eval_model(model, dict(train=train_loader, test=test_loader), args.device, lambda x: prediction(x, config.General.model))
    
    scores_out += f"\nMode {args.mode}"
    if args.mode != "normal":
        scores_out += f"\nConcepts in theory (total={len(rule_concepts)}): {str(rule_concepts)}\n"
        scores_out += f"\nConcepts in BG (total={len(concepts_in_bg)}): {str(concepts_in_bg)}\n"
        scores_out += f"Masked out concepts (total={len(filters_to_mask)}): {str(filters_to_mask)}\n"
    print(scores_out)

    os.makedirs(paths.RESULTS_MODELS, exist_ok=True)
    if args.mode == "normal":
        filename = f"eval_{config.General.model}_{args.mode}.txt"
    else:
        filename = f"eval_{args.project}_{config.General.model}_{args.mode}.txt"
    
    with open(paths.RESULTS_MODELS / filename, mode="w") as file:
        file.write(scores_out)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect concept predictions for selected samples.')
    parser.add_argument('-p', '--project', type=str,
                    choices=list(x.replace(".ini","") for x in os.listdir(paths.CONFIG)),
                    required=True,
                    help='The identifier of the model to conduct an analysis for.')

    parser.add_argument('-d', '--device', 
                    default=("cuda:0" if torch.cuda.is_available() else "cpu"),
                    help='Sets the device model and samples are loaded to.')

    parser.add_argument('-m', '--mode', 
                    choices=["normal", "mask_rules", "mask_other"],
                    default="normal",
                    help='Mode of model evaluation. "normal": Unmodified model. \
                    "mask_rules": Mask filters of concepts in theory and filters of concepts not in BK. "mask_other": Mask all but filters of concepts in BK.')

    args : argparse.Namespace = parser.parse_args()
    
    main(args)

