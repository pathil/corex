import os
import sys
sys.path.append(os.getcwd())

import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from torchvision import datasets, transforms
from finetuned_models.med_models import ResNet50
import time
import copy

import common.paths as paths

train_dir = paths.DATASETS / "pathmnist" / "train"
val_dir = paths.DATASETS / "pathmnist" / "val"
test_dir = paths.DATASETS / "pathmnist" / "test"

num_classes = 2
batch_size = 128
num_epochs = 10

feature_extract = True

def train_model(model, dataloaders, criterion, optimizer, num_epochs=10):
    since = time.time()

    train_f1_history = []
    val_f1_history = []
    test_f1_history = []

    best_model_wts = copy.deepcopy(model.state_dict())
    best_val_f1 = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        for phase in ['train', 'val', 'test']:
            if phase == 'train':
                model.train()
            else:
                model.eval()
            
            running_loss = 0.0
            running_tp = 0
            running_tn = 0
            running_fp = 0
            running_fn = 0

            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)
                
                    _, preds = torch.max(outputs, 1)

                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                running_loss += loss.item() * inputs.size(0)
                preds_list = preds.tolist()
                labels_list = labels.data.tolist()
                for p, l in zip(preds_list, labels_list):
                    if p == 0 and l == 0:
                        running_tn += 1
                    elif p == 1 and l == 0:
                        running_fp += 1
                    elif p == 0 and l == 1:
                        running_fn += 1
                    elif p == 1 and l == 1:
                        running_tp += 1

            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            precision = running_tp / (running_tp + running_fp)
            recall = running_tp / (running_tp + running_fn)
            epoch_f1 = 2 * ((precision * recall)/(precision + recall))

            print('{} Loss: {:.4f} F1: {:.4f}'.format(phase, epoch_loss, epoch_f1))

            if phase == 'val' and epoch_f1 > best_val_f1:
                best_val_f1 = epoch_f1
                best_model_wts = copy.deepcopy(model.state_dict())
            if phase == 'train':
                train_f1_history.append(epoch_f1)
            if phase == 'val':
                val_f1_history.append(epoch_f1)
            if phase == 'test':
                test_f1_history.append(epoch_f1)
        
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val F1: {:4f}'.format(best_val_f1))

    model.load_state_dict(best_model_wts)
    
    return model, train_f1_history, val_f1_history, test_f1_history

def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False

def initialize_model(feature_extract, use_pretrained=True):
    model_ft = ResNet50(in_channels=3, num_classes=9)
    # to obtain the pretrained model, go to https://github.com/MedMNIST/experiments and get the weights from the provided drive (under point 5.).
    model_ft.load_state_dict(torch.load("./models/pathmnist_resnet50_28_3.pth", map_location='cuda:0')['net'], strict=True)
    set_parameter_requires_grad(model_ft, feature_extract)
    model_ft.avgpool = nn.AdaptiveAvgPool2d(output_size=(1, 1))
    num_ftrs = model_ft.linear.in_features
    model_ft.linear = nn.Linear(num_ftrs, 2)
    input_size = 28

    return model_ft, input_size


model_ft, input_size = initialize_model(feature_extract, use_pretrained=True)

print(model_ft)
print("-" * 10)


data_transforms = {
    'train': transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])]
    ),
    'val': transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])]
    ),
    'test': transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize(mean=[.5], std=[.5])]
    )
}

print("Initializing Datasets and Dataloaders...")

image_datasets = {
    'train': datasets.ImageFolder(train_dir, data_transforms['train']),
    'val': datasets.ImageFolder(val_dir, data_transforms['val']),
    'test': datasets.ImageFolder(test_dir, data_transforms['test'])
}

dataloaders_dict = {
    x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size, shuffle=True, num_workers=4) for x in ['train', 'val', 'test']
}

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model_ft = model_ft.to(device)

params_to_update = model_ft.parameters()
print("Params to learn:")
if feature_extract:
    params_to_update = []
    for name, param in model_ft.named_parameters():
        if param.requires_grad == True:
            params_to_update.append(param)
            print("\t", name)
else:
    for name, param in model_ft.named_parameters():
        if param.requires_grad == True:
            print("\t", name)

optimizer_ft = optim.Adam(params_to_update, lr=0.001)

criterion = nn.CrossEntropyLoss()

model_ft, hist_train, hist_val, hist_test = train_model(model_ft, dataloaders_dict, criterion, optimizer_ft, num_epochs=num_epochs)

print("Train F1 History:")
print(hist_train)

print("Validation F1 History:")
print(hist_val)

print("Test F1 History:")
print(hist_test)

torch.save(model_ft.state_dict(), "./finetuned_models/pathmnist_2_classes.pth")
