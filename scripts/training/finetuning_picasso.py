import os
import sys
sys.path.append(os.getcwd())

import torch
import torch.nn as nn
from torchvision import datasets, transforms
from torchvision.models import vgg16, vgg16_bn
from torch.optim import Adam
from tqdm import tqdm
from torch.utils.data.dataloader import DataLoader, Dataset

import os
from typing import Sequence, Tuple, Dict, Union

import common.paths as paths
from common.datasets import Picasso

from PIL import Image

train_dir = paths.DATASETS("picasso") / "train"
test_dir = paths.DATASETS("picasso") / "test"

batch_size = 16


def train(model, data_loader, optimizer, criterion, accuracy, device):
    if len(list(model.parameters())) == 0:
        raise ValueError("Model to train does not have any parameters!")
    
    epoch_loss = 0
    epoch_cor = 0.0
    total_samples = 0

    model.train()

    # pbar: tqdm = tqdm((data_loader[i] for i in range(len(data_loader))), total=len(data_loader))
    pbar: tqdm = tqdm(data_loader, total=len(data_loader), dynamic_ncols=True)

    for inputs, targets in pbar:

        inputs = inputs.to(device)
        targets = targets.to(device)

        optimizer.zero_grad()
        preds = model(inputs)


        # res = torch.argmax(targets, 1).float()
        loss = criterion(preds if preds.size()[1] > 1 else preds.squeeze(1),
                            targets)
        # loss = criterion(preds ,targets)
        

        loss.backward()
        optimizer.step()

        batch_acc, _, _, _ = accuracy(preds, targets)

        epoch_loss += loss.item()
        # epoch_cor += cor
        # total_samples += n_samples

        pbar.set_postfix({'loss': loss.item(), 'acc': batch_acc})
        pbar.update()


    return epoch_loss, batch_acc #epoch_acc


def evaluate(model, data_loader, criterion, accuracy, device):
    
    epoch_loss = 0
    epoch_cor = 0.0
    total_samples = 0
    
    model.eval()

    tp, tn, all_ = 0, 0, 0
    with torch.no_grad():
        # for inputs, targets in tqdm((data_loader[i] for i in range(len(data_loader))),
        #                             total=len(data_loader)):
        pbar: tqdm = tqdm(data_loader, total=len(data_loader), dynamic_ncols=True)

        for inputs, targets in pbar:

            inputs = inputs.float().to(device)
            targets = targets.float().to(device)

            # actual prediction
            preds = model(inputs)

            loss = criterion(preds if preds.size()[1] > 1 else preds.squeeze(1),
                                targets)

            # Calc true positives, true negatives, and tn+tp+fn+fp for batch
            _, batch_tp, batch_tn, batch_all = accuracy(preds, targets)
            # add to accumulated values:
            tp += batch_tp
            tn += batch_tn
            all_ += batch_all

            epoch_loss += loss.item()
            
    # accuracy:
    acc = float(tp + tn) / all_

    epoch_loss /= len(data_loader)

    return epoch_loss, acc

      
#   return epoch_loss, epoch_acc



def accuracy(preds: torch.Tensor, targets: torch.Tensor, cls_dim: int = 1,
             ) -> Tuple[float, float, float, float]:
    # Calc true positives, true negatives, and tn+tp+fn+fp for batch
    # from one-hot or binary encoding to class indices:
    class_pred = (torch.sigmoid(preds) > 0.5).squeeze(cls_dim).float()

    # class_gt = torch.argmax(targets, dim=cls_dim).float()
    class_gt = targets
    # accuracy calculation
    batch_tp = float(torch.sum(class_pred * class_gt))
    batch_tn = float(torch.sum((1 - class_pred) * (1 - class_gt)))
    batch_all = float(class_gt.size()[0])
    batch_acc = (batch_tp + batch_tn) / batch_all
    return batch_acc, batch_tp, batch_tn, batch_all


data_transforms = {
    'train': transforms.Compose(
        [transforms.Resize(224), transforms.CenterCrop((224,224)), transforms.ToTensor(), \
         transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))]
    ),
    'val': transforms.Compose(
        [transforms.Resize(224), transforms.CenterCrop((224,224)), transforms.ToTensor(), \
         transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))]
    ),

}

import pandas as pd
    


if __name__ == "__main__":
    device = "cuda"
    
    EPOCHS = 20
    n_class = 1


    model = vgg16(pretrained=True)
    model.classifier[0] = torch.nn.Linear(7 * 7 * 512, 4096)
    model.classifier[3] = torch.nn.Linear(4096, 4096)
    model.classifier[6] = torch.nn.Linear(4096, 1)


    model = model.to(device)

    train_set = datasets.ImageFolder(train_dir, data_transforms['train'])
    val_set = datasets.ImageFolder(test_dir, data_transforms['val'])
    
    train_set = Picasso(train_dir, data_transforms['train'], "train")
    val_set = Picasso(test_dir, data_transforms['val'], "test")


    train_loader = DataLoader(train_set, shuffle=True, batch_size=32, num_workers=8)
    val_loader = DataLoader(val_set, shuffle=False, batch_size=32, num_workers=8)

    print(f"{len(train_set)=}")
    print(f"{len(val_set)=}")
    

    optimizer = Adam([
    #   {'params': model.features.parameters(), 'lr': 1e-6},
        {'params': model.classifier.parameters(), 'lr': 1e-4} 
    ])

    criterion = nn.BCEWithLogitsLoss()


    best_valid_loss = float('+inf')
    best_epoch = 0
    best_valid_acc = 0

    for epoch in range(EPOCHS):
        print("EPOCH:", epoch+1)

        train_loss, train_acc_1 = train(model, train_loader, optimizer, criterion, accuracy, device)

        print(f'\tTrain Loss: {train_loss:.3f}')
        print(f'\tTrain Acc @1: {train_acc_1*100:6.2f}%')

        valid_loss, valid_acc_1 = evaluate(model, val_loader, criterion, accuracy, device)
        


        if best_valid_loss > valid_loss:
            # filename = f'picasso_vgg16_{best_valid_acc*100:.2f}.pt'
            filename = paths.MODELS / f'picasso_vgg16.pt'
            if os.path.isfile(filename):
                os.remove(filename)
            best_valid_loss = valid_loss
            best_valid_acc = valid_acc_1
            best_epoch = epoch + 1
            torch.save(model.state_dict(), filename)
            print(f"\tEpoch {best_epoch} saved")
    
        print(f'\tBest Valid Loss @1: {best_valid_loss:.3f} in Epoch {best_epoch}')
