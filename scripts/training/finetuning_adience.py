# adapted based on a first version, that was implemented by Reduan Achtibat
# https://github.com/rachtibat

import os
import sys
sys.path.append(os.getcwd())

from torch.utils.data.dataloader import DataLoader
import torch.nn as nn
import torch
from tqdm import tqdm
import os

def train(model, iterator, optimizer, criterion, accuracy, device):
    
  epoch_loss = 0
  epoch_cor = 0.0
  total_samples = 0

  model.train()

  pbar = tqdm(total=len(iterator), dynamic_ncols=True)
  
  for (x, y) in iterator:
      
    x = x.to(device)
    y = y.to(device)
    
    optimizer.zero_grad()
            
    y_pred = model(x)
    
    loss = criterion(y_pred, y)
    
    cor, n_samples = accuracy(y_pred, y)
    
    loss.backward()
    
    optimizer.step()
    
    epoch_loss += loss.item()
    epoch_cor += cor
    total_samples += n_samples

    pbar.update(1)
      
  epoch_loss /= len(iterator)
  epoch_acc = epoch_cor / total_samples

  pbar.close()
      
  return epoch_loss, epoch_acc


def evaluate(model, iterator, criterion, accuracy, device):
    
  epoch_loss = 0
  epoch_cor = 0.0
  total_samples = 0
  
  model.eval()

  pbar = tqdm(total=len(iterator), dynamic_ncols=True)
  
  with torch.no_grad():
      
    for (x, y) in iterator:

      x = x.to(device)
      y = y.to(device)

      y_pred = model(x)

      loss = criterion(y_pred, y)

      correct, n_samples = accuracy(y_pred, y)

      epoch_loss += loss.item()
      epoch_cor += correct
      total_samples += n_samples

      pbar.update(1)
      
  epoch_loss /= len(iterator)
  epoch_acc = epoch_cor/total_samples

  pbar.close()
      
  return epoch_loss, epoch_acc

softmax = nn.Softmax(dim=-1)
def calc_accuracy(pred, target):
    
    pred = softmax(pred)
    pred = torch.argmax(pred, dim=-1)

    correct = torch.sum(pred==target)

    return correct, len(target)


if __name__ == "__main__":
    from torch.optim import Adam
    from torchvision.models import vgg16
    from common.datasets import AdienceGender
    import common.paths as paths

    EPOCHS = 20
    device = "cuda"
    
    n_class = 2

    model = vgg16(pretrained=True)

    model.classifier[0] = torch.nn.Linear(7 * 7 * 512, 4096)
    model.classifier[3] = torch.nn.Linear(4096, 4096)
    model.classifier[6] = torch.nn.Linear(4096, n_class)

    model = model.to(device)

    dir = paths.DATASETS("adience")
    trainset = AdienceGender(dir, transform=None, split="train")
    valset = AdienceGender(dir, transform=None, split="test")

    trainloader = DataLoader(trainset, shuffle=True, batch_size=32, num_workers=8)
    valloader = DataLoader(valset, shuffle=False, batch_size=32, num_workers=8)

    optimizer = Adam([
      {'params': model.classifier.parameters(), 'lr': 1e-4} 
    ])
    
    criterion = nn.CrossEntropyLoss()

    best_valid_loss = float('+inf')
    best_epoch = 0
    best_valid_acc = 0

    for epoch in range(EPOCHS):

        print("EPOCH:", epoch+1)
    
        train_loss, train_acc_1 = train(model, trainloader, optimizer, criterion, calc_accuracy, device)
        
        print(f'\tTrain Loss: {train_loss:.3f}')
        print(f'\tTrain Acc @1: {train_acc_1*100:6.2f}%')

        valid_loss, valid_acc_1 = evaluate(model, valloader, criterion, calc_accuracy, device)
        
        print(f'\tValid Loss: {valid_loss:.3f}')
        print(f'\tValid Acc @1: {valid_acc_1*100:6.2f}%')
        
        if best_valid_loss > valid_loss:
          # filename = paths.MODELS / f'adience_gender_{best_valid_acc*100:.2f}.pt'
          filename = paths.MODELS / f'adience_gender_vgg16.pt'
          
          if os.path.isfile(filename):
            os.remove(filename)
          best_valid_loss = valid_loss
          best_valid_acc = valid_acc_1
          best_epoch = epoch + 1
          torch.save(model.state_dict(), filename)
          print(f"\tEpoch {best_epoch} saved")
    
        print(f'\tBest Valid Loss @1: {best_valid_loss:.3f} in Epoch {best_epoch}')
