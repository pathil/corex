import os
import sys
sys.path.append(os.getcwd())

import argparse
import torch 
import os

from common.paths import FileNavigator, configs
from common.config import Config
from common.loader import LOADER, COMPOSER
from common.datasets import DATASET

from zennit.canonizers import SequentialMergeBatchNorm
from zennit.composites import EpsilonPlusFlat

from crp.attribution import CondAttribution
from crp.concepts import ChannelConcept
from crp.helper import get_layer_names
from crp.visualization import FeatureVisualization

def analyse(config: Config, device: str, batch_size: int = 16):
    model : torch.nn.Module = LOADER[config.General.model](device).to(device)
    model.eval()

    dataset = DATASET[config.General.dataset_loader](config.General.dataset)
    comp_info = COMPOSER[config.General.composer]
    preprocessing_fn = comp_info.normalizer

    canonizers = [SequentialMergeBatchNorm()]
    composite = EpsilonPlusFlat(canonizers)
    cc = ChannelConcept()

    layer_names = get_layer_names(model, [torch.nn.Conv2d])
    layer_map = {layer: cc for layer in layer_names}

    attribution = CondAttribution(model)

    cache = None #ImageCache()

    fv = FeatureVisualization(attribution, dataset, layer_map, preprocess_fn=preprocessing_fn,
                                path=config.FeatureVis.results, cache=cache)

    fv.run(composite, 0, len(dataset), batch_size=batch_size)


def main(args : argparse.Namespace):
    config = Config.load_config(FileNavigator.config(args.project))

    analyse(config, args.device, args.batch_size)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect concept predictions for selected samples.')
    parser.add_argument('-p', '--project', type=str,
                    choices=list(x.replace(".ini","") for x in os.listdir(configs())),
                    required=True,
                    help='The identifier of the model to conduct an analysis for.')

    parser.add_argument('-d', '--device', 
                    default=("cuda:0" if torch.cuda.is_available() else "cpu"),
                    help='Sets the device model and samples are loaded to.')

    parser.add_argument('-b', '--batch_size', 
                    default=16,
                    help='Batch size.')

    args : argparse.Namespace = parser.parse_args()
    
    main(args)
