"""
This script samples from the original PathMNIST to create a 2-class problem.
"""

from os import listdir
from shutil import copyfile
from random import shuffle

# it is assumed that the original PathMNIST data lies as splits in a folder 'pathmnist'
pathmnist_path = "./pathmnist/"
# also it is assumed that under the folder 'pathmnist_2_class' there are three folders
# with 'train', 'val' and 'test', each containing empty folders 'pos' and 'neg'
new_path = "./pathmnist_2_class/"

for split in ['train', 'val', 'test']:
    print("Current split: " + split)
    cur_path = pathmnist_path + split + "/"

    neg_class_dict = {} # for the negative data

    all_pos_count = 0
    for f in listdir(cur_path):
        class_id = f.split("_")[-1].split(".")[0]

        # all positive images will have 8 as their original class
        if class_id == "8":
            copyfile(pathmnist_path + split + "/" + f, new_path + split + "/pos/" + f)
            all_pos_count += 1
        else: # negative image
            if class_id in neg_class_dict:
                neg_class_dict[class_id].append(f)
            else:
                neg_class_dict[class_id] = []
                neg_class_dict[class_id].append(f)
    
    all_neg_count = 0
    for k in neg_class_dict.keys():
        all_neg_count += len(neg_class_dict[k])

    for k in neg_class_dict.keys():
        print("Key: " + k + ", Length: " + str(len(neg_class_dict[k])))
        ratio = len(neg_class_dict[k]) / all_neg_count
        amount = int(ratio * all_pos_count)
        print("Ratio: " +  str(ratio) + ", Amount: " + str(amount))
        shuffle(neg_class_dict[k])
        sampled_files = neg_class_dict[k][:amount]
        for s in sampled_files:
            copyfile(pathmnist_path + split + "/" + s, new_path + split + "/neg/" + s)
