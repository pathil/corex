import sys
import os

sys.path.append(os.getcwd())
from typing import Dict
from tinydb import TinyDB, Query
from tinydb.table import Document

import common.paths as paths
import os

from tqdm import tqdm
import shutil

import torch
import torchvision.transforms as T
from torchvision.models.vgg import vgg16_bn
from PIL import Image
from common.config import Config

project = "teapot"
config = Config.load_config(paths.FILE_CONFIG(project))

classes = [849, 883]    #   teapot, vase
# project = "saxophone"
# classes = [776, 683]    #   sax, oboe
# project = "safe"
# classes = [771, 771]

val_dir = paths.DATASETS("ImageNet") / "val"
test_dir = paths.DATASETS("ImageNet") / "test"

todo = Query()	
db_path = paths.SAMPLING / "imagenet_labels_info.json"
db = TinyDB(db_path)

labels : Dict[int, Document] = dict()
sample_lookup : Dict[int, list] = dict()

for id in classes:
    labels[id] = db.get(todo.idx==id)
    sample_lookup[id] = list()

def copy(src_file : str, dest_dir : str):
    name = os.path.basename(src_file)
    dest_file = os.path.join(dest_dir, name.replace("ILSVRC2012_", ""))
    shutil.copyfile(src_file, dest_file)

def copy_all(src_dir : str, dest_dir : str):
    for root, _, files in os.walk(src_dir):
        for file in tqdm(files):
            if(file.endswith("JPEG")):
                src_file = os.path.join(root, file)
                copy(src_file, dest_dir)


def copy_from_val(val_dir : str, pos_id : int, neg_id : int):
    
    # teapot_wnd_id = 'n04398044'
    # vase_wnd_id = 'n04522168'
    teapot_wnd_id = labels[pos_id]['wnid']
    vase_wnd_id = labels[neg_id]['wnid']

    source_teapot = os.path.join(val_dir, teapot_wnd_id)
    source_vase = os.path.join(val_dir, vase_wnd_id)

    dest_dir = paths.POS(config.General.samples)
    copy_all(source_teapot, dest_dir)
    dest_dir = paths.NEG(config.General.samples)
    copy_all(source_vase, dest_dir)

def get_lookup_txt_name(id : int):
    return paths.SAMPLING / f"{id}_{labels[id]['label']}.txt"

def create_prediction_lookup(test_dir : str):
    device = "cuda:0" if torch.cuda.is_available() else "cpu"

    model = vgg16_bn(True).to(device)
    model.eval()

    transform = T.Compose([T.Resize(256), T.CenterCrop(224), T.ToTensor(), T.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    softmax = torch.nn.Softmax(dim=-1)
    
    db_path = paths.SCRIPTS / "imagenet_test_pred.json"
    db = TinyDB(db_path)

    for root, _, files in os.walk(test_dir):
        for file in tqdm(files):
            if(file.endswith(config.General.file_ending)):
                src_file = os.path.join(root, file)
                image = Image.open(src_file)
                sample = transform(image.convert('RGB')).unsqueeze(0).to(device)
                pred = torch.argmax(softmax(model(sample)), dim=-1).item()
                
                newItem = {"img": file, "pred": pred}
                db.insert(newItem)

                if pred in labels:
                    sample_lookup[pred].append(file)
                    print(f"File {file} is {pred}")

    for id in labels:
        with open(get_lookup_txt_name(id), 'w') as file:
            file.write(str.join('\n', sample_lookup[id]))


# if os.path.exists(paths.POS(config.General.samples)):
#     shutil.rmtree(paths.POS(config.General.samples))
os.makedirs(paths.POS(config.General.samples), exist_ok=True)

# if os.path.exists(paths.NEG(config.General.samples)):
#     shutil.rmtree(paths.NEG(config.General.samples))
os.makedirs(paths.NEG(config.General.samples), exist_ok=True)

#   get samples from the imagenet validation set
copy_from_val(val_dir, classes[0], classes[1])


# #   get the samples from the imagenet test set
# #   since there is no official labeling, we let it predict by vgg16_bn
# #   YOU DONT NEED TO DO THIS: predictions are already in the .json
# predict_again = False
# for id in classes:
#     if os.path.exists(get_lookup_txt_name(id)) == False:
#         predict_again = True
#         break
# if predict_again:
#     create_prediction_lookup(test_dir, classes)
# todo = Query()	
# db_path = paths.SCRIPTS / "imagenet_test_pred.json"
# db = TinyDB(db_path)
# labels = dict()
# id = classes[0]
# labels[id] = db.search(todo.pred==id)


# # YOU DONT NEED TO DO THIS: lookup-file is already in the dir
# for id in labels:
#     with open(get_lookup_txt_name(id), 'w') as file:
#         file.write('\n'.join(item['img'] for item in labels[id]))    

print(f"Copy samples from test-set to {config.General.samples} ...")
pos = get_lookup_txt_name(classes[0])
with open(pos, "r", ) as file:
    for line in file:
        src_file = os.path.join(test_dir, line.replace('\n', ''))
        copy(src_file, paths.POS(config.General.samples))

neg = get_lookup_txt_name(classes[1])
with open(neg, "r", ) as file:
    for line in file:
        src_file = os.path.join(test_dir, line.replace('\n', ''))
        copy(src_file, paths.NEG(config.General.samples))

