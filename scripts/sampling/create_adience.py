
import os
import sys
sys.path.append(os.getcwd())

import pandas
import shutil

import common.paths as paths

import random
random.seed("abc123")


path_label = paths.DATASETS("adience")
path_imgs = path_label / "aligned"

train = False

if train:
    path_data = paths.DATA_SAMPLES("adience_train")
    folds = range(4)
else:
    path_data = paths.DATA_SAMPLES("adience_test")    
    folds = [4]

csvs = []
for i in folds:
    csvs.append(pandas.read_csv(path_label / f"fold_frontal_{i}_data.txt", delimiter="\t"))

csv = pandas.concat(csvs, ignore_index=True)

csv = csv[csv["gender"].isin(["m", "f"])]
csv = csv.drop_duplicates(subset='face_id', keep="first")

csv_m = csv.where(csv["gender"] == "m").dropna()
csv_f = csv.where(csv["gender"] == "f").dropna()

size = 200

#   drop males
drop_num = len(csv_m) - size

while len(csv_m) > size:
    csv_m = csv_m.drop(random.choice(csv_m.index))

#   drop females
drop_num = len(csv_f) - size

while len(csv_f) > size:
    csv_f = csv_f.drop(random.choice(csv_f.index))


print(csv_f.head())
print(len(csv_f))

def copy(src_file : str, dest_dir : str):
    shutil.copyfile(src_file, dest_dir)

os.makedirs(path_data / "pos", exist_ok=True)
os.makedirs(path_data / "neg", exist_ok=True)

from tqdm import tqdm

#   copy males
for index in tqdm(range(len(csv_m))):
    row = csv_m.iloc[index]
    t_path = path_imgs / row["user_id"] / ("landmark_aligned_face." + str(row["face_id"]).replace(".0", "") + "." + row["original_image"])
    name = "pic_" + str(row["face_id"]).replace(".0", "") + ".jpg"
    copy(t_path, path_data / "pos" / name)
    

#   copy females
for index in tqdm(range(len(csv_f))):
    row = csv_f.iloc[index]
    t_path = path_imgs / row["user_id"] / ("landmark_aligned_face." + str(row["face_id"]).replace(".0", "") + "." + row["original_image"])
    name = "pic_" + str(row["face_id"]).replace(".0", "") + ".jpg"
    copy(t_path, path_data / "neg" / name)
    