from os import listdir
from shutil import copyfile
from random import shuffle

path = "./pathmnist_2_class/"

# it is assumed that under this folder there are folders 'train', 'val', 'test'
# each containing folders 'pos' and 'neg'
new_path = "./pathmnist_pipeline/"

for split in ['train', 'val', 'test']:
    print("Current split: " +  split)
    cur_path = path + split + "/"

    pos_path = cur_path + "pos/"
    pos_files = listdir(pos_path)
    shuffle(pos_files)
    sampled_pos_files = pos_files[:250]
    for s in sampled_pos_files:
        copyfile(pos_path + s, new_path + split + "/pos/" + s)
    
    neg_class_dict = {}
    neg_path = cur_path + "neg/"
    all_neg_files = listdir(neg_path)
    for f in all_neg_files:
        class_id = f.split("_")[-1].split(".")[0]
        if class_id in neg_class_dict:
            neg_class_dict[class_id].append(f)
        else:
            neg_class_dict[class_id] = []
            neg_class_dict[class_id].append(f)
    
    all_neg_count = len(all_neg_files)

    for k in neg_class_dict.keys():
        print("Key: " + k + ", Length: " + str(len(neg_class_dict[k])))
        ratio = len(neg_class_dict[k]) / all_neg_count
        amount = int(ratio * 250)
        print("Ratio: " + str(ratio) + ", Amount: " + str(amount))
        shuffle(neg_class_dict[k])
        sampled_files = neg_class_dict[k][:amount]
        for s in sampled_files:
            copyfile(neg_path + s, new_path + split + "/neg/" + s)
