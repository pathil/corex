from typing import List

from common.paths import ProjectNavigator, configs, config_file
from common.helper import sample_img_path, treat_pyswip_proof_format, layer_concept_splitter
from common.helper import parse_fact, parse_predicate
from common.loader import COMPOSER
from common.config import Config
from common.structs import Rule, Fact, Predicate

from pathlib import Path
import pickle
import os
import argparse

from PIL import Image
from ref_sampling.labels import DbAccess
# from pipeline.knowledge import resolve_in_nl, set_relations
from common.relations import set_relations, resolve_in_nl
from interpretation.interpretation import Interpretation
from common.analysis import plot_shapes
from shapely.geometry import Polygon
import regex

def indent(line:str, level:int = 0) -> str:
    return f"{' '*2*level + line}\n"

class Responder:
    def __init__(self, pn: ProjectNavigator) -> None:
        self.pn = pn
        self.project = pn.config.General.project
        self.rules: List[Rule] = pn.file.rules().read()
        self.config = pn.config
        self.sample = ""
        self.set_relations = set_relations(self.config.Relations.sets)
        self.ip = None
        self.label_access = DbAccess(pn.file.labels_db().path)

    # @staticmethod
    # def _load_rules(proj_path):
    #     with open(paths.FILE_RULES(proj_path), mode='rb') as file:
    #         return pickle.load(file)

    # @staticmethod
    # def _load_shape(proj_path: str, sample: str, layer: str, concept: str):
    #     with open(paths.FILE_SHAPES(proj_path, sample, layer, concept), mode='rb') as file:
    #         shape = pickle.load(file)
    #     return shape

    @staticmethod
    def distinct_layer_concepts(rules: List[Rule]):
        # retrieve concepts from rules
        concepts = []
        for rule in rules:
            for predicate in rule.body:
                for arg in predicate.args:
                    if arg == "A":
                        continue
                    if type(arg) == Predicate:
                        for pos_neg_arg in arg.args:
                            if pos_neg_arg == "A":
                                continue
                            c = pos_neg_arg
                            if not c in concepts:
                                concepts.append(c)
                    elif type(arg) == str:
                        c = arg
                        if not c in concepts:
                            concepts.append(c)
                    else:
                        raise Exception(f"Unexpected arg-type: {type(arg)=}")
        return concepts

    def locate_on_img(self, concept: int, layer: str) -> str:        
        poly: Polygon = self.pn.file.shapes(self.sample, layer, concept).read()[0]
        height = COMPOSER[self.config.General.composer].crop_h
        width = COMPOSER[self.config.General.composer].crop_w

        # image segmented into 3x3 raster
        loc = []
        if poly.centroid.x > width * 0.33 and poly.centroid.x < width * 0.66:
            loc.append("middle")
        if poly.centroid.y < height * 0.33:
            loc.append("upper")
        elif poly.centroid.y > height * 0.66:
            loc.append("lower")
        else: 
            if "middle" in loc:
                loc.append("middle")
        
        if poly.centroid.x < width * 0.33:
            loc.append("left")
        elif poly.centroid.x > width * 0.66:
            loc.append("right")

        return " ".join(loc)

    def resolve_label_placeholder(self, placeholder: str, locate: bool):
        layer, concept = layer_concept_splitter(placeholder)
        location = ""
        if locate:
            location = " in the " + self.locate_on_img(concept, layer) + " part"

        label = self.label_access.load_desc(concept, layer)
        if label != "":
            placeholder = label
        return placeholder + location

    def verbalize_predicate(self, pred: Predicate, negate: bool=False):
        to_resolve = pred.name
        if to_resolve.endswith("_bg"):
            to_resolve = to_resolve[:-3]
        verbalized: str = resolve_in_nl(to_resolve)
        if verbalized == None:
            raise Exception(f"[Error!: Couldn't resolve {pred.name}]")
        else:
            variables = pred.args[1:]
            if len(variables) == 1:
                verbalized = verbalized.replace('[A]', f"a [{variables[0]}]")
            elif len(variables) > 1:
                verbalized_preds = list()
                for var in variables:
                    if type(var) == Predicate:
                        verbalized_preds.append(self.verbalize_predicate(var, negate))
                    else:
                        verbalized_preds.append(f"[{var}]")
                if len(variables) >= 1:
                    verbalized = verbalized.replace('[A]', f"{verbalized_preds[0]}")
                if len(variables) >= 2:
                    verbalized = verbalized.replace('[B]', f"{verbalized_preds[1]}")
        # verbalized = "not " + verbalized.replace(" is", ", that is")
        return verbalized

    def verbalize_rule(self, rule: Rule):
        resolved = ""
        for predicate in rule.body:
            verbalized = "there is " + self.verbalize_predicate(predicate, negate=False).replace(" is ", ", that is ")
            # if predicate.args == 2:
            #     verbalized = "a " + verbalized
            # elif predicate.args == 3:
            #     verbalized = "there is " + verbalized
            if resolved != "":
                verbalized = " and " + verbalized
            resolved = resolved + verbalized
        return resolved

    def verbalize_trace(self, trace: List[str]):
        resolved = ""
        if trace[len(trace) - 1] == 'false':   #    last node should be the word 'false'
            failed_pred = trace[len(trace) - 2]   #   the one before the last is the failed predicate       
            pred: Predicate = parse_predicate(failed_pred)
            resolved: str = self.verbalize_predicate(pred, negate=True)            
        return resolved

    #   Preorder traversal (right to left, though)
    def verbalize_proof(self, lst, level: int = 0):
        verbalized = ""
        if len(lst) == 3:   #   Prolog meta-interpreter should only provide binary-tree like proofs
            if type(lst[2]) == str and lst[2] != "true":
                pred: Predicate = parse_predicate(lst[2])
                resolved = self.verbalize_predicate(pred)
                verbalized += indent(f"-> because there is {resolved}",level=level)
            elif type(lst[2]) == list:
                verbalized += self.verbalize_proof(lst[2], level=level+1)
            # else:
            #     raise Exception("Error: Part of proof had unexpected type.")
            
            if type(lst[0]) == str and lst[0] != "true":
                pred: Predicate = parse_predicate(lst[0])
                resolved = self.verbalize_predicate(pred)
                verbalized += indent(f"because there is {resolved}",level=level)
            elif type(lst[0]) == list:
                verbalized += self.verbalize_proof(lst[0], level=level+1)
            # else:
            #     raise Exception("Error: Part of proof had unexpected type.")
        else:
            raise Exception("Error: Proof had unexpected structure.")
        
        return verbalized

    def print_global_explanation(self):
        print(f"\nThe {self.project}-model knows the following rules:\n")
        print("A sample is positive, when\n")
        segments = []
        for r_i in range(len(self.rules)):
            verbalized = self.verbalize_rule(self.rules[r_i])
            for placeholder in set(regex.findall(r"\[\w*\]",verbalized)):
                label = self.resolve_label_placeholder(placeholder, False)
                verbalized = verbalized.replace(placeholder, label)

            segments.append(f"[{r_i + 1}] {verbalized}")
        print(" or when\n".join(segments) + ".")

    def print_local_explanation(self):        
        rules_local: List[Rule] = self.ip.explain()
        segments = []

        # check, if sample is positive, otherwise make explanation contrastive
        if len(rules_local) == 0:
            for r_idx in range(len(self.rules)):
                tracings = self.ip.trace(r_idx)
                for trace in tracings:
                    segments.append(f"[{r_idx}] there was " + self.verbalize_trace(trace).replace("no", "a").replace("is", "was"))
            print(f"Sample {self.sample} is not a positive, but it would have been if\n")
            print(" or when\n".join(segments) + ".")
        else:
            for rule in rules_local:
                verbalized = self.verbalize_rule(rule)
                for placeholder in set(regex.findall(r"\[\w*\]",verbalized)):
                    label = self.resolve_label_placeholder(placeholder, True)
                    verbalized = verbalized.replace(placeholder, label)
                r_i = rule.head.name.split("_")[2]
                # r_i = [x for x,z in enumerate(self.rules) if z.head.name == rule.head.name][0]
                segments.append(f"[{r_i}] {verbalized}")
            
            for i in range(len(segments)):
                segments[i] = segments[i].removesuffix(",")  #   if ends in a comma
                
            print(f"Sample {self.sample} is a positive, because in the image\n")
            print(" and because\n".join(segments) + ".")

    def print_proof(self, rule_idx: int):
        if rule_idx > len(self.rules) or rule_idx < 0:
            print(f"Error: rule [{rule_idx}] does not exist!")
        else:
            proof = self.ip.prove(rule_idx)
            if proof == "false.":
                print("The rule does not apply. Trace the rule, to find out why.")
            else:
                proof_root = treat_pyswip_proof_format(proof[0])
                verbalized = self.verbalize_proof(proof_root[0][0])
                print(f"Rule {rule_idx + 1} applies to sample {self.sample}")
                print(verbalized)

#   although there is a ... , there is no ..., and there has to be a [rule]
    def print_trace(self, rule_idx: int):        
        if rule_idx > len(self.rules) or rule_idx < 0:
            print(f"Error: rule [{rule_idx}] does not exist!")
        else:
            tracings: List = self.ip.trace(rule_idx)
            if tracings == "false.":
                print(f"There is nothing to trace, because the rule applies to [{self.sample}].")
            else:
                segments = []
                concepts = []
                for trace in tracings:
                    verbalized = " there is no " + self.verbalize_trace(trace).replace(" is", ", that is")
                    for placeholder in set(regex.findall(r"\[\w*\]",verbalized)):
                        label = self.resolve_label_placeholder(placeholder, False)
                        verbalized = verbalized.replace(placeholder, label)
                    segments.append(verbalized)

                rule: Rule = self.rules[rule_idx]
                concepts = Responder.distinct_layer_concepts([rule])
                existence = dict(true=list(),false=list())

                for c in concepts:
                    fact_absence = self.ip.is_existent(Fact("has_a", self.sample, c))
                    if fact_absence:
                        existence["true"].append(c)
                    elif fact_absence == False:
                        existence["false"].append(c)

                    

                # although there is a
                # there is no
                existence_lst = list()
                
                existence_str = ""
                # if len(existence["true"]) > 0:
                #     existence_str += ", although"
                existence_str += " and".join(f" there is a {x}" for x in existence["true"])

                # if existence[False] != None:
                #     existence_str += "there is missing"
                #     existence_str += " and ".join(f"a {x}" for x in existence[False])


                # existence_str += f".\n\nThe rule does not apply on [{self.sample}]"
                # existence_str += f"\n   because ".join()

               

                # at_least_one = False
                # for item in existence:
                #     if existence[item] == ['true.']:
                #         existence_lst.append(f"although there is a [{item}]")
                #         at_least_one = True
                #     else:
                #         existence_lst.append(f"there is no [{item}]")
                # existence_str = " and ".join(e for e in existence_lst)
                for placeholder in set(regex.findall(r"\[\w*\]",existence_str)):
                    label = self.resolve_label_placeholder(placeholder, False)
                    existence_str = existence_str.replace(placeholder, label)

                if existence_str == "":
                    print(f"In [{self.sample}]")
                else:
                    print(f"In [{self.sample}], although {existence_str},")
                print(f"Rule [{rule_idx + 1}] does not apply to [{self.sample}], because")
                print(f" and\n".join(segments) + ".")

    def visualize_concepts(self, rule_idx: int):
        rule: Rule = self.rules[rule_idx]
        polys: List[Polygon] = []
        labels: List[str] = []
        for pred in rule.body:
            for arg in pred.args[1:]:
                if type(arg) == str:    #   only has_a
                    poly, label = self.poly_label_from_literal(arg)
                    polys.append(poly)
                    labels.append(label)
                elif type(arg) == Predicate:    #   other with pos(...), neg(...)
                    for var in arg.args[1:]:
                        poly, label = self.poly_label_from_literal(var)
                        polys.append(poly)
                        labels.append(label)

        img = Image.open(self.pn.dir.explanation(self.sample) / f"t_{Path(self.file).name}")
        plot_shapes(polys=polys, img=img, ylim=img.height, xlim=img.width, labels=labels, single_color=False)

    def poly_label_from_literal(self, literal):
        layer, concept = layer_concept_splitter(literal)
        poly = self.pn.file.shapes(self.sample, layer, concept).read()[0]
        label = self.resolve_label_placeholder(literal, locate=False)
        return poly, label

    def induce_constraint(self, rule_idx: int):
        from pipeline.ilp import induce, parse_recordfile
        from pipeline.formalisation import Aleph

        rule: Rule = self.rules[rule_idx]

        constraints = list()
        
        if self.pn.file.constraints().path.exists():
            constraints = self.pn.file.constraints().read()
        
        for pred in rule.body:
            constraints.append(pred)

        self.pn.file.constraints().write(constraints)

        bk_aleph = self.pn.file.bk_aleph(constr=False)
        bk_aleph_constr = self.pn.file.bk_aleph(constr=True)
        record_out = self.pn.file.aleph_record()

        Aleph.add_constraint(constraints, bk_aleph, bk_aleph_constr)
        induce(bk_aleph_constr, record_out)
        self.rules = parse_recordfile(record_out)

    def setup_sample(self, file, generate_relations=False):
        import torchvision.transforms as T
        self.file = file
        self.sample = Path(file).stem
        out_dir = self.pn.dir.explanation(self.sample)
        os.makedirs(out_dir, exist_ok=True)
        img = Image.open(file)
        img.save(out_dir / f"{Path(file).name}")
        composer = COMPOSER[self.config.General.composer]
        t_img = composer.get_composer(False)(img)
        img : Image = T.ToPILImage()(t_img)
        img.save(out_dir / f"t_{Path(file).name}")
        
        sample_bg = self.pn.file.sample_bg(self.sample).read()
        self.ip: Interpretation = Interpretation(self.sample, sample_bg, self.set_relations, self.rules, out_dir)

def print_options(project, sample):
    print(f"\n-------------------------------------")
    print(f"Select one of the following options:\n")
    print(f"[1]\tSelect a sample for local explanations from the '{project}'-dataset.")
    print(f"[2]\tPrint the global explanation for '{project}'.")
    print(f"[8]\tConstraint and induce.")
    if sample != "":
        print(f"[3]\tShow original image of [{sample}].")
        print(f"[4]\tExplain why [{sample}] is a positive or negative.")
        print(f"[5]\tVisualize a rule for [{sample}].")
        print(f"[6]\tProof a valid rule on [{sample}].")
        print(f"[7]\tExplain why [{sample}] is not covered by a rule (Trace a Rule).")
    print(f"\n[Q]\tQuit - to quit the application.")
    print(f"-------------------------------------\n")


def main(args : argparse.Namespace):
    project = args.project
    
    device = args.device
       
    config = Config.load_config(config_file(project))
    pn = ProjectNavigator(config)
    
    #   delete constraints from last session
    if pn.file.constraints().path.exists():
        os.remove(pn.file.constraints())
        
    responder = Responder(pn)

    sample = ""
    img_path = ""

    # req = '1'

    while True:
        print_options(project, sample)
        req = input("Please enter a request (enter help for options): ").lower()
        req_sample = ""
        if req == '1':
            req_sample = input(f"Please, select a sample: ")
            try:
                img_path = sample_img_path(config.General.samples, req_sample)
            except:
                pass
            finally:
                if img_path != '':
                    responder.setup_sample(img_path)
                    sample = req_sample
                    print("Sample loaded.")
                else:
                    print("Error: Could not load sample.")
        elif req == '2':
            try:
                responder.print_global_explanation()
            except:
                print("Error: Could not print global explanation. (Might not work anymore before sample selection after implementation of negative concepts.")
        elif req == '8':
            rule_idx = input(f"Please, select number of a rule: ")
            if rule_idx.isnumeric():
                responder.induce_constraint(int(rule_idx) - 1)
            else:
                print("Error: Please enter a number!")
        if sample != "":
            if req == '3':
                Image.open(pn.dir.explanation(sample) / f"{sample}.{config.General.file_ending}").show()
            elif req == '4':
                responder.print_local_explanation()
            elif req == '5':
                rule_idx = input(f"Please, select number of a rule: ")
                if rule_idx.isnumeric():
                    responder.visualize_concepts(int(rule_idx) - 1)
                else:
                    print("Error: Please enter a number!")
            elif req == '6':
                # continue
                rule_idx = input(f"Please, select number of a rule: ")
                if rule_idx.isnumeric():
                    responder.print_proof(int(rule_idx) - 1)
                else:
                    print("Error: Please enter a number!")
            elif req == '7':
                rule_idx = input(f"Please, select number of a rule: ")
                if rule_idx.isnumeric():
                    responder.print_trace(int(rule_idx) - 1)
                else:
                    print("Error: Please enter a number!")
        if req == 'q':
            break
    print("Goodbye!")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect concept predictions for selected samples.')
    parser.add_argument('-p', '--project', type=str,
                    choices=list(x.replace(".ini","") for x in os.listdir(configs())),
                    required=True,
                    help='The identifier of the project to conduct an analysis for.')
                    
    parser.add_argument('-d', '--device', 
                    default=("cuda:0"),
                    help='Sets the device in which model and samples are loaded to.')

    args : argparse.Namespace = parser.parse_args()
    main(args)

