from shapely.geometry import Polygon, MultiPolygon
import alphashape
import rasterio.features as rf
import numpy as np
from typing import List
from operator import attrgetter

def relevance_mask(heatmap: np.ndarray, rlv_threshold: float) -> np.ndarray:
    #   only vals above 0
    hm = np.where(heatmap < 0, 0, heatmap)

    #   normalize data  (es gibt eine allgemeine Formel)
    min, max = np.min(hm), np.max(hm)
    div = max - min
    normalized = (hm - min) / (div if div != 0 else 1)
    
    #   threshold with percentile
    mask = np.where(normalized > rlv_threshold, 1, 0)
    return mask

def concave_poly(heatmap: np.ndarray, rel_thresh: int, alpha: int) -> MultiPolygon:        
    # get all regions of relevant pixels, which relevance values are above the threshold
    top_rels = relevance_mask(heatmap, rel_thresh)

    # convert to points with coords
    onlypoints = np.where(top_rels == 1)
    points = list(zip(onlypoints[1],onlypoints[0]))     #   zip(x,y)
    if len(points) == 0:
        return None

    #   draw polygons around regions
    try:    
        alpha_shape = alphashape.alphashape(points, alpha)
        if alpha_shape.geom_type == "LineString" or alpha_shape.geom_type == "Point":
            return None
        elif alpha_shape.area == 0:
            alpha_shape = alphashape.alphashape(points, 0.0)    #   convex hull instead
        
    except Exception as ex:
        if type(ex).__name__ == "QhullError":
    #   if points are arranged in a flat line, no concave hull can be 
    # computed (apparently not supported) -> compute a convex hull then
            alpha_shape = alphashape.alphashape(points, 0.0)
            if alpha_shape.geom_type == "LineString" or alpha_shape.geom_type == "Point":
                return None
        else:
            raise

    if type(alpha_shape) == Polygon:
        shapes = MultiPolygon([alpha_shape])
    else:
        shapes = MultiPolygon(alpha_shape)
    
    return shapes

def concave_poly_top_x(heatmap: np.ndarray, rel_thresh: int, alpha: int, top_x_polys: int = 1, criteria='relevance') -> List[Polygon]:
    criterias = ['relevance_max', 'relevance_sum', 'area']   #   order polygons after their maximum rel, or by their size
    if criteria in criterias == False:
        raise Exception(f"{criteria} value not acceptable for 'criteria'. Must be one of the following: {criterias}")

    shapes: MultiPolygon = concave_poly(heatmap, rel_thresh, alpha)

    top_x_polys_sorted = list()
    if shapes == None or len(shapes.geoms) < 1:
        return None #   no Polygons were computed
    elif len(shapes.geoms) == 1:
        top_x_polys_sorted.append(shapes.geoms[0])
    elif criteria == criterias[0] or criteria == criterias[1]:
        shapes_mr_merged = list()
        for shape in shapes.geoms:
            #   Convert shapely polygon into numpy raster, where points covered by polygon area is set to 1, the rest 0.
            shape_to_numpy = rf.rasterize([shape], out_shape=(heatmap.shape[0], heatmap.shape[1]))
            #   Find out what what highest relevance score of a rasterized polygon is on original heatmap.
            #   The assumption here is that relevant points are not arbitrarily spread.
            shape_rlvs = np.where(shape_to_numpy == 1, heatmap, 0)
            if criteria == criterias[0]:
                shapes_mr_merged.append((shape_rlvs.max(), shape))
            else:
                shapes_mr_merged.append((shape_rlvs.sum(), shape))

        #   The Polygon with the highest relevance value is ranked highest,
        #   the next Polygon is the one that contains the highes relevance value,
        #   ignoring all points already covered by the one before it, and so on...
        shapes_mr_merged.sort(key=lambda x: x[0], reverse=True)
        top_x_polys_sorted = list(list(zip(*shapes_mr_merged))[1])[:top_x_polys]
    elif criteria == criterias[2]:
        #   Polygons are ranked by their size, i.e. the biggest polygon is first in list the smalles the last.
        shape_list = shapes.geoms.sort(key=attrgetter('area'))
        top_x_polys_sorted = shape_list[:top_x_polys]
    else:
        raise Exception(f"Unexpected behaviour while sorting shapes.")

    return top_x_polys_sorted

