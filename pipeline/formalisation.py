from tqdm import tqdm
from typing import Dict, List
from common.structs import Predicate, Fact, Rule, Relation

MODEH = "is_class"

class Aleph:
    def __init__(self, file_template: str, rels: Dict[str,List[Relation]], out_file: str, file_record: str, \
                    seed: str, pos_only = False, exclude: List[str] = []):
        self.file_template = file_template
        self.rels = rels
        self.pos_only = False   #   not supported right now?
        self.exclude = exclude
        self.out_file = out_file
        self.file_record = file_record
        self.seed = seed

    def __get_domain_facts(self):
        return Aleph.domain_facts(self.rels)

    @staticmethod
    def domain_facts(rels: Dict[str, List[Relation]]) -> str:
        facts = ""

        #   dummys (BG rules need at least one definition of a literal in Prolog)
        facts += f"%dummy facts:\n"
        for rel in rels["unary"]:
            facts += f"{rel.name}_bg(dummy_a, dummy_b).\n"
        
        added_counterparts = []
        for rel in rels["binary"]:
            facts += f"{rel.name}_bg(dummy_a, dummy_b, dummy_c).\n"
            if rel.counterpart == rel.name:
                facts += f"{rel.name}_bg(A,B,C) :- {rel.name}_bg(A,C,B).\n"
            elif rel.counterpart != "" and not rel.name in added_counterparts:
                facts += f"{rel.counterpart}_bg(A,B,C) :- {rel.name}_bg(A,C,B).\n"
                added_counterparts.append(rel.counterpart)

        #   Transitivity rules
        facts += "\n%transitivity rules:\n"
        facts += "has_a(A,B) :- neg_bg(A,B) ; pos_bg(A,B).\n"
        facts += "neg(A,B) :- neg_bg(A,B).\n"
        facts += "pos(A,B) :- pos_bg(A,B).\n"
        # facts += "pos_bg(A,B) :- \+(neg_bg(A,B)).\n"

        for rel in rels["binary"]:
            facts += f"{rel.name}(A,pos(A,B),pos(A,C)) :- {rel.name}_bg(A,B,C),pos_bg(A,B),pos_bg(A,C).\n"
            facts += f"{rel.name}(A,pos(A,B),neg(A,C)) :- {rel.name}_bg(A,B,C),pos_bg(A,B),neg_bg(A,C).\n"
            facts += f"{rel.name}(A,neg(A,B),neg(A,C)) :- {rel.name}_bg(A,B,C),neg_bg(A,B),neg_bg(A,C).\n"
            facts += f"{rel.name}(A,neg(A,B),pos(A,C)) :- {rel.name}_bg(A,B,C),neg_bg(A,B),pos_bg(A,C).\n"

        facts += "\n"
        return facts

    def __write_background_section(self, bg_dict : Dict[str, Dict[str, Dict[str, list]]], file_handle, batch: int = 40) -> str:
        bg_str = self.__get_domain_facts()

        for sample in bg_dict:
            bg_str += f"sample({sample}).\n"
        bg_str += f"\n"

        i = 0
        for sample in tqdm(bg_dict, desc=f"Compile and write to Prolog"):
            i += 1
            bg_str += f"% [bg of sample {sample}]\n"

            for layer in bg_dict[sample]:
                if bg_dict[sample][layer]['unary'] != [None]:
                    for rel in bg_dict[sample][layer]['unary']:
                        if rel[0] == "has_a":
                            continue
                        fact: Fact = Fact(rel[0] + "_bg", sample, f"{layer}_c{rel[1]}")
                        bg_str += fact.to_prolog_str() + "\n"

                if bg_dict[sample][layer]['binary'] != [None]:
                    for rel in bg_dict[sample][layer]['binary']:
                        fact: Fact = Fact(rel[0] + "_bg", sample, f"{layer}_c{rel[1]}", f"{layer}_c{rel[2]}")
                        bg_str += fact.to_prolog_str() + "\n"
            if i < batch:
                continue
            else:
                # dump batch
                file_handle.write(bg_str)
                i = 0
                bg_str = ""

        # the rest
        file_handle.write(bg_str)

    def __get_mode_determ_decl(self) -> str:
        mode_decl = ":-" + Fact("modeh", '*', Predicate(MODEH, '+sample')).to_prolog_str() + "\n\n"
        determ_decl = ""

        for rel in self.rels['unary']:
            if rel.name in self.exclude:
                continue
            mode_decl   += ":-" + Fact("modeb", '*', Predicate(rel.name, '+sample', '#filter')).to_prolog_str() + "\n"
            determ_decl += ":-" + Fact("determination", MODEH + "/1", rel.name + "/2").to_prolog_str() + "\n"

        mode_decl   += "\n"
        determ_decl += "\n"

        for rel in self.rels['binary']:
            if rel.name in self.exclude:
                continue
            pos_fil = Predicate("pos", "+sample", "#filter")
            neg_fil = Predicate("neg", "+sample", "#filter")
            mode_decl   += ":-" + Fact("modeb", '*', Predicate(rel.name, '+sample', pos_fil, pos_fil)).to_prolog_str() + "\n"
            mode_decl   += ":-" + Fact("modeb", '*', Predicate(rel.name, '+sample', pos_fil, neg_fil)).to_prolog_str() + "\n"
            mode_decl   += ":-" + Fact("modeb", '*', Predicate(rel.name, '+sample', neg_fil, pos_fil)).to_prolog_str() + "\n"
            mode_decl   += ":-" + Fact("modeb", '*', Predicate(rel.name, '+sample', neg_fil, neg_fil)).to_prolog_str() + "\n"

            determ_decl += ":-" + Fact("determination", MODEH + "/1", rel.name + "/3").to_prolog_str() + "\n"

        return mode_decl + "\n" + determ_decl

    def create_file(self, out_file : str,   \
        bg_dict : Dict[str, Dict[str, Dict[str, list]]],   \
            pos : List[str], neg : List[str], file_record: str,   \
                rnd_seed: str) :
        data = ""
        with open(self.file_template, 'r') as file:
            data = file.read()

        data = data.replace("[evalfn]", ":- aleph_set(evalfn,coverage)." if len(neg) > 0 else ":- aleph_set(evalfn,posonly).")
        data = data.replace("[random_seed]", f":- set_random(seed({rnd_seed})).")
        data = data.replace("[recordfile]", f":- aleph_set(recordfile,'{file_record}').")
        data = data.replace("[modes]", self.__get_mode_determ_decl())
        
        with open(out_file, 'w') as file:
            file.write(data)
        
        with open(out_file, 'a') as file:
            # file.write(":-begin_bg.\n\n")
            self.__write_background_section(bg_dict, file)
            file.write(":-end_bg.\n\n")

            file.write(":-begin_in_pos.\n\n")
            file.write("\n".join(Fact(MODEH, item).to_prolog_str() for item in pos))
            file.write("\n:-end_in_pos.\n\n")

            file.write(":-begin_in_neg.\n\n")
            file.write("\n".join(Fact(MODEH, item).to_prolog_str() for item in neg))
            file.write("\n:-end_in_neg.\n")

    def write_head(self):
        data = ""
        with open(self.file_template, 'r') as file:
            data = file.read()

        data = data.replace("[evalfn]", ":- aleph_set(evalfn,posonly)." if self.pos_only else ":- aleph_set(evalfn,coverage).")
        data = data.replace("[random_seed]", f":- set_random(seed({self.seed})).")        
        data = data.replace("[recordfile]", f":- aleph_set(recordfile,'{self.file_record}').")
        data = data.replace("[modes]", self.__get_mode_determ_decl())

        with open(self.out_file, 'w') as file:
            file.write(data)

        self.file_handle = open(self.out_file, 'a')
        self.file_handle.write(self.__get_domain_facts())
    
    def write_append(self, sample_name, sample_bg):
        bg_str = f"\n% [bg of sample {sample_name}]\n"

        for layer in sample_bg:
            if sample_bg[layer]['unary'] != [None]:
                for rel in sample_bg[layer]['unary']:
                    if rel[0] == "has_a":
                        continue
                    fact: Fact = Fact(rel[0] + "_bg", sample_name, f"{layer}_c{rel[1]}")
                    bg_str += fact.to_prolog_str() + "\n"

            if sample_bg[layer]['binary'] != [None]:
                for rel in sample_bg[layer]['binary']:
                    fact: Fact = Fact(rel[0] + "_bg", sample_name, f"{layer}_c{rel[1]}", f"{layer}_c{rel[2]}")
                    bg_str += fact.to_prolog_str() + "\n"
        
        self.file_handle.write(bg_str)

    def write_tail(self, pos : List[str], neg : List[str]):
            self.file_handle.write(":-end_bg.\n\n")

            self.file_handle.write(":-begin_in_pos.\n")
            self.file_handle.write("\n".join(Fact(MODEH, item).to_prolog_str() for item in pos))
            self.file_handle.write("\n:-end_in_pos.\n\n")

            self.file_handle.write(":-begin_in_neg.\n")
            self.file_handle.write("\n".join(Fact(MODEH, item).to_prolog_str() for item in neg))
            self.file_handle.write("\n:-end_in_neg.\n")
            self.file_handle.close()

    def add_constraint(predicates: List[Predicate], bg_aleph: str, bg_aleph_constr: str):
        hypothesis = Predicate("hypothesis", "_", "Body", "_")

        constraints = list()
        for pred in predicates:
            aleph_in = Predicate("aleph:in", "Body", pred, "_", "_")
            aleph_false = Rule(Predicate("aleph_false"), hypothesis, aleph_in)
            constraints.append(aleph_false)

        with open(bg_aleph, mode="r") as source:
            with open(bg_aleph_constr, mode="w") as dest: 
                placeholder = "% [CONSTRAINTS]\n"

                line = ""
                while line != placeholder:
                    line = source.readline()
                    dest.write(line)

                for constraint in constraints:
                    constraint_str = constraint.to_prolog_str(True) + "\n"
                    dest.writelines(constraint_str)

                # Write the rest in chunks.
                while True:
                    data = source.read(1024)
                    if not data:
                        break
                    dest.write(data)