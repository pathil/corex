import os
from pyswip import Prolog
from multiprocessing import Process, Queue
import common.helper as helper
from common.structs import Rule

def induce(filename : str, record_file: str):
    #   needs to be deleted before calling aleph via pyswip. 
    if os.path.exists(record_file):
        os.remove(record_file)

    # induce in another process to ensure filestream on recordfile is being closed,
    # otherwise, there might appear file access errors
    q = Queue()
    p = Process(target=__induce_aleph, args=(q, filename))
    p.start()
    print(q.get(timeout=600))
    p.join()

def __induce_aleph(q, filename):
    try:
        prolog = Prolog()
        response = list(prolog.query(f"working_directory(CWD, CWD)."))
        cwd = response[0].get('CWD')

        ilp_dir = os.path.dirname(filename).replace('\\','/')

        if cwd != ilp_dir.lower():
            response = list(prolog.query(f"working_directory(_, '{ilp_dir}')."))

        consult_name = os.path.basename(filename)
        prolog.consult(consult_name,catcherrors=(True))
        query = prolog.query(f"induce(M).")
        result = list(query)[0].get('M')

        # It was necessary at the beginning. Uncomment if pyswip, does strange stuff at this point.
        # query.close()
        # prolog.query("halt.")
        # del prolog
    except Exception as ex:
        result = f"Error occured while trying to induce {filename}:\n{ex}"
        raise Exception(ex)
    finally:
        q.put(result)

def parse_recordfile(recordfile : str) -> list:
    '''        
    Parses Aleph-Recordfile for Rules. Necessary with prolog on windows.
    Aleph does not save the Rule-file. Although it works on Linux, use general
    approach for both. 

    recordfile = path to Aleph Record-file
    returns list of parsed Rules
    '''
    rules = []
    with open(recordfile) as file:
        for line in file:
            if '[theory]' in line:
                break
        for line in file:
            if "[Rule " in line:
                line = file.readline()
                if ":-" in line:
                    concatenated_rule = line
                    
                    # iterate over lines until end of rule.
                    for line in file:
                        if ")." in line:
                            concatenated_rule = concatenated_rule + line
                            rule: Rule = helper.parse_rule(concatenated_rule.replace("\n",""))
                            rule.head.name = rule.head.name + f"_{len(rules) + 1}"
                            rules.append(rule)
                            break
                        else:
                            concatenated_rule = concatenated_rule + line
                # We dont need rules for single samples. These are outliers. Uncomment if you do.
                # else:
                #     facts.append(helper.parse_single_fact(line))
    return rules

