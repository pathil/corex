from math import ceil
from typing import List, Tuple
import torch
from torch import Tensor
from PIL import Image
import torchvision.transforms as T

from crp.concepts import ChannelConcept
from zennit.composites import EpsilonPlusFlat, EpsilonPlus
from zennit.canonizers import SequentialMergeBatchNorm
from crp.attribution import CondAttribution

from common.loader import ComposerInfo

class ConceptRetriever:
    def __init__(self, model : torch.nn.Module, composer_info: ComposerInfo, device : str = "cuda:0" if torch.cuda.is_available() else "cpu"):
        self.device = device
        model.eval()
        model.to(device)
        self.model = model
        self.composer_info = composer_info
        self.sample = None

        self.composite = EpsilonPlusFlat([SequentialMergeBatchNorm()])
        self.attribution = CondAttribution(model)
        self.cc = ChannelConcept()
        self.setup_sample()

    def __del__(self):
        if self.model != None:
            del self.model
        if self.sample != None:
            del self.sample
        # del self.composite
        # del self.attribution
        # del self.cc

    @staticmethod
    def load_sample_img(path : str) -> Image:
        image = Image.open(path)
        return image

    def setup_sample(self, image : Image = None, layer : str = None, class_id : int = None, from_layer : bool = False):
        if self.sample != None:
            del self.sample
        if image != None:
            transform = self.composer_info.get_composer()
            self.sample = transform(image.convert('RGB')).unsqueeze(0).to(self.device)
            self.sample.requires_grad = True
        self.layer = layer
        self.class_id = class_id
        self.start_from_layer = from_layer


    def _check_arguments(self, concept_ids : List[int]):
        err = ""
        if self.sample == None :
            err = "sample"
        elif self.layer == None :
            err = "layer"
        elif concept_ids == None:
            err = "concept_ids"
        if err != "":
            raise f"'{err}' must NOT be None."
        if self.class_id == None and self.start_from_layer == False:
            raise "'class-id must be set if backward pass starts from output."

    def get_concepts(self, concept_ids : List[int]) -> List[Tuple[int,Tensor]]:
        self._check_arguments(concept_ids)
        if self.start_from_layer:
            conditions = [{self.layer: [id]} for id in concept_ids]
            attr = self.attribution(self.sample, conditions, self.composite, start_layer=self.layer)
        else:
            conditions = [{self.layer: [id], 'y': [self.class_id]} for id in concept_ids]
            attr = self.attribution(self.sample, conditions, self.composite)

        id_heatmap_merged = [(concept_ids[i], attr.heatmap[i]) for i in range(len(attr.heatmap))]
        return id_heatmap_merged

    def relevant_filters_by_quantil(self, q : float) -> Tuple[List, Tensor]:
        if self.class_id == None:
            raise "'class_id' must be set to determine most relevant concepts for a class."
        conditions = [{'y': [self.class_id]}]
        attr = self.attribution(self.sample, conditions, self.composite, record_layer=[self.layer])
        rlv_c = self.cc.attribute(attr.relevances[self.layer], abs_norm=True)
        
        selected = ConceptRetriever.abs_rlvs_x_quantile(rlv_c[0].tolist(), q)

        return selected, rlv_c[0]

    def abs_rlvs_x_quantile(filter_rlvs, q=0.1, remove_zeros=True):
        '''
            filter_rlvs: ordered list of filter-relevances
            a: quantile
            remove_zeros: omit the zero-valued
            selects the xth quantile of the absolute valued list of filter relevances of a layer.
            On default zeros will be ommitted.
            Returns filter rlvs in orginal order, but only with those filters, that are within the quantile.
        '''
        #   remember filter-idx and which relevances are pos and neg before abs
        rlvs_annotated = []
        for i in range(len(filter_rlvs)):
            sign = 0
            if filter_rlvs[i] > 0:
                sign = 1
            elif filter_rlvs[i] < 0:
                sign = -1
            rlvs_annotated.append((filter_rlvs[i], sign, i))

        # # abs and sort descending
        rlvs_abs = [(abs(rlv[0]), rlv[1], rlv[2]) for rlv in rlvs_annotated]    #   absolut value of all
        rlvs_abs.sort(key = lambda x: x[0], reverse=True)   #
        # # only quantile
        quantile = ceil(len(rlvs_abs) * q)
        rlv_quantiled = rlvs_abs[:quantile]

        # # remove all filters that have no impact (the zeros)
        if (remove_zeros):
            rlv_quantiled = [f for f in rlv_quantiled if f[1] != 0]

        # # convert back
        result = [(f[2], f[0] * f[1]) for f in rlv_quantiled]
        result.sort(key=lambda x: x[1])
        
        return result
