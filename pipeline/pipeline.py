import os
import traceback
from typing import Dict, List, Tuple

import numpy as np
from tqdm import tqdm

from common.helper import sample_img_path
from common.structs import ConceptInfo, SampleInfo, LayerInfo, PipelineInfo
from common.paths import ProjectNavigator
from common.logging import Logger

from common.config import Config
from common.loader import LOADER, COMPOSER, mask

from shapely.geometry import Polygon
from pipeline.formalisation import Aleph

import torch.multiprocessing as mp
from torch.multiprocessing import Queue, Process
from multiprocessing.connection import Pipe, PipeConnection

import time

from datetime import datetime, timezone

def time_offset_format(from_timestamp: float) -> str:
    return datetime.fromtimestamp(time.time()-from_timestamp, tz=timezone.utc).strftime('%H:%M:%S')

def invert_list_dict(list_dict: Dict[str, List]):
    inv_dict = dict()
    for k,v in list_dict.items():
        for item in v:
            inv_dict[item] = k
    return inv_dict

class Procedure:
    def __init__(self, pn: ProjectNavigator, logger: Logger) -> None:
        self.queue = Queue()
        self.prev = None
        self.next: Procedure = None
        self.pn = pn
        self.config = pn.config
        self.log = logger
        self.ended = False
        self.name = f"{type(self).__name__}"
        self.conn_rcv: PipeConnection = None
        self.conn_snd: PipeConnection = None

    def worker(self):
        self.log.info("Started worker. Wait for items...")

        if self.conn_rcv == None:
            self.log.error("Receiving PipeConnection may not be None")
            raise Exception()

        t_0 = time.time()

        self._prepare()

        pbar = None
        finished = 0
        while True:
            try:
                task = self.queue.get(timeout=None)

                if self.conn_rcv.poll():    #   info only if prev Procedure terminated
                    #   I prefer tqdm over countdowns:
                    #   self.upd_info(f"%d items remaining in queue. ({time_offset_format(t_0)})" % self.queue.qsize())
                    if type(pbar) == type(None):
                        pbar = tqdm(desc=f"[{self.name}]", total=(finished + self.queue.qsize()))
                        pbar.update(finished)
                    else:
                        pbar.total = finished + self.queue.qsize()

                if task != None:
                    self._do_work(task)
                    if type(pbar) != type(None):
                        pbar.update(1)
                else:
                    self._follow_up()
                    pbar.close()
                    self.put_next_sentinel()
                    break
                
                finished += 1
            except Exception as ex:
                if type(ex).__name__ == "Empty":    #   Are timeouts useful in this application?
                    self.log.warning("No more items in queue. Leave Procedure now.")
                    self.put_next_sentinel()
                    break
                else:                    
                    self.log.error(f"An error has occured. Stop worker...\nException: {ex}\nTraceback:\n{traceback.print_exc()}")
                    self.put_next_sentinel()
                    raise ex
            
        self.log.info("Completed. Shutdown worker. (%s)" % time_offset_format(t_0))

    def _prepare(self):
        '''
        Called one time before entering workers loop for preprocessing. Same thread as _do_work().
        '''
        pass

    def _do_work(self, input: Tuple[PipelineInfo, object]):
        '''
        The routine which handles a single instance of the queue.
        '''
        pass

    def _follow_up(self):
        '''
        Called one time before exiting workers loop for postprocessing. Same thread as _do_work().
        '''
        pass

    def starter(self):
        '''
        Is called, when Procedure is the first in line, i.e. program was started with this step. Supposed to run on main thread.
        '''
        self.queue.put(None)

    def put_next(self, item: Tuple[PipelineInfo, object]):
        '''
        Puts a new task in form of a data (object) with metadata (PipelineInfo) on the next Procedures queue.
        '''
        if self.next != None:
            self.next.queue.put(item)

    def put_next_sentinel(self):
        '''
        Puts a single flag on the queue of the next Procedure in line. 
        This indicates, that this Procedure will not send any more tasks.
        '''
        if self.next != None:
            self.next.queue.put(None)
            self.conn_snd.send(True)    #   inform the conn_rcv

    def upd_info(self, info: str):
        print(f"[{self.name}]: {info}", end="\r")

class Preprocessing(Procedure):
    def __init__(self, device, pn: ProjectNavigator, logger: Logger, concepts_masked: List[int]) -> None:
        super().__init__(pn, logger)

        self.device = device
        self.concepts_masked = concepts_masked
        self.is_mode_masked = len(concepts_masked) > 0

        self.ground_truth = dict(pos=[], neg=[])
        self.model_truth = dict(pos=[], neg=[])

    def _prepare(self):
        from pipeline.preprocessing import Preprocessor

        #   pity, pytorch model can not really be shared among processes. If you 
        #   run out of RAM, then I have no other solution for you other then run step 0 and 1 after one another.
        model = LOADER[self.config.General.model](self.device)
        
        if self.is_mode_masked:
            model = mask(model, self.config.CRP.layers[0], self.concepts_masked, self.device)
        comp_info = COMPOSER[self.config.General.composer]
        self.preproc = Preprocessor(model, self.device, comp_info)
        # self.model.to(self.device)
        # self.model.share_memory()

    def _do_work(self, input):
        file: str = input[0]
        class_gt: str = input[1]
        
        sample = os.path.splitext(os.path.basename(file))[0]
        self.ground_truth[class_gt].append(sample)

        mt = self.preproc.covered_by_mt(file, self.config.General.model_pos, self.config.General.model_neg)

        class_mt = ""
        if mt[0]:
            class_mt = "pos"
        if mt[1]:
            class_mt = "neg"

        #   if a sample is classified neither with config.General.model_pos nor [...].model_neg, drop it
        if class_mt != "":
            s_info = SampleInfo(sample_name=sample, \
                                class_mt=self.config.General.model_pos if class_mt == "pos" else self.config.General.model_neg, \
                                num_layers=len(self.config.CRP.layers), \
                                path=file)

            self.put_next(s_info)
            self.model_truth[class_mt].append(s_info.name)

    def _follow_up(self):
        self.log.info(f"Ground Truth: {len(self.ground_truth['pos'])} positives and {len(self.ground_truth['neg'])} negatives")
        self.log.info(f"Model Truth: {len(self.model_truth['pos'])} positives and {len(self.model_truth['neg'])} negatives")
        self.pn.file.ground_truth().write(self.ground_truth)
        self.pn.file.model_truth().write(self.model_truth)

        del self.preproc

    def starter(self):
        for root, dir, files in os.walk(self.config.General.samples):
            parent, class_gt = os.path.split(root)

            for file in files:
                if not file.endswith(self.config.General.file_ending):
                    continue
                if class_gt != "pos" and class_gt != "neg":
                    continue

                self.queue.put((os.path.join(root, file), class_gt))

        super().starter()

    @staticmethod
    def all_with_filending(path: str, file_ending: str):
        collection = list()
        for root, dir, files in os.walk(path):
            for file in files:
                if(file.endswith(file_ending)):
                    collection.append(path / file)
        return collection

class CRP(Procedure):
    def __init__(self, device, pn: ProjectNavigator, logger: Logger, is_mode_ignore: bool, concepts_masked: List[int]) -> None:
        super().__init__(pn, logger)

        self.device = device
        self.layer = self.config.CRP.layers
        self.model_pos = self.config.General.model_pos
        self.model_neg = self.config.General.model_neg
        self.filter_quantile = self.config.CRP.filter_quantil
        self.samples_dir = self.config.General.samples
        self.is_mode_ignore = is_mode_ignore
        self.concepts_masked = concepts_masked
        self.is_mode_masked = len(concepts_masked) > 0

    def _prepare(self):
        from pipeline.concepts import ConceptRetriever

        comp_info = COMPOSER[self.config.General.composer]
        model = LOADER[self.config.General.model](self.device)
        #   TODO: test
        if self.is_mode_masked:
            model = mask(model, self.config.CRP.layers[0], self.concepts_masked, self.device)
        self.cr = ConceptRetriever(model, comp_info, self.device)

    def _do_work(self, input):
        from PIL import Image

        sample_info: SampleInfo = input

        sample = sample_info.name
        class_mt = sample_info.class_mt

        filter_quantil = self.config.CRP.filter_quantil

        img_path = sample_img_path(self.samples_dir, sample)
        img = Image.open(img_path)

        for layer in self.config.CRP.layers:
            self.cr.setup_sample(img, layer, class_mt, from_layer=False)
            # cr.sample.share_memory_()
        
            quantiled, layer_rlvs = self.cr.relevant_filters_by_quantil(filter_quantil)

            rlv_order = [x[0] for x in quantiled]

            self.pn.file.rlv_order(sample, layer, masked=self.is_mode_masked).write(rlv_order)
            self.pn.file.layer_rlvs(sample, layer, masked=self.is_mode_masked).write(layer_rlvs.cpu().numpy())
            
            layer_info = LayerInfo(layer, sample_info, len(quantiled))

            # positives = [x for x in quantiled if x[1] > 0]
            for c_id, rel in tqdm(quantiled, desc=f"[{self.name}]", leave=False):
                info = ConceptInfo(id=c_id, layer_info=layer_info, rlv=layer_rlvs[int(c_id)].item())

                rlv_map_acc = self.pn.file.rlv_map(sample, layer, c_id)                

                try:
                    if self.is_mode_ignore and os.path.exists(rlv_map_acc):
                        heatmap = rlv_map_acc.read()
                    else:
                        #   Relevance Map for c_id
                        id, heatmap = self.cr.get_concepts([c_id])[0]

                        # Save relevance tensor
                        rlv_map_acc.write(heatmap)

                    #   hand over to [localization]
                    hm = heatmap.cpu().numpy()
                    self.put_next((info, hm))

                    del heatmap

                except Exception as ex:
                    err = f"Error with file {os.path.basename(img_path)} on layer {layer} concept {c_id}:\n{ex}"
                    self.log.error(err)
                    raise Exception(err)
                
            del quantiled
            del layer_rlvs

    def _follow_up(self):
        # model not needed anymore after step 1: free the gpu/ram
        # del self.model
        del self.cr

    def starter(self):
        model_truth = self.pn.file.model_truth().read()

        for target in self.pn.file.model_truth().read():
            for sample in model_truth[target]:
                class_mt = self.config.General.model_pos if target == 'pos' else self.config.General.model_neg
                self.queue.put(SampleInfo(sample, class_mt, len(self.config.CRP.layers)))

        super().starter()

class Localisation(Procedure):
    def __init__(self, pn: ProjectNavigator, logger: Logger, is_mode_ignore: bool, is_mode_masked: bool) -> None:
        super().__init__(pn, logger)

        self.layers = self.config.CRP.layers
        self.rel_threshold = self.config.Localization.rel_mask_threshold
        self.alpha = self.config.Localization.alpha_val
        self.is_mode_ignore = is_mode_ignore
        self.is_mode_masked = is_mode_masked

        self.top_x = 2   #   only amid-relation considers first 2. All other only the first. Lets save space and use always two. Additional config-option would be confusing.
        self.criteria = 'relevance_max' #   performance of other criterias should be investigated in the future.

    def _do_work(self, input):
        import pipeline.localisation as localisation

        c_info: ConceptInfo = input[0]
        heatmap: np.ndarray = input[1]
        
        sample = c_info.layer_info.sample_info.name
        layer = c_info.layer_info.name

        shapes = self.pn.file.shapes(sample, layer, c_info.id)

        try:
            if self.is_mode_ignore and shapes.path.exists():
                polys = shapes.read()
            else:
                # flip pixel relevances if filter has negative relevance
                if c_info.rlv < 0:
                    heatmap = heatmap * -1

                # compute polygons
                polys = localisation.concave_poly_top_x(heatmap, self.rel_threshold, self.alpha, self.top_x, self.criteria)

                #   If a shape is generated (again), then there has been made at least 1 modification, which affects the final sample BG.
                #   Remove it, so the Relation-Generation is forced to update the samples relations.
                sample_bg = self.pn.file.sample_bg(sample, masked=self.is_mode_masked)
                if self.is_mode_ignore and sample_bg.path.exists():
                    os.remove(sample_bg)

            #   TODO: Test with poly==None
            if polys == None:
                self.log.warning(f"Resulting shape is None, therefore concept will be ignored: concept {c_info.id} of layer '{layer}' of sample {sample}.")
                self.put_next((c_info, None))
            else:
                # Save polygons
                shapes.write(polys)
                #   hand over to relations
                self.put_next((c_info, polys))

        except Exception as ex:
            err = f"Error during shape generation with sample {sample} and concept {c_info.id} in layer {layer}:\n{ex}"
            self.log.error(err)
            raise Exception(err)

    def starter(self):
        model_truth: Dict = self.pn.file.model_truth().read()
        mt_inv = invert_list_dict(model_truth)

        for sample in mt_inv:
            class_mt = self.config.General.model_pos if mt_inv[sample] == "pos" else self.config.General.model_neg
            s_info = SampleInfo(sample, class_mt, len(self.layers))

            for layer in self.layers:
                quantiled = self.pn.file.rlv_order(sample, layer, masked=self.is_mode_masked).read()
                l_rlvs = self.pn.file.layer_rlvs(sample, layer, masked=self.is_mode_masked).read()
                l_info = LayerInfo(layer, s_info, len(quantiled))

                for c_id in quantiled:
                    shapes = self.pn.file.shapes(sample, layer, c_id) 

                    if self.is_mode_ignore and shapes.path.exists():
                        continue
                    try:
                        # Load heatmap
                        heatmap = self.pn.file.rlv_map(sample, layer, c_id).read().cpu().numpy()

                        c_info = ConceptInfo(id=c_id, layer_info=l_info, rlv=l_rlvs[int(c_id)])
                        self.queue.put((c_info, heatmap))

                    except Exception as ex:
                        err = f"Error during shape generation with sample {sample} and concept {c_id} in layer {layer}:\n{ex}"
                        self.log.error(err)
                        raise Exception(err)

        super().starter()

class Relations(Procedure):
    def __init__(self, pn: ProjectNavigator, logger: Logger, is_mode_ignore: bool, is_mode_masked: bool) -> None:
        super().__init__(pn, logger)

        self.is_mode_ignore = is_mode_ignore
        self.is_mode_masked = is_mode_masked

        self.sets = self.config.Relations.sets
        self.tolerance = self.config.Relations.intersect_tolerance
        composer_info = COMPOSER[self.config.General.composer]
        self.img_shape = (composer_info.crop_h, composer_info.crop_w)
        self.layers = self.config.CRP.layers


    def _prepare(self):
        self.sample_rels_dict : Dict[str, Dict[str, list]] = dict()
        self.sample_concepts_dict: Dict[str, Dict[str, List[tuple]]] = dict()

    def _do_work(self, input):
        import pipeline.knowledge as knowledge

        #   Since relations can be constructed only layerwise, we need to wait until all
        #   concepts of a layer have been received. Only then, we can compute relations. 

        c_info: ConceptInfo = input[0]
        polys: List[Polygon] = input[1]

        sample = c_info.layer_info.sample_info.name
        layer = c_info.layer_info.name

        if sample not in self.sample_concepts_dict:
            self.sample_concepts_dict[sample] = dict()
            self.sample_rels_dict[sample] = dict()

        if layer not in self.sample_concepts_dict[sample]:
            self.sample_concepts_dict[sample][layer] = list()

        self.sample_concepts_dict[sample][layer].append((c_info.id, polys))

        #   Have all concepts of a layer been received? Then generate their relations.
        if len(self.sample_concepts_dict[sample][layer]) == c_info.layer_info.num_concepts:
            # omit concepts with poly==None. Maybe do that a bit more elegant?
            layer_concepts = [c for c in self.sample_concepts_dict[sample][layer] if c[1] != None]
            l_rlvs = self.pn.file.layer_rlvs(sample, layer).read()

            try:
                sample_bg = self.pn.file.sample_bg(sample, masked=self.is_mode_masked)
                if self.is_mode_ignore and sample_bg.path.exists(): #   no changes in this layer. But maybe in another layer. Not clear at this point.
                    sample_rels = sample_bg.read()
                    rel_dict = sample_rels[layer]
                else:
                    rel_dict = knowledge.generate_layer_relations(layer_concepts, l_rlvs, self.sets, self.tolerance, self.img_shape)

                if sample not in self.sample_rels_dict[sample]:
                    self.sample_rels_dict[sample][layer] = dict()

                self.sample_rels_dict[sample][layer] = rel_dict
                self.sample_concepts_dict[sample].pop(layer)

            except Exception as ex:
                err = f"Error with sample {sample} in layer {layer}:\n{ex}"
                self.log.error(err)
                raise Exception(err)

        #   Have all layer-relations of a sample been computed? Then store sample_bg and handover to next.
        if len(self.sample_rels_dict[sample]) == c_info.layer_info.sample_info.num_layers:
            sample_bg = self.pn.file.sample_bg(sample, masked=self.is_mode_masked)
            if self.is_mode_ignore and sample_bg.path.exists():
                sample_rels = sample_bg.read()
            else:
                sample_rels = self.sample_rels_dict[sample]

            self.put_next((c_info.layer_info.sample_info, sample_rels))
            sample_bg.write(sample_rels)

            self.sample_rels_dict.pop(sample)
            self.sample_concepts_dict.pop(sample)

    def _follow_up(self):
        if len(self.sample_rels_dict) > 0:
            self.log.warning(f"Not all dicts have been committed.")

    def starter(self):
        model_truth: Dict[str,str] = self.pn.file.model_truth().read()
        mt_inv = invert_list_dict(model_truth)

        # for each concept in each layer
        for sample in mt_inv:
            class_mt = self.config.General.model_pos if mt_inv[sample] == "pos" else self.config.General.model_neg
            s_info = SampleInfo(sample, class_mt, len(self.layers))

            for layer in self.layers:
                if self.is_mode_ignore and self.pn.file.sample_bg(sample).path.exists():
                    continue

                l_order = self.pn.file.rlv_order(sample, layer, masked=self.is_mode_masked).read()
                l_rlvs = self.pn.file.layer_rlvs(sample, layer, masked=self.is_mode_masked).read()

                layer_concepts : List[Tuple[int, List[Polygon]]] = []

                for concept in l_order:
                    #   if no shape could be constructed, neither a relation can be
                    shapes = self.pn.file.shapes(sample, layer, concept)
                    if not shapes.path.exists():
                        continue

                    # Load polygon
                    layer_concepts.append((int(concept), shapes.read()))
                    
                l_info = LayerInfo(layer, s_info, len(layer_concepts))

                for c_id, polys in layer_concepts:
                    c_info = ConceptInfo(c_id, l_info, l_rlvs[c_id].item())
                    self.queue.put((c_info, polys))

        super().starter()

class Formalisation(Procedure):
    def __init__(self, pn: ProjectNavigator, logger: Logger, is_mode_ignore: bool, is_mode_masked: bool) -> None:
        super().__init__(pn, logger)

        self.is_mode_ignore = is_mode_ignore
        self.is_mode_masked = is_mode_masked

    def _prepare(self):
        from common.relations import set_relations

        ilp_dir = self.pn.dir.ilp() 
        os.makedirs(ilp_dir, exist_ok=True)

        set_rels = set_relations(self.config.Relations.sets)
        pos_only = self.config.Aleph.pos_only
        seed = self.config.Aleph.random_seed
        exclude = self.config.Aleph.exclude
        record_out = self.pn.file.aleph_record()
        bk_aleph = self.pn.file.bk_aleph(constr=False)
        aleph_struct = self.pn.file.aleph_struct().path

        self.aleph: Aleph = Aleph(aleph_struct, set_rels, bk_aleph, record_out.path.name, seed, pos_only, exclude)

        self.pos_list: List[str] = []
        self.neg_list: List[str] = []
        self.aleph.write_head()

    def _do_work(self, input):
        s_info: SampleInfo = input[0]
        sample_bg = input[1]

        if s_info.class_mt == self.config.General.model_pos:
            self.pos_list.append(s_info.name)
        else:
            self.neg_list.append(s_info.name)

        self.aleph.write_append(s_info.name, sample_bg)

    def _follow_up(self):
        self.aleph.write_tail(self.pos_list, self.neg_list)
        self.put_next(True)     #   next step only does not require a specific input. It just has to be triggered somehow.

    def starter(self):
        model_truth: Dict[str,str] = self.pn.file.model_truth().read()

        for target in model_truth:
            for sample in model_truth[target]:
                class_mt = self.config.General.model_pos if target == 'pos' else self.config.General.model_neg

                sample_bg = self.pn.file.sample_bg(sample, self.is_mode_masked).read()

                self.queue.put((SampleInfo(sample, class_mt, len(self.config.CRP.layers)), sample_bg))

        super().starter()

class ILP(Procedure):
    def __init__(self, pn: ProjectNavigator, logger: Logger, is_mode_masked: bool) -> None:
        super().__init__(pn, logger)

        self.is_mode_masked = is_mode_masked

    def _prepare(self):
        self.file_bk_aleph  = self.pn.file.bk_aleph(constr=False)
        self.file_record    = self.pn.file.aleph_record()
        self.file_rules     = self.pn.file.rules(constr=False)

    def _do_work(self, input):
        from pipeline.ilp import induce, parse_recordfile
        from common.structs import Rule

        try:
            induce(self.file_bk_aleph, self.file_record)
            rules: List[Rule] = parse_recordfile(self.file_record)
        except Exception as ex:
            err = f"Induction failed: {ex}\nTraceback:\n{traceback.print_exc()}"
            self.log.error(err)
            raise Exception(err)
        #TODO: Test this
        if len(rules) > 0:
            self.file_rules.write(rules)
            self.log.info(f"Rules found: {len(rules)}")
            rules_str = '\n'.join([rule.to_prolog_str(format=True) for rule in rules])
            self.log.info(f"Rules:\n{rules_str}")
        else:
            self.log.error(f"No rules computed.")

    def _follow_up(self):
        pass

    def starter(self):
        self.queue.put(True)

        super().starter()

class Pipeline:
    SEQUENCE_DICT: Dict[int, Procedure] = {
        0: Preprocessing,
        1: CRP,
        2: Localisation,
        3: Relations,
        4: Formalisation,
        5: ILP,
    }

    def __init__(self, config: Config, device: str, logger: Logger, ignore_existing: bool, concepts_masked: bool) -> None:
        self.procedures: List[Procedure] = list()

        self.pn = ProjectNavigator(config)
        self.device = device
        self.ignore_existing = ignore_existing
        self.concepts_masked = concepts_masked
        self.is_mode_masked = len(concepts_masked) > 0
        self.log = logger

    def ProcedureFactory(self, name: str) -> Procedure:
        procs = {
            Preprocessing.__name__: lambda: Preprocessing(self.device, self.pn, self.log, self.concepts_masked),
            CRP.__name__: lambda: CRP(self.device, self.pn, self.log, self.ignore_existing, self.concepts_masked),
            Localisation.__name__: lambda: Localisation(self.pn, self.log, self.ignore_existing, self.is_mode_masked),
            Relations.__name__: lambda: Relations(self.pn, self.log, self.ignore_existing, self.is_mode_masked),
            Formalisation.__name__: lambda: Formalisation(self.pn, self.log, self.ignore_existing, self.is_mode_masked),
            ILP.__name__: lambda: ILP(self.pn, self.log, self.is_mode_masked)
        }
        
        return procs[name]()

    def assemble(self, steps: Tuple[int,int]):
        i = steps[0]
        while i <= steps[1]:
            proc = self.ProcedureFactory(Pipeline.SEQUENCE_DICT[i].__name__)
            if len(self.procedures) > 0:
                self.procedures[-1].next = proc
                proc.prev = self.procedures[-1]
            self.procedures.append(proc)
            i += 1

    def produce(self):
        t_start = time.time()
        mp.set_start_method('spawn', force=True)    #   necessary for working with cuda in subprocesses 

        jobs: List[Process] = []

        for i in range(len(self.procedures)):
            #   The worker of each Procedure runs in its own process
            proc = self.procedures[i]
            process = mp.Process(target=proc.worker, name=f"p_{type(proc).__name__}")

            #   Pipe Connection to communicate previous Procedures termination to current
            if i != 0:
                conn_rcv, conn_snd = Pipe()
                proc.conn_rcv = conn_rcv
                self.procedures[i-1].conn_snd = conn_snd

            jobs.append(process)

        #   connection of first procedure with main thread
        conn_rcv_0, conn_snd_main = Pipe()
        self.procedures[0].conn_rcv = conn_rcv_0

        for job in jobs:
            job.start()

        self.procedures[0].starter()
        conn_snd_main.send(True)    #   let the first procedure behave as if its predecessor has terminated

        for job in jobs:
            job.join()


        self.log.info('All work completed (%s)' % time_offset_format(t_start))
