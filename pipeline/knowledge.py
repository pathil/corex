from shapely.geometry import Polygon
from typing import Dict, List, Tuple

from common.structs import UnarySet, BinarySet
from common.relations import *

def generate_layer_relations(layer_concepts: List[Tuple[int, List[Polygon]]], layer_rlvs, sets: List[str],    \
    tolerance: int = 20, img_shape: Tuple[int,int] = (224,224)) -> Dict[str, list]:
    
    sets = [x.lower() for x in sets]
    rel_dict : Dict[str, list] = dict()

    unary_list: List[UnarySet] = [Negativity(layer_rlvs)]

    binary_list = []
    if 'simple_align' in sets:
        binary_list.append(SimpleAlignment())
    if 'compass_align' in sets:
        binary_list.append(CompassAlign(tolerance))
    if 'nine_im' in sets:
        binary_list.append(NineIM())
    if 'distance' in sets:
        binary_list.append(Distance(img_shape))
    pseudo_ternary_list = []
    if 'amid' in sets:
        pseudo_ternary_list.append(Amid(tolerance))

    rel_dict['unary'] = __unaries(layer_concepts, unary_list)
    rel_dict['binary'] = __binaries(layer_concepts, binary_list) + __pseudo_ternaries(layer_concepts, pseudo_ternary_list)        
    # rel_dict['ternary'] = self.__ternary_relations(layer_concepts, ternary_list)
    return rel_dict

def __unaries(layer_concepts : List[Tuple[int, List[Polygon]]], sets: List[UnarySet]) -> List[Tuple[str, str]]:
    collection = []
    for a in layer_concepts:
        for set in sets:
            for rel in set.relation(a):
                collection.append((rel, a[0]))
    return collection

def __binaries(layer_concepts : List[Tuple[int, List[Polygon]]], sets: List[BinarySet]) -> List[Tuple[str, str, str]]:
    collection = []
    for a_i in range(len(layer_concepts)):
        a = layer_concepts[a_i]
        for b in layer_concepts[a_i+1:]:
            for set in sets:
                for rel in set.relation(a[1], b[1]):    #   if it is e.g. above_of and also left_of (only in 'simple_align' and 'amid' sets where max 2 can appear in this list)
                    collection.append((rel, a[0], b[0]))
    return collection

def __pseudo_ternaries(layer_concepts : List[Tuple[int, List[Polygon]]], sets: List[BinarySet]) -> List[Tuple[str, str, str]]:
    collection = []
    for a in layer_concepts:
        for b in layer_concepts:
            if a == b:
                continue
            for set in sets:
                for rel in set.relation(a[1], b[1]):
                    collection.append((rel, a[0], b[0]))
    return collection
    
def __ternaries() -> List[Tuple[str, str, str, str]]:
    pass
    
def predicate_names(sets: List[str]):
    
    return
