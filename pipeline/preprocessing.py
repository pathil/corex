import torch
# import torchvision.transforms as transform
from PIL import Image
import os
from tqdm import tqdm
import torch

from typing import List

from common.loader import ComposerInfo

class Preprocessor():
    def __init__(self, model: torch.nn.Module, device: str, comp_info: ComposerInfo) -> None:
        self.comp_info = comp_info
        model.to(device)
        self.model = model
        self.device = device
        
        self.transform = self.comp_info.get_composer()
        self.softmax = torch.nn.Softmax(dim=-1)
        self.model.eval()

    def __del__(self):
        if self.model != None:
            del self.model

    def covered_by_model_truth(self, files: list, target: int):
        covered = list()
        not_covered = list()
        transform = self.comp_info.get_composer()
        softmax = torch.nn.Softmax(dim=-1)
        self.model.eval()
        for file in tqdm(files, desc="Check for conformable model_truth", leave=False):
            sample_name = os.path.splitext(os.path.basename(file))[0]
            img = Image.open(file)
            sample = transform(img=img.convert('RGB')).unsqueeze(0).to(self.device)
                        
            pred = torch.argmax(softmax(self.model(sample)), dim=-1).item()
            if pred == target:
                covered.append(sample_name)
            else:
                not_covered.append(sample_name)
        return covered, not_covered

    def covered_by_mt(self, file: str, model_pos: int, model_neg: int) -> List[bool]:
        # sample_name = os.path.splitext(os.path.basename(file))[0]
        self.model.eval()
        img = Image.open(file)
        sample = self.transform(img=img.convert('RGB')).unsqueeze(0).to(self.device)
        pred = torch.argmax(self.model(sample), dim=-1).item()

        target_model = (model_pos, model_neg)

        return [True if x == pred else False for x in target_model]
