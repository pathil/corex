# CoReX

## Filesystem
'data/samples': Directory for a contrastive ground-truth subset of a dataset (500 samples in total recommended), that has to be organized in folders pos/ and neg/. Scripts for sampling from respective dataset can be found in 'helper_scripts/sampling/'. 

'data/datasets': Directory for the complete datasets. That is the aligned set from Adience, val+test of teapot/vases from Imagenet, PathMNIST or Picasso.

'results/experiments/': All results generated and evaluation/statistics-scripts will be placed in corresponding folder as defined in its config.ini. Pipeline results in the 'concepts/' contain relevance_mapping and localization  w.r.t to each concepts in a layer for a sample. On sample level, the BK is stored. The Aleph-File as well as the result from the induction step is stored in 'ilp/'
'results/experiments/[experiment]/experiment_results': Results, such as clustering and ranking produced by 'helper_scripts/statistics/stats.py' are placed here.

results/model_eval/': Model evaluation results produced by 'helper_scripts/statistics/evaluation.py'

## Setup
Before running the generation. You need to:
-   Copy sample data into '/sampling/pos' and '/sampling/neg'.
-   Make sure, filenames do not start with numbers and do not contain symbols other than underscores (_). 
-   Add a .INI in the /config/. Refer to the /config/config.md to understand its members
-   If you want to use another model as the predefined in common.loaders.LOADER, add it accordingly.-  
-   To retrieve the images for the teapot-vase project, execute 
    -   python helper_scripts/sampling/create_teapot.py
    -   with an extracted Imagenet val- and test-folder in 'data/datasets/imagenet' as well as the label data for the val-set
    -   find the pathmnist sampling material in 'helper_scripts/sampling/'.
-	To generate the picasso set, please refer to https://github.com/mc-lovin-mlem/concept-embeddings-and-ilp/tree/ki2020
-   To retrieve images for the adience-experiment please refer to https://talhassner.github.io/home/projects/Adience/Adience-data.html. Use the corresponding script 'create_adience.py' to retrieve sampled data.
-   A ready-to-go experiment is already set up. You will find a 'teapot_example'-folder and a .INI-file, that will work out of the box.


##  Build Theory
-   Build a theory from the 'teapot_example'-data with
    > python generation.py -p teapot_example
- 6 steps are performed. Each step is dependant on the previous
    - 0. Preprocessing creates lookup dictionaries for samples according to ground-truth and model-truth.
    - 1. Concept selection and relevance map generation via CRP.
    - 2. Geometrical Localization (shapely Polygon).
    - 3. Compute relations on spatial information.
    - 4. Translation into Prolog.
    - 5. Execute Aleph induction step.
- You can define a range of steps, in case you would like to recompute something without running the whole process again.
-   The output directory is defined in the experiments .INI-file. On default results are stored into /results/experiments/[experiment-name]
    -   Concept and Polygons are stored in /concepts/
    -   ILP Theory and BK in /ilp/

##  Experiment Results

-   To generate results for the constructed theories execute from the root-directory:
>   python scripts/statistics/stats.py -p [experiment-name]

-   To produce the statistics:
    -   concept_rankings: concept in relevance ranking for each  sample
    -   rules_per_sample: for each positive sample, all rules that apply
    -   subclassses: powerset and the subsets listing the samples
    -   faithfulness-score w.r.t the model
>   python scripts/statistics/evaluation.py -p [experiment-name] --mode [Mode]
-   The mode "normal" will only evaluate the unmodified finetuned model. "mask_other" will mask all filters, that do not appear in a the BK. "mask_rules" does the same as "mask_other", but also will mask the filters in the rules from the learnt theory.

  
##  Finetuning
-   Under 'scripts/training/' you can find the scripts, that have been used to create the models, which have been used for the experiments. Execute from the root-directory.
  > python scripts/training/[finetuning_*.py]
-   where * refers to the either 'adience', 'pathmnist', 'teapot' or 'picasso'.